/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api.metrics;

import com.gigadrivegroup.yoshino.api.API;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import spark.Route;
import spark.Spark;

import java.util.HashMap;

public class MetricsPathGroup {
	public static Route metrics = (request, response) -> {
		HashMap<String, Object> results = new HashMap<>();

		for (Module module : ModuleStorage.getInstance().getLoadedModules()) {
			if (module.getMetricsCollector() == null) continue;

			results.put(module.getName(), module.getMetricsCollector().toMap());
		}

		return results;
	};

	public static void init() {
		Spark.get("/metrics", metrics, SerializationUtil.GSON::toJson);
		Spark.options("/metrics", API.OPTIONS_RESPONSE, SerializationUtil.GSON::toJson);
	}
}
