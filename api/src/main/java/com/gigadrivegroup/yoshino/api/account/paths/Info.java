/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api.account.paths;

import com.gigadrivegroup.yoshino.api.API;
import com.gigadrivegroup.yoshino.api.RequestData;
import com.gigadrivegroup.yoshino.api.RouteHandler;
import com.gigadrivegroup.yoshino.core.account.Account;
import spark.Route;

import java.util.HashMap;

public class Info extends RouteHandler {
	public Info() {
		super("/account/info");
	}

	@Override
	public Route get() throws Exception {
		return ((request, response) -> {
			RequestData data = API.getRequestData(request);

			if (data.getAccount() != null) {
				Account account = data.getAccount();

				HashMap<String, Object> result = new HashMap<String, Object>();
				result.put("id", account.getId().toString());
				result.put("linkedAccounts", account.getLinkedAccounts().length);
				result.put("timeCreated", account.getTimeCreated().get());

				return result;
			}

			return error(response, 400);
		});
	}
}
