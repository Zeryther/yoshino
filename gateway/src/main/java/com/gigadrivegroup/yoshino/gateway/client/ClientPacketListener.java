/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client;

import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.gateway.client.packet.response.auth.ClientAuthorizationResponsePacket;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketHandler;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

public class ClientPacketListener implements PacketListener {
	@PacketHandler
	public void onClientAuthorizationResponse(ClientAuthorizationResponsePacket packet) {
		if (packet.getStatus() == PacketResponseStatus.SUCCESS) {
			GatewayClient client = ((GatewayClient) ModuleStorage.getInstance().getModule("gateway-client"));

			client.setConnectionUUID(packet.getConnectionUUID());

			Logger.log(LoggerLevel.INFO, "Authenticated with the gateway as %s", packet.getConnectionUUID().toString());
		} else {
			Logger.log(LoggerLevel.ERROR, "Failed to authenticate with the gateway!");
		}
	}
}
