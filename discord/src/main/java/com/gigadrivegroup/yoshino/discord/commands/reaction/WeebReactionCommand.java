/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.reaction;

import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandExecution;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.AnimeReactionComponent;
import com.gigadrivegroup.yoshino.discord.util.ApiUtil;
import com.github.natanbc.weeb4j.Weeb4J;
import com.github.natanbc.weeb4j.image.HiddenMode;
import com.github.natanbc.weeb4j.image.ImageProvider;
import com.github.natanbc.weeb4j.image.NsfwFilter;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.List;

public class WeebReactionCommand extends Command {
	/**
	 * Constructor
	 *
	 * @param name The command name
	 */
	public WeebReactionCommand(String name) {
		super(name, AnimeReactionComponent.class);
	}

	/**
	 * Constructor
	 *
	 * @param name    The command name
	 * @param aliases The command aliases
	 */
	public WeebReactionCommand(String name, List<String> aliases) {
		super(name, AnimeReactionComponent.class);

		setAliases(aliases);
	}

	@Override
	public void execute(CommandExecution execution) {
		ApiUtil apiUtil = ApiUtil.getInstance();
		if (apiUtil != null) {
			Weeb4J weebApi = ApiUtil.getInstance().getWeebApi();

			if (weebApi != null) {
				ImageProvider imageProvider = weebApi.getImageProvider();

				if (imageProvider != null) {
					imageProvider.getRandomImage(getName(), HiddenMode.DEFAULT, execution.getChannel().isNSFW() ? NsfwFilter.ALLOW_NSFW : NsfwFilter.NO_NSFW).async(image -> {
						execution.reply(new EmbedBuilder().setColor(Constants.YOSHINO_EMBED_COLOR).setImage(image.getUrl()).setFooter(execution.t("discord.component.animereactioncomponent.weebfooter"), null));
					});
				} else {
					execution.reply(execution.t("errorOccurred"));
				}
			} else {
				execution.reply(execution.t("errorOccurred"));
			}
		} else {
			execution.reply(execution.t("errorOccurred"));
		}
	}
}
