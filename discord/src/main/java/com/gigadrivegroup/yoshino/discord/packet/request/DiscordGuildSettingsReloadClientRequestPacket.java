/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.packet.request;

import com.gigadrivegroup.yoshino.gateway.client.packet.request.ClientRequestPacket;
import lombok.Getter;

/**
 * Sent from the client to the server, telling the server to spread the reload info across all instances
 */
public class DiscordGuildSettingsReloadClientRequestPacket extends ClientRequestPacket {
	/**
	 * @return The ID of the guild to reload the settings of.
	 */
	@Getter
	private long guildId;

	public DiscordGuildSettingsReloadClientRequestPacket(long guildId) {
		this.guildId = guildId;
	}
}
