/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.jda.message;

import com.gigadrivegroup.yoshino.discord.commands.CommandManager;
import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MessageReceivedListener extends ListenerAdapter {
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		DiscordGuild guild = DiscordGuild.getGuild(event.getGuild());
		DiscordUser user = DiscordUser.getUser(event.getAuthor());

		if (guild != null && user != null) {
			String commandPrefix = guild.getCommandPrefix();

			String message = event.getMessage() != null ? event.getMessage().getContentRaw() : null;
			if (message != null) {
				if (message.startsWith(commandPrefix)) {
					CommandManager.getInstance().getHandler().handleCommand(event);
				}
			}
		}
	}
}
