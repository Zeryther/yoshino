/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.guild;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.i18n.Locale;
import com.gigadrivegroup.yoshino.core.i18n.LocalizationManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.CachedObjectField;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.DiscordGuildSettings;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.BotSettingsComponent;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.DiscordComponent;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import com.gigadrivegroup.yoshino.discord.packet.request.DiscordGuildSettingsReloadClientRequestPacket;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.gigadrivegroup.yoshino.gateway.client.GatewayClient;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import lombok.Getter;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.utils.MiscUtil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

public class DiscordGuild extends CachedObject {
	/**
	 * @return The id of this guild.
	 */
	@Getter
	private long id;
	/**
	 * @return The name of this guild.
	 */
	@Getter
	private CachedObjectField<String> name = new CachedObjectField<>();
	/**
	 * @return The URL of the icon of this guild, null if the guild does not have an icon.
	 */
	@Getter
	private CachedObjectField<String> iconUrl = new CachedObjectField<>();

	/**
	 * @param inside The new inside value.
	 * @return True if the guild has Yoshino.
	 */
	@Getter
	private CachedObjectField<Boolean> inside = new CachedObjectField<>();

	private DiscordGuildSettings settings;
	private long settingsCacheTime;

	public DiscordGuild(long id) {
		this.id = id;

		DatabaseManager manager = DatabaseManager.getInstance();

		DBCursor cursor = manager.discordGuilds().find(new BasicDBObject("_id", this.id));
		if (cursor.count() > 0) {
			DBObject result = cursor.one();

			this.id = manager.retrieveLong(result, "_id");
			this.name = new CachedObjectField<>(manager.retrieveString(result, "name"));
			this.iconUrl = new CachedObjectField<>(manager.retrieveString(result, "icon"));
			this.inside = new CachedObjectField<>(manager.retrieveBoolean(result, "inside"));

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for guild %s (%s)", this.id, this.name.get());
		} else {
			if (this.isOnThisInstance()) {
				try {
					this.loadDataFromJDA();

					manager.discordGuilds().insert(this.toDBObject());

					getStorage().add(this);

					Logger.log(LoggerLevel.DEBUG, "Loaded data for guild %s (%s)", this.id, this.name.get());
				} catch (DuplicateKeyException e) {
					getGuild(this.id); // retry
				}
			}
		}

		cursor.close();
	}

	/**
	 * Gets a DiscordGuild object by it's ID.
	 *
	 * @param id The ID of the guild.
	 * @return The DiscordGuild object, null if it does not exist.
	 */
	public static DiscordGuild getGuild(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof DiscordGuild) {
					DiscordGuild o = ((DiscordGuild) object);

					if (o.id == id) {
						return o;
					}
				}
			}
		}

		DiscordGuild g = new DiscordGuild(id);
		return getStorage().contains(g) ? g : null;
	}

	/**
	 * Gets a DiscordGuild object by it's JDA object.
	 *
	 * @param guild The JDA object of the guild..
	 * @return The DiscordGuild object, null if it does not exist.
	 */
	public static DiscordGuild getGuild(Guild guild) {
		return guild != null ? getGuild(guild.getIdLong()) : null;
	}

	/**
	 * Gets the command prefix for this guild.
	 *
	 * @return The command prefix of this guild, returns the default prefix if the guild prefix could not be fetched.
	 */
	public String getCommandPrefix() {
		DiscordGuildSettings settings = this.getSettings();

		if (settings != null) {
			BotSettingsComponent component = settings.getComponent(BotSettingsComponent.class) != null ? (BotSettingsComponent) settings.getComponent(BotSettingsComponent.class) : null;

			if (component != null) {
				return component.getCommandPrefix();
			}
		}

		return Config.getInstance().getEnvironmentType().getDefaultCommandPrefix();
	}

	/**
	 * Gets the locale for this guild.
	 *
	 * @return The locale object, returns English by default.
	 */
	public Locale getLocale() {
		DiscordGuildSettings settings = this.getSettings();

		if (settings != null) {
			BotSettingsComponent component = settings.getComponent(BotSettingsComponent.class) != null ? (BotSettingsComponent) settings.getComponent(BotSettingsComponent.class) : null;

			if (component != null) {
				return component.getLocale();
			}
		}

		return LocalizationManager.getInstance().getLocale("en");
	}

	/**
	 * Returns the settings object from this guild. The settings are fetched from the database and cached for a maximum of {@link com.gigadrivegroup.yoshino.core.util.Constants#OBJECT_CACHE_TIME}. If the settings are not stored in the database yet, a new object will be created and saved to the database.
	 *
	 * @return The settings object.
	 */
	public DiscordGuildSettings getSettings() {
		final long cacheTime = Constants.OBJECT_CACHE_TIME;

		if (this.settings == null || (System.currentTimeMillis() - this.settingsCacheTime) >= cacheTime) {
			DatabaseManager manager = DatabaseManager.getInstance();

			DBCursor cursor = manager.discordGuildSettings().find(new BasicDBObject("_id", this.id));
			if (cursor.count() > 0) {
				DBObject result = cursor.one();

				this.settings = DiscordGuildSettings.fromDBObject(result);
			} else {
				this.settings = new DiscordGuildSettings(this.id);

				try {
					manager.discordGuildSettings().insert(this.settings.toDBObject());
				} catch (DuplicateKeyException e) {
					// ignored
				}
			}

			this.settingsCacheTime = System.currentTimeMillis();

			cursor.close();
		}

		return this.settings;
	}

	/**
	 * @return The JDA object of the shard this guild is on, null if the shard is not ready or on another instance.
	 */
	public JDA getShard() {
		return ShardHandler.getInstance().getShardByGuildId(this.id);
	}

	/**
	 * @return The ID of the shard this guild is on.
	 */
	public int getShardId() {
		return ShardHandler.getInstance().getShardIdByGuildId(this.id);
	}

	/**
	 * Gets a targeted user from a specific text (might be a mention or just a name)
	 *
	 * @param text The text.
	 * @return The DiscordUser object of the target, null if none could be found.
	 */
	public DiscordUser getTarget(String text) {
		return this.getTarget(text, null);
	}

	/**
	 * Gets a targeted user from a specific text (might be a mention or just a name)
	 *
	 * @param text     The text.
	 * @param fallback The user object to use as a fallback.
	 * @return The DiscordUser object of the target, returns the fallback if none could be found.
	 */
	public DiscordUser getTarget(String text, DiscordUser fallback) {
		if (text == null) return fallback;
		text = text.trim();
		if (text.isEmpty()) return fallback;

		Matcher matcher = Message.MentionType.USER.getPattern().matcher(text);

		DiscordUser target = null;

		while (matcher.find() && target == null) {
			// is mention

			try {
				target = DiscordUser.getUser(MiscUtil.parseSnowflake(matcher.group(1)));
			} catch (NumberFormatException ignored) {
				// ignored
			}
		}

		if (target == null) {
			Guild discordGuild = this.toJDAGuild();

			if (discordGuild != null) {
				List<Member> members = discordGuild.getMembers();

				if (members != null) {
					for (Member member : members) {
						User user = member.getUser();
						if (user == null) continue;

						if (Util.isValidLong(text)) {
							// is user ID

							Member m = discordGuild.getMemberById(Long.parseLong(text));
							if (m != null) {
								target = DiscordUser.getUser(m.getUser());
							}
						}

						if (target == null) {
							if (text.contains("#")) {
								String[] split = text.split("#");
								String potentialDiscriminator = split[split.length - 1];
								String potentialName = Util.implode(Arrays.copyOfRange(split, 0, split.length - 2), "#");

								if (user.getDiscriminator().equals(potentialDiscriminator) && (user.getName().equalsIgnoreCase(potentialName) || user.getName().toLowerCase().startsWith(potentialName.toLowerCase()))) {
									// is name#discriminator

									target = DiscordUser.getUser(user);
								}
							}
						}

						if (target == null) {
							// is name

							if (member.getNickname() != null && (member.getNickname().equalsIgnoreCase(text) || member.getNickname().toLowerCase().startsWith(text.toLowerCase()))) {
								target = DiscordUser.getUser(user);
							}

							if (target == null && (user.getName().equalsIgnoreCase(text) || user.getName().toLowerCase().startsWith(text.toLowerCase()))) {
								target = DiscordUser.getUser(user);
							}
						}

						if (target != null) break;
					}
				}
			}
		}

		return target;
	}

	/**
	 * Gets whether a component is enabled.
	 *
	 * @param componentType The component type.
	 * @return Whether the component is enabled, false if the component could not be found.
	 */
	public boolean isComponentEnabled(String componentType) {
		DiscordGuildSettings settings = this.getSettings();

		if (settings != null) {
			DiscordComponent component = settings.getComponent(componentType);

			if (component != null) {
				return component.isEnabled();
			}
		}

		return false;
	}

	/**
	 * Gets whether a component is enabled.
	 *
	 * @param component The component.
	 * @return Whether the component is enabled, false if the component could not be found.
	 */
	public boolean isComponentEnabled(Class<? extends DiscordComponent> component) {
		return this.isComponentEnabled(component.getSimpleName());
	}

	/**
	 * Returns whether the shard of this guild is on the current instance.
	 *
	 * @return True or false.
	 */
	public boolean isOnThisInstance() {
		ShardHandler handler = ShardHandler.getInstance();

		return Arrays.asList(handler.getShardIDs()).contains(handler.getShardIdByGuildId(this.id));
	}

	/**
	 * Updates the data by getting it from JDA, does not work if the guild is on another instance.
	 */
	public void loadDataFromJDA() {
		loadDataFromJDA(this.toJDAGuild());
	}

	/**
	 * Updates the data by getting it from JDA, does not work if the guild is on another instance.
	 *
	 * @param guild The {@link Guild} object to load the data from.
	 */
	public void loadDataFromJDA(Guild guild) {
		JDA shard = this.getShard();
		if (shard != null) {
			this.inside.set(shard.getGuildById(this.id) != null);
		}

		if (guild == null) return;
		if (guild.getIdLong() != this.id) return;

		this.name.set(guild.getName());
		this.iconUrl.set(guild.getIconUrl());

		this.saveData();
	}

	/**
	 * Reloads the guild settings from the database.
	 */
	public void reloadSettings() {
		this.settings = null;
	}

	/**
	 * Reloads the guild settings on all instances.
	 */
	public void reloadSettingsGlobal() {
		Module module = ModuleStorage.getInstance().getModule("gateway-client");
		if (module != null) {
			((GatewayClient) module).sendPacket(new DiscordGuildSettingsReloadClientRequestPacket(this.id));
		}
	}

	/**
	 * Saves the object data to the database, skipped if there is no data to save.
	 */
	public void saveData() {
		DatabaseManager manager = DatabaseManager.getInstance();
		super.saveData(manager.discordGuilds());
	}

	/**
	 * Saves the guild settings to the database.
	 */
	public void saveSettings() {
		if (this.settings != null) {
			DatabaseManager manager = DatabaseManager.getInstance();

			manager.discordGuildSettings().update(new BasicDBObject("_id", this.id), new BasicDBObject("$set", this.settings.toDBObject()));
		}
	}

	/**
	 * Returns the associated {@link Guild} object.
	 *
	 * @return The guild instance, null if the Guild could not be found (e.g. on another instance).
	 */
	public Guild toJDAGuild() {
		JDA shard = this.getShard();

		return shard != null ? shard.getGuildById(this.id) : null;
	}
}
