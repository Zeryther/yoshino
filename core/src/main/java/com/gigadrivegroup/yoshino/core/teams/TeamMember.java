/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.teams;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

public class TeamMember {
	/**
	 * @return The account ID of this team member.
	 */
	@Getter
	private ObjectId accountId;

	/**
	 * @return The rank of this team member.
	 */
	@Getter
	@Setter
	private TeamMemberRank rank;

	public TeamMember(ObjectId accountId, TeamMemberRank rank) {

	}
}
