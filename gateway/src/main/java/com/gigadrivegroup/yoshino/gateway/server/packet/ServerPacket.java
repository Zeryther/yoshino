/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.packet;

import com.gigadrivegroup.yoshino.gateway.packet.Packet;

/**
 * A packet sent by the server (may also hold response packet sent after a request packet from the server).
 */
public class ServerPacket extends Packet {
}
