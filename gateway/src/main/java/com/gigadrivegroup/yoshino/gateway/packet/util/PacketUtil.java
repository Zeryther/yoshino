/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.packet.util;

import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class PacketUtil {
	/**
	 * Returns the Packet object that is referenced in an input string that was received over the gateway.
	 *
	 * @param input The input string to be converted.
	 * @return The packet instance, null if the input string is not a valid packet.
	 */
	public static Packet getStringAsPacket(String input) {
		try {
			if (Util.isValidJsonObject(input)) {
				JsonObject o = new JsonParser().parse(input).getAsJsonObject();

				if (o.has("type") && o.get("type").isJsonPrimitive()) {
					String type = o.get("type").getAsString();

					Class<? extends Packet> packetClass = (Class<? extends Packet>) Class.forName(type);

					return SerializationUtil.GSON.fromJson(input, packetClass);
				}
			}
		} catch (Exception e) {
			// ignored
		}

		return null;
	}

	/**
	 * Returns whether an input string that was received over the gateway is a valid packet.
	 *
	 * @param input The input string to be checked.
	 * @return True or false.
	 */
	public static boolean isValidPacket(String input) {
		return getStringAsPacket(input) != null;
	}
}
