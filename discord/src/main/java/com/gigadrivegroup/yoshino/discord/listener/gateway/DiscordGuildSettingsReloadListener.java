/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.gateway;

import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import com.gigadrivegroup.yoshino.discord.packet.request.DiscordGuildSettingsReloadClientRequestPacket;
import com.gigadrivegroup.yoshino.discord.packet.request.DiscordGuildSettingsReloadServerRequestPacket;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;

public class DiscordGuildSettingsReloadListener implements PacketListener {
	// client asks server to reload guild settings
	public void on(DiscordGuildSettingsReloadClientRequestPacket packet) {
		ClientConnectionManager.getInstance().getConnections().forEach(clientConnection -> clientConnection.sendPacket(new DiscordGuildSettingsReloadServerRequestPacket(packet.getGuildId())));
	}

	// server forwards reload request
	public void on(DiscordGuildSettingsReloadServerRequestPacket packet) {
		DiscordGuild guild = DiscordGuild.getGuild(packet.getGuildId());

		if (guild != null) {
			guild.reloadSettings();
		}
	}
}
