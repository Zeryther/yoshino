/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client.packet.request;

import com.gigadrivegroup.yoshino.gateway.client.packet.ClientPacket;
import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnection;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;

/**
 * A packet sent by the client to the server, requesting data and expecting a {@link com.gigadrivegroup.yoshino.gateway.client.packet.response.ClientResponsePacket}.
 */
public class ClientRequestPacket extends ClientPacket {
	/**
	 * Calls {@link ClientConnection#sendPacket(Packet)}, use this method for easier use in {@link com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener PacketListeners}.
	 *
	 * @param packet
	 */
	public void sendPacket(Packet packet) {
		packet.setAssociatedConnectionUUID(this.getAssociatedConnectionUUID());

		ClientConnection connection = ClientConnectionManager.getInstance().getConnectionByUUID(packet.getAssociatedConnectionUUID());

		connection.sendPacket(packet);
	}
}
