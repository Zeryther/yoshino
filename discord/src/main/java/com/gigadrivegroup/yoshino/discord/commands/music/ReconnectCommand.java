/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.music;

import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.MusicComponent;

import java.util.Arrays;

public class ReconnectCommand extends Command {
	public ReconnectCommand() {
		super("reconnect", MusicComponent.class);

		setAliases(Arrays.asList("rejoin", "join"));
	}
}
