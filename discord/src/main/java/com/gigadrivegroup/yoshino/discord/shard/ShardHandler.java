/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.shard;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.thread.AsyncManager;
import com.gigadrivegroup.yoshino.discord.listener.jda.JDAListenerManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sedmelluq.discord.lavaplayer.jdaudp.NativeAudioSendFactory;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.bot.sharding.DefaultShardManagerBuilder;
import net.dv8tion.jda.bot.sharding.ShardManager;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.OnlineStatus;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Consumer;

public class ShardHandler {
	/**
	 * @param instance The new ShardHandler instance.
	 * @return The ShardHandler instance.
	 */
	@Getter
	@Setter
	private static ShardHandler instance;

	/**
	 * @return The IDs of the shards that this ShardHandler holds.
	 */
	@Getter
	private Integer[] shardIDs;

	/**
	 * @return The shardTotal value used to interact with Discord.
	 */
	@Getter
	private int shardTotal;

	/**
	 * @return The JDA ShardManager instance;
	 */
	@Getter
	private ShardManager shardManager;

	/**
	 * @return A list of running (ready) shards.
	 */
	@Getter
	private ArrayList<JDA> shards;

	/**
	 * Constructor
	 *
	 * @param shardIDs   An array of the shard IDs to startup.
	 * @param shardTotal
	 */
	public ShardHandler(Integer[] shardIDs, int shardTotal) {
		this.shardIDs = shardIDs;
		this.shardTotal = shardTotal;

		this.shards = new ArrayList<JDA>();

		try {
			// TODO: Add uncaught exception handler
			// TODO: Register listeners

			this.shardManager = new DefaultShardManagerBuilder()
					.setToken(Config.getInstance().getDiscordBotToken())
					.setBulkDeleteSplittingEnabled(false)
					.setAutoReconnect(true)
					.setStatus(OnlineStatus.OFFLINE)
					.setThreadFactory(new ThreadFactoryBuilder().setNameFormat("yoshino-jda-%d").setPriority(Thread.MAX_PRIORITY).build())
					.setCallbackPool(AsyncManager.getInstance().getRequestExecutor())
					.setRateLimitPool(AsyncManager.getInstance().getScheduledExecutor())
					.setShardsTotal(this.shardTotal)
					.setShards(Arrays.asList(this.shardIDs))
					.setAudioSendFactory(new NativeAudioSendFactory())
					.addEventListeners(JDAListenerManager.getEventWaiter())
					.build();

			JDAListenerManager.registerListeners(this.shardManager);
		} catch (LoginException e) {
			Logger.log(LoggerLevel.ERROR, "Failed to log into discord (%s)", e.getMessage());
			return;
		}
	}

	/**
	 * Executes a consumer on all loaded shards.
	 *
	 * @param consumer The consumer to execute.
	 */
	public void broadcastEval(Consumer<JDA> consumer) {
		this.shards.forEach(consumer);
	}

	/**
	 * Gets the shard the guild with the given ID is on.
	 *
	 * @param guildId The guild id.
	 * @return The JDA instance of the shard, null if not found.
	 */
	public JDA getShardByGuildId(long guildId) {
		Optional<JDA> o = this.shards.stream().filter(jda -> jda.getShardInfo().getShardId() == this.getShardIdByGuildId(guildId)).findAny();

		return o.isPresent() ? o.get() : null;
	}

	/**
	 * Calculates the shard ID the guild with the given ID is on.
	 *
	 * @param guildId The guild id.
	 * @return The shard ID this guild is on.
	 */
	public int getShardIdByGuildId(long guildId) {
		return (int) (guildId >> 22) % this.shardTotal;
	}

	/**
	 * @return True if all shards are ready and have been started.
	 */
	public boolean isReady() {
		return this.shards.size() == this.shardIDs.length;
	}

	/**
	 * Shuts down the ShardHandler and all it's shards.
	 */
	public void shutdown() {
		if (this.shardManager != null) {
			this.shardManager.shutdown();
			this.shardManager = null;
		}

		setInstance(null);
	}
}
