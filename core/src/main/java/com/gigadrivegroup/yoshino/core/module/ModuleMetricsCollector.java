/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.module;

import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;

import java.util.HashMap;

public class ModuleMetricsCollector {
	/**
	 * @return The statistics in a map to be used in the API etc.
	 */
	public HashMap<String, Object> toMap() {
		return null;
	}

	/**
	 * Gets the statistics as a JSON string.
	 *
	 * @return The JSON string.
	 */
	public String toJson() {
		return this.toJson(false);
	}

	/**
	 * Gets the statistics as a JSON string.
	 *
	 * @param prettyPrint If true, the result will be pretty-printed.
	 * @return The JSON string.
	 */
	public String toJson(boolean prettyPrint) {
		HashMap<String, Object> map = this.toMap();

		if (map != null) {
			return prettyPrint ? SerializationUtil.GSON_PRETTY_PRINT.toJson(map) : SerializationUtil.GSON.toJson(map);
		}

		return null;
	}
}
