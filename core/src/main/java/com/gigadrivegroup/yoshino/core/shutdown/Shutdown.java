/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.shutdown;

import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

public class Shutdown {
	/**
	 * Shuts down Yoshino with exit code 0
	 */
	public static void shutdown() {
		shutdown(0);
	}

	/**
	 * Shuts down Yoshino with the specified exit code
	 *
	 * @param exitCode The exit code Yoshino should use to shut down
	 */
	public static void shutdown(int exitCode) {
		Logger.log(LoggerLevel.INFO, "Shutting down! (Exit code %s)", exitCode);

		System.exit(exitCode);
	}

	/**
	 * Registers the shutdown hook that will execute on shutdown (only to be used once, in bootstrap).
	 */
	public static void registerShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				// shut down modules in reverse-dependent order
				shutdownModule("api");
				shutdownModule("gateway-client");
				shutdownModule("gateway-server");
				shutdownModule("discord");
				shutdownModule("twitch");
				shutdownModule("youtube");
				shutdownModule("core");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, "Yoshino Shutdown Hook"));
	}

	private static void shutdownModule(String name) throws Exception {
		ModuleStorage storage = ModuleStorage.getInstance();

		Module module = storage.getModule(name);
		if (module != null) {
			storage.unloadModule(module.getName());
		}
	}
}
