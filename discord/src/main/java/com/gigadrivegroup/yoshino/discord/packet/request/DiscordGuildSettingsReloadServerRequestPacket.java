/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.packet.request;

import com.gigadrivegroup.yoshino.gateway.server.packet.request.ServerRequestPacket;
import lombok.Getter;

/**
 * Sent from the server to the client, telling the client to reload the settings of a specific guild
 */
public class DiscordGuildSettingsReloadServerRequestPacket extends ServerRequestPacket {
	/**
	 * @return The ID of the guild to reload the settings of.
	 */
	@Getter
	private long guildId;

	public DiscordGuildSettingsReloadServerRequestPacket(long guildId) {
		this.guildId = guildId;
	}
}
