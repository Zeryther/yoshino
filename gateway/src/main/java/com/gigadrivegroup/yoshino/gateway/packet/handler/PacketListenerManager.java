/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.packet.handler;

import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import lombok.Getter;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

public class PacketListenerManager {
	/**
	 * @return The list of registered listeners and their data.
	 */
	@Getter
	private static ArrayList<PacketListenerData> listenerData;

	static {
		listenerData = new ArrayList<PacketListenerData>();
	}

	/**
	 * Invokes all listeners that listen for this type of packet.
	 *
	 * @param packet The packet to be handled.
	 * @throws Exception Thrown if the listener could not be invoked.
	 */
	public static void handlePacket(Packet packet) throws Exception {
		Object o = Class.forName(packet.getType()).cast(packet);

		Iterator<PacketListenerData> iterator = listenerData.iterator();
		while (iterator.hasNext()) {
			PacketListenerData data = iterator.next();

			if (data.getPacketType().equals(packet.getType())) {
				MethodUtils.invokeMethod(data.getListener(), data.getMethod().getName(), packet);
			}
		}
	}

	/**
	 * Registers a PacketListener class
	 *
	 * @param clazz The PacketListener class to be registered.
	 * @throws Exception Thrown if the listener could not be registered.
	 */
	public static void registerListener(Class<? extends PacketListener> clazz) throws Exception {
		Validate.isTrue(PacketListener.class.isAssignableFrom(clazz), String.format("%s is not a PacketListener!", clazz.getSimpleName()));

		for (Method method : clazz.getDeclaredMethods()) {
			if (method.getParameterCount() == 1 && method.isAnnotationPresent(PacketHandler.class) && Packet.class.isAssignableFrom(method.getParameterTypes()[0])) { // check if valid packet handler method
				method.setAccessible(true);

				PacketListener listener = clazz.newInstance();

				listenerData.add(new PacketListenerData(listener, method));
			}
		}
	}

	/**
	 * Unregisters a PacketListener class
	 *
	 * @param clazz The PacketListener class to be unregistered.
	 * @throws Exception Thrown if the listener could not be unregistered.
	 */
	public static void unregisterListener(Class<? extends PacketListener> clazz) {
		Iterator<PacketListenerData> iterator = getListenerData().iterator();
		while (iterator.hasNext()) {
			PacketListenerData data = iterator.next();

			if (data.getListener().getClass().equals(clazz)) {
				iterator.remove();
			}
		}
	}
}
