/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands;

import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommand;
import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommandManager;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class HelpCommand extends ConsoleCommand {
	public HelpCommand() {
		super("help");

		setAliases(Arrays.asList("commands"));
		setDescription("Lists all available commands.");
	}

	@Override
	public void execute(String label, String[] args) {
		ArrayList<ConsoleCommand> commands = new ArrayList<ConsoleCommand>(ConsoleCommandManager.getInstance().getCommands());
		Collections.sort(commands, (o1, o2) -> o1.getName().compareTo(o2.getName()));

		final int maxLength = 30;

		final String separator = "========================================================================================================================================";

		StringBuilder sb = new StringBuilder("\n")
				.append(separator).append("\n");

		for (ConsoleCommand command : commands) {
			sb.append("    ");

			StringBuilder s = new StringBuilder();

			s.append(command.getName());

			if (command.getAliases().size() > 0) {
				s.append(" (").append(Util.implode(command.getAliases(), ", ")).append(")");
			}

			while (s.toString().length() < maxLength) {
				s.append(" ");
			}

			sb.append(s);

			if (command.getDescription() != null) {
				sb.append(" -    ").append(command.getDescription());
			}

			sb.append("\n");
		}

		sb.append(separator);

		Logger.log(LoggerLevel.INFO, sb.toString());
	}
}
