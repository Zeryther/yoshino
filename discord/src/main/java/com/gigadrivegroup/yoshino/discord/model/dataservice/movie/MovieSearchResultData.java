/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.movie;

import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import lombok.Getter;
import lombok.Setter;

public class MovieSearchResultData extends DataServiceSearchResult.Data {
	@Getter
	@Setter
	private int year;
}
