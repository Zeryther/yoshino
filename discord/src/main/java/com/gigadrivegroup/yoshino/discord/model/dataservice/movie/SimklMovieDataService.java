/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.movie;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringEscapeUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class SimklMovieDataService extends MovieDataService {
	public SimklMovieDataService() {
		super("SIMKL-MOVIE");
	}

	@Override
	public DataServiceSearchResult[] search(String query) {
		OkHttpClient client = HttpUtil.getHttpClient();

		try {
			Response response = client.newCall(new Request.Builder()
					.url(String.format("https://api.simkl.com/search/movie?q=%s&client_id=%s", URLEncoder.encode(query, StandardCharsets.UTF_8), URLEncoder.encode(Config.getInstance().getSimklClientId(), StandardCharsets.UTF_8)))
					.header("User-Agent", Constants.USER_AGENT)
					.build()).execute();

			String body;
			if (response.isSuccessful() && response.body() != null && !(body = response.body().string()).isEmpty()) {
				JsonElement rootElement = new JsonParser().parse(body);
				response.close();

				if (rootElement.isJsonArray()) {
					ArrayList<JsonElement> r = new ArrayList<JsonElement>();

					rootElement.getAsJsonArray().forEach(e -> {
						if (r.size() == Config.getInstance().getMaximumDataServiceSearchResults()) return;

						if (e.isJsonObject()) r.add(e);
					});

					ArrayList<DataServiceSearchResult> results = new ArrayList<DataServiceSearchResult>();

					for (int i = 0; i < r.size(); i++) {
						JsonObject attributes = r.get(i).getAsJsonObject();

						String identifier = attributes.get("ids").getAsJsonObject().get("simkl_id").getAsString();
						String url = String.format("https://simkl.com/movie/%s", identifier);
						String title = attributes.get("title").getAsString();
						String year = attributes.has("year") ? attributes.get("year").getAsString() : null;

						DataServiceSearchResult result = new DataServiceSearchResult(identifier, String.format("%s%s", title, year != null ? String.format(" (%s)", year) : ""), url, this);
						results.add(result);
					}

					return results.toArray(new DataServiceSearchResult[]{});
				}
			}

			return null;
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void fetchData(DataServiceSearchResult result) {
		String identifier = result.getIdentifier();
		OkHttpClient client = HttpUtil.getHttpClient();

		try {
			Response response = client.newCall(new Request.Builder()
					.url(String.format("https://api.simkl.com/movies/%s?extended=full&client_id=%s", URLEncoder.encode(identifier, StandardCharsets.UTF_8), URLEncoder.encode(Config.getInstance().getSimklClientId(), StandardCharsets.UTF_8)))
					.header("User-Agent", Constants.USER_AGENT)
					.build()).execute();

			String body;
			if (response.isSuccessful() && response.body() != null && !(body = response.body().string()).isEmpty()) {
				JsonElement rootElement = new JsonParser().parse(body);
				response.close();

				if (rootElement.isJsonObject()) {
					JsonObject attributes = rootElement.getAsJsonObject();

					// get title
					String title = attributes.get("title").getAsString();

					// get image
					String image = null;
					if (attributes.has("poster") && !attributes.get("poster").isJsonNull()) {
						image = String.format("https://simkl.in/posters/%s_m.jpg", attributes.get("poster").getAsString());
					}

					// get year
					int year = -1;
					if (attributes.has("year") && !attributes.get("year").isJsonNull()) {
						year = attributes.get("year").getAsInt();
					}

					// get rating
					String rating = null;
					if (attributes.has("ratings") && attributes.get("ratings").isJsonObject() && attributes.get("ratings").getAsJsonObject().has("simkl") && attributes.get("ratings").getAsJsonObject().get("simkl").isJsonObject() && attributes.get("ratings").getAsJsonObject().get("simkl").getAsJsonObject().has("rating") && attributes.get("ratings").getAsJsonObject().get("simkl").getAsJsonObject().get("rating").isJsonPrimitive()) {
						rating = attributes.get("ratings").getAsJsonObject().get("simkl").getAsJsonObject().get("rating").getAsString();
					}

					// get status
					DataServiceSearchResult.Data.Status status = null;
					if (attributes.has("status") && !attributes.get("status").isJsonNull()) {
						String s = attributes.get("status").getAsString();

						switch (s) {
							case "airing":
								status = DataServiceSearchResult.Data.Status.CURRENT;
								break;
							case "ended":
								status = DataServiceSearchResult.Data.Status.FINISHED;
								break;
							default:
								status = null;
								break;
						}
					}

					// get synopsis (converted from HTML)
					String synopsis = null;
					if (attributes.has("overview") && !attributes.get("overview").isJsonNull()) {
						synopsis = StringEscapeUtils.unescapeHtml4(attributes.getAsJsonObject().get("overview").getAsString()).replace("<br/>", "\n")
								.replace("<b>", "**")
								.replace("</b>", "**")
								.replace("<i>", "*")
								.replace("</i>", "*")
								.replace("<u>", "__")
								.replace("</u>", "__")
								.replace("<strong>", "**")
								.replace("</strong>", "**")
								.replace("<em>", "*")
								.replace("<em>", "*")
								.replace("&quot;", "\"")
								.replace("&#039;", "'")
								.replace("<br />", "\n")
								.replace("<br>", "\n");
					}

					MovieSearchResultData data = new MovieSearchResultData();
					data.setIdentifier(result.getIdentifier());
					data.setName(title);
					data.setUrl(result.getLink());
					data.setImage(image);
					data.setYear(year);
					data.setRatingString(rating);
					data.setStatus(status);
					data.setDescription(synopsis);

					result.setDataObject(data);
				}
			}
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
		}
	}
}
