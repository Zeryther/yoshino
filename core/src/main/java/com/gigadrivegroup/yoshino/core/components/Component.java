/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.components;

import com.gigadrivegroup.yoshino.core.config.Config;
import lombok.Getter;

public class Component {
	/**
	 * @return The name of the component.
	 */
	@Getter
	private String type = this.getClass().getSimpleName();

	/**
	 * @return Whether the component is enabled.
	 */
	@Getter
	private boolean enabled = !this.mayBeDisabled() || Config.getInstance().isComponentEnabledByDefault();

	/**
	 * Defines whether this component should be enabled.
	 *
	 * @param enabled Whether this component should be enabled.
	 */
	public void setEnabled(boolean enabled) {
		if (!enabled && !mayBeDisabled()) return;

		this.enabled = enabled;
	}

	/**
	 * @return Whether the component may be disabled.
	 */
	public boolean mayBeDisabled() {
		return true;
	}
}
