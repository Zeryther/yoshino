/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.gateway.client.packet.request.auth.ClientAuthorizationRequestPacket;
import com.gigadrivegroup.yoshino.gateway.client.packet.response.auth.ClientAuthorizationResponsePacket;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketHandler;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnection;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;

public class ServerPacketListener implements PacketListener {
	@PacketHandler
	public void onClientAuthorizationRequest(ClientAuthorizationRequestPacket packet) {
		ClientConnection connection = ClientConnectionManager.getInstance().getConnectionByUUID(packet.getAssociatedConnectionUUID());

		if (connection != null) {
			if (packet.getAuthToken().equals(Config.getInstance().getGatewayAuthToken())) {
				connection.setStatus(ClientConnection.Status.AUTHENTICATED);
				connection.sendPacket(new ClientAuthorizationResponsePacket(PacketResponseStatus.SUCCESS, connection.getUuid()));
			} else {
				connection.sendPacket(new ClientAuthorizationResponsePacket(PacketResponseStatus.FAILURE, connection.getUuid()));
			}
		}
	}
}
