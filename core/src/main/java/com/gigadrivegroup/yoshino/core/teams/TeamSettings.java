/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.teams;

import com.gigadrivegroup.yoshino.core.components.Component;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.util.ComponentUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;

public class TeamSettings {
	/**
	 * @return The object ID of the platform link that holds these settings.
	 */
	@Getter
	private transient ObjectId platformLinkId;

	/**
	 * @return A list of all registered components.
	 */
	@Getter
	private ArrayList<Component> components;

	public TeamSettings(ObjectId platformLinkId) {
		this.platformLinkId = platformLinkId;
		this.components = new ArrayList<Component>();
	}

	/**
	 * Re-creates a settings object from a MongoDB object.
	 *
	 * @param object The MongoDB object.
	 * @return The settings object.
	 */
	public static TeamSettings fromDBObject(DBObject object) {
		DatabaseManager manager = DatabaseManager.getInstance();

		TeamSettings settings = new TeamSettings(manager.retrieveObjectId(object, "_id"));

		settings.components = (ArrayList) manager.retrieveObjectOfType(ArrayList.class, object, "components", new ArrayList<Component>());

		return settings;
	}

	/**
	 * Gets a component object by it's type.
	 *
	 * @param type The type of the component object.
	 * @return The component object, null if the component could not be found.
	 */
	public Component getComponent(String type) {
		Iterator<Component> iterator = this.components.iterator();
		while (iterator.hasNext()) {
			Component component = iterator.next();

			if (component.getType().equalsIgnoreCase(type)) {
				return component;
			}
		}

		try {
			Class clazz = ComponentUtil.getComponentClass(type);

			if (clazz != null) {
				Component component = (Component) clazz.newInstance();

				if (component != null) {
					components.add(component);
					return component;
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			// ignored
		}

		return null;
	}

	/**
	 * Gets a component object by it's class.
	 *
	 * @param clazz The class of the component object.
	 * @return The component object, null if the component could not be found.
	 */
	public Component getComponent(Class<? extends Component> clazz) {
		return this.getComponent(clazz.getSimpleName());
	}

	/**
	 * Gets this settings object as a DBObject to be used in a database.
	 *
	 * @return The converted DBObject.
	 */
	public DBObject toDBObject() {
		DatabaseManager manager = DatabaseManager.getInstance();

		return new BasicDBObject("_id", this.platformLinkId)
				.append("components", manager.convertObjectToDBObject(this.components));
	}
}
