/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util;

import lombok.Getter;
import okhttp3.OkHttpClient;

public class HttpUtil {
	/**
	 * @return The OkHttpClient instance used to make HTTP requests.
	 */
	@Getter
	private static OkHttpClient httpClient;

	static {
		httpClient = new OkHttpClient.Builder()
				.build();
	}
}
