/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

public class CachedObjectField<T> {
	private T value;

	@Getter
	@Setter
	private boolean modified;

	public CachedObjectField() {
	}

	public CachedObjectField(T value) {
		this.value = value;
	}

	public void set(T value) {
		if (!Objects.equals(value, this)) {
			this.value = value;
			this.modified = true;
		}
	}

	public T get() {
		return value;
	}

	public boolean is() {
		return this.value != null;
	}
}
