/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.utility;

import com.gigadrivegroup.yoshino.core.components.Component;
import com.gigadrivegroup.yoshino.core.util.ComponentUtil;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandExecution;
import com.gigadrivegroup.yoshino.discord.commands.CommandManager;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.DiscordComponent;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.UtilityComponent;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.Arrays;

public class HelpCommand extends Command {
	public HelpCommand() {
		super("help", UtilityComponent.class);

		setAliases(Arrays.asList("commands"));
	}

	@Override
	public void execute(CommandExecution execution) {
		if (execution.getArguments().length > 0) {
			String commandName = execution.getArguments()[0];
			Command command = CommandManager.getInstance().getCommand(commandName);

			if (command != null) {
				StringBuilder sb = new StringBuilder("\n");

				final String spaces = "       ";

				sb.append("```css\n");

				sb.append(String.format("[%s%s]", execution.getGuild().getCommandPrefix(), command.getName()));
				sb.append("\n\n");

				sb.append(execution.t("discord.cmd.help.commandHelp.description")).append(":\n");
				sb.append(spaces).append(command.getDescription(execution.getGuild().getLocale()));

				sb.append("\n\n");
				sb.append(execution.t("discord.cmd.help.commandHelp.usage")).append(":\n");
				sb.append(spaces).append(command.getUsage(execution.getGuild().getCommandPrefix(), command.getName(), execution.getGuild().getLocale()));

				String[] examples = command.getExamples(execution.getGuild().getCommandPrefix(), command.getName(), execution.getGuild().getLocale());

				if (examples.length > 0) {
					sb.append("\n\n");
					sb.append(execution.t("discord.cmd.help.commandHelp.examples")).append(":\n");

					for (int i = 0; i < examples.length; i++) {
						String example = examples[i];

						sb.append(spaces).append(example);
						if (i != examples.length - 1) sb.append("\n");
					}
				}

				if (command.getAliases().size() > 0) {
					sb.append("\n\n");
					sb.append(execution.t("discord.cmd.help.commandHelp.aliases")).append(":\n");
					sb.append(spaces).append(Util.implode(command.getAliases(), ", "));
				}

				sb.append("\n\n").append(execution.t("discord.parameterSymbolInfo"));

				sb.append("```");

				execution.reply(sb.toString());
			} else {
				execution.reply(execution.t("discord.cmd.help.commandNotFound"));
			}
		} else {
			EmbedBuilder builder = new EmbedBuilder()
					.setColor(Constants.YOSHINO_EMBED_COLOR)
					.setTitle(execution.t("discord.cmd.help.headline"));

			for (Class<? extends Component> comp : ComponentUtil.COMPONENTS) {
				if (!DiscordComponent.isDiscordComponent(comp)) continue;
				Class<? extends DiscordComponent> componentClass = (Class<? extends DiscordComponent>) comp;

				DiscordComponent component = execution.getGuild().getSettings().getComponent(componentClass);

				if (component != null && component.isEnabled()) {
					StringBuilder sb = new StringBuilder();

					for (Command command : CommandManager.getInstance().getCommands()) {
						if (command.getComponent().equals(componentClass)) {
							if (!sb.toString().isEmpty()) {
								sb.append(",");
							}

							sb.append(String.format("`%s`", command.getName()));
						}
					}

					if (!sb.toString().isEmpty()) {
						builder.addField(execution.t(String.format("discord.component.%s.title", component.getType().toLowerCase())), sb.toString(), false);
					}
				}
			}

			execution.reply(builder);
		}
	}
}
