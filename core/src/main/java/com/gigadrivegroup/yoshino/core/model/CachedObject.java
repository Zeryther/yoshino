/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.model;

import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Represents a class that holds cached objects that should be cleared out after 10 minutes of usage.
 */
public class CachedObject {
	@Getter
	private static ArrayList<? super CachedObject> storage;

	static {
		storage = new ArrayList<CachedObject>();
	}

	/**
	 * Returns the {@link System#currentTimeMillis() timestamp} this object was last invoked.
	 */
	@Getter
	@Setter
	private long lastInvocationTime;

	public CachedObject() {
		this.invoke();
	}

	/**
	 * Removes all objects from the {@link CachedObject#getStorage()}  storage} that are older than {@link com.gigadrivegroup.yoshino.core.util.Constants#OBJECT_CACHE_TIME}
	 */
	public static void clearCache() {
		clearCache(false);
	}

	/**
	 * Removes all objects from the {@link CachedObject#getStorage()}  storage} that are older than {@link com.gigadrivegroup.yoshino.core.util.Constants#OBJECT_CACHE_TIME}
	 * @param force If true, objects will be cleared even if they are not older than {@link com.gigadrivegroup.yoshino.core.util.Constants#OBJECT_CACHE_TIME}.
	 */
	public static void clearCache(boolean force) {
		final long maxHoldingTime = Constants.OBJECT_CACHE_TIME;

		//storage.removeIf(o -> System.currentTimeMillis() - ((CachedObject) o).getLastInvocationTime() >= maxHoldingTime);
		Iterator<? super CachedObject> iterator = storage.iterator();
		while (iterator.hasNext()) {
			Object o = iterator.next();

			if (force || System.currentTimeMillis() - ((CachedObject) o).getLastInvocationTime() >= maxHoldingTime) {
				try {
					o.getClass().getDeclaredMethod("saveData").invoke(o);
				} catch (Exception e) {
					e.printStackTrace(); // TODO
				}

				iterator.remove();
			}
		}
	}

	/**
	 * Updates this object's {@link CachedObject#getLastInvocationTime() invocation time}.
	 */
	public void invoke() {
		this.lastInvocationTime = System.currentTimeMillis();
	}

	/**
	 * Saves the object data to the database, skipped if there is no data to save.
	 */
	public void saveData(DBCollection collection) {
		DBObject object = this.toDBObject();

		if (object != null) {
			collection.update(new BasicDBObject("_id", object.get("_id")), new BasicDBObject("$set", object));
		}
	}

	public DBObject toDBObject() {
		try {
			Field idField = this.getClass().getDeclaredField("id");

			if (idField != null) {
				BasicDBObject object = new BasicDBObject();

				idField.setAccessible(true);

				object.append("_id", idField.get(this));

				Field[] clazzFields = this.getClass().getDeclaredFields();
				for (Field clazzField : clazzFields) {
					clazzField.setAccessible(true);

					if (clazzField.getType().equals(CachedObjectField.class)) {
						try {
							CachedObjectField cachedObjectField = (CachedObjectField) clazzField.get(this);
							if (cachedObjectField == null || !cachedObjectField.isModified()) continue;

							object.append(clazzField.getName(), (!(cachedObjectField.get() instanceof String || cachedObjectField.get() instanceof Integer || cachedObjectField.get() instanceof Double || cachedObjectField.get() instanceof Boolean || cachedObjectField.get() instanceof Long || cachedObjectField.get() instanceof Date || cachedObjectField.get() instanceof Timestamp || cachedObjectField.get() instanceof Float || cachedObjectField.get() instanceof Character || cachedObjectField.get() instanceof Byte)) ? JSON.parse(SerializationUtil.GSON.toJson(cachedObjectField.get())) : cachedObjectField.get());
						} catch (IllegalAccessException e) {
							// TODO
							e.printStackTrace();
						}
					}
				}

				return object;
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @return An array of all cached object fields of this cached object.
	 */
	public CachedObjectField[] getFields() {
		ArrayList<CachedObjectField> fields = new ArrayList<CachedObjectField>();

		Field[] clazzFields = this.getClass().getFields();
		for (Field clazzField : clazzFields) {
			if (clazzField.getType().equals(CachedObjectField.class)) {
				try {
					CachedObjectField cachedObjectField = (CachedObjectField) clazzField.get(this);

					fields.add(cachedObjectField);
				} catch (IllegalAccessException e) {
					// TODO
					e.printStackTrace();
				}
			}
		}

		return fields.toArray(new CachedObjectField[]{});
	}
}
