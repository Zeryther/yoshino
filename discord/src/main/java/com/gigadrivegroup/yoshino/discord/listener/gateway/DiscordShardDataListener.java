/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.gateway;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.discord.packet.request.DiscordShardDataRequestPacket;
import com.gigadrivegroup.yoshino.discord.packet.response.DiscordShardDataResponsePacket;
import com.gigadrivegroup.yoshino.discord.shard.GlobalShardHandler;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.gigadrivegroup.yoshino.discord.util.ShardUtil;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketHandler;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnection;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionDiscordData;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

import java.util.Arrays;

public class DiscordShardDataListener implements PacketListener {
	private int shardTotal = -1;

	@PacketHandler
	public void onDataRequest(DiscordShardDataRequestPacket packet) {
		ClientConnection connection = ClientConnectionManager.getInstance().getConnectionByUUID(packet.getAssociatedConnectionUUID());

		if (connection != null) {
			if (connection.getStatus() == ClientConnection.Status.AUTHENTICATED) {
				ClientConnectionDiscordData discordData = connection.getDiscordData();

				if (discordData != null) {
					if (discordData.getShardIDs().size() == 0) {
						GlobalShardHandler globalShardHandler = GlobalShardHandler.getInstance();

						if (globalShardHandler != null) {
							int shardTotal = ShardUtil.getRecommendedShardAmount(Config.getInstance().getDiscordBotToken());

							if (shardTotal != -1) {
								Integer[] newShards = globalShardHandler.assignShardIDs(Config.getInstance().getDiscordShardAmount());

								discordData.getShardIDs().addAll(Arrays.asList(newShards));

								packet.sendPacket(new DiscordShardDataResponsePacket(PacketResponseStatus.SUCCESS, newShards, shardTotal));
							}
						}
					}
				}
			}
		}
	}

	@PacketHandler
	public void onDataResponse(DiscordShardDataResponsePacket packet) {
		if (ShardHandler.getInstance() == null) {
			ShardHandler manager = new ShardHandler(packet.getShardIDs(), packet.getShardTotal());

			ShardHandler.setInstance(manager);

			Logger.log(LoggerLevel.DEBUG, "Registered new shard handler: %s | %s", SerializationUtil.GSON.toJson(packet.getShardIDs()), packet.getShardTotal());
		}
	}
}
