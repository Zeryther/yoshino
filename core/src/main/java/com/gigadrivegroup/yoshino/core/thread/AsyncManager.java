/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.thread;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AsyncManager {
	/**
	 * @return The AsyncManager instance.
	 */
	@Getter
	@Setter
	private static AsyncManager instance;

	/**
	 * @return The global ExecutorService.
	 */
	@Getter
	private ExecutorService requestExecutor;

	/**
	 * @return The global ScheduledExecutorService.
	 */
	@Getter
	private ScheduledExecutorService scheduledExecutor;

	public AsyncManager() {
		this.requestExecutor = Executors.newFixedThreadPool(350, new ThreadFactoryBuilder().setNameFormat("yoshino-requestExecutor-%d").setPriority(Thread.MAX_PRIORITY).build());
		this.scheduledExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder().setNameFormat("yoshino-scheduledExecutor-%d").setPriority(Thread.MAX_PRIORITY).build());

		// TODO: Set uncaught exception handler
	}

	public void async(Runnable runnable) {
		if (!this.requestExecutor.isShutdown() && !this.requestExecutor.isTerminated()) {
			this.requestExecutor.execute(runnable);
		}
	}

	public void delay(Runnable runnable, long millis) {
		if (!this.scheduledExecutor.isShutdown() && !this.scheduledExecutor.isTerminated()) {
			this.scheduledExecutor.schedule(runnable, millis, TimeUnit.MILLISECONDS);
		}
	}

	public void delaySeconds(Runnable runnable, long seconds) {
		delay(runnable, seconds * 1000);
	}

	public void repeat(Runnable runnable, long millis) {
		delay(() -> {
			runnable.run();
			repeat(runnable, millis);
		}, millis);
	}

	public void repeatSeconds(Runnable runnable, long seconds) {
		repeat(runnable, seconds * 1000);
	}

	public void shutdown() {
		if (this.requestExecutor != null) {
			this.requestExecutor.shutdown();
			this.requestExecutor = null;
		}

		if (this.scheduledExecutor != null) {
			this.scheduledExecutor.shutdown();
			this.scheduledExecutor = null;
		}
	}
}
