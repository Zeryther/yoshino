/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api;

import com.gigadrivegroup.yoshino.api.account.AccountPathGroup;
import com.gigadrivegroup.yoshino.api.auth.AuthPathGroup;
import com.gigadrivegroup.yoshino.api.metrics.MetricsPathGroup;
import com.gigadrivegroup.yoshino.core.EnvironmentType;
import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.common.collect.ImmutableMap;
import com.google.gson.reflect.TypeToken;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import spark.debug.DebugScreen;

import java.util.HashMap;

public class API extends Module {
	public API() {
		super("api");
	}

	public static RequestData getRequestData(Request request) throws Exception {
		return new RequestData(request);
	}

	public static HashMap<String, String> getRequestDataParameters(Request request) {
		SerializationUtil.GSON.toJson(request.requestMethod());

		if (request.requestMethod().equalsIgnoreCase("GET")) {
			HashMap<String, String> data = new HashMap<>();

			request.queryMap().toMap().forEach((key, values) -> data.put(key, Util.implode(values, " ")));

			return data;
		} else {
			if (request.contentType().toLowerCase().startsWith("application/json")) {
				return SerializationUtil.GSON.fromJson(request.body(), new TypeToken<HashMap<String, String>>() {
				}.getType());
			}

			return new HashMap<>();
		}
	}

	public static final Route OPTIONS_RESPONSE = (request, response) -> {
		response.status(204);
		return "";
	};

	/**
	 * Creates an error response to be used in an API route.
	 *
	 * @param response The response.
	 * @param code     The HTTP code to be used.
	 * @return The object to return in the route.
	 */
	public static ImmutableMap<String, String> error(Response response, int code) {
		return error(response, code, "An error occurred.");
	}

	@Override
	public void stop() throws Exception {
		Spark.stop();
		Spark.awaitStop();
	}

	/**
	 * Creates an error response to be used in an API route.
	 *
	 * @param response The response.
	 * @param code     The HTTP code to be used.
	 * @param message  The error message.
	 * @return The object to return in the route.
	 */
	public static ImmutableMap<String, String> error(Response response, int code, String message) {
		response.status(code);

		return ImmutableMap.of("error",message);
	}

	@Override
	public void start() throws Exception {
		Spark.port(Config.getInstance().getApiServerPort());

		// TODO
		Spark.exception(Exception.class, ((e, request, response) -> {
			Logger.log(LoggerLevel.ERROR, "An error occurred!");
			e.printStackTrace(); // TODO

			if (Config.getInstance().getEnvironmentType() == EnvironmentType.DEVELOPMENT) {
				new DebugScreen().handle(e, request, response);
			}
		}));

		Spark.before((request, response) -> {
			response.header("Access-Control-Allow-Origin", "*");
			response.header("Access-Control-Allow-Headers", "Authorization,Content-Type,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control");
		});

		Spark.after(((request, response) -> {
			if (!request.requestMethod().equalsIgnoreCase("OPTIONS") && (Config.getInstance().getEnvironmentType() != EnvironmentType.DEVELOPMENT || response.status() != 500)) {
				response.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,DELETE,PUT,PATCH");
				response.header("Content-Type", "application/json; charset=utf-8");
			}
		}));

		Spark.get("/", (request, response) -> ImmutableMap.of("status", "200 OK"), SerializationUtil.GSON::toJson);

		AccountPathGroup.init();
		AuthPathGroup.init();
		MetricsPathGroup.init();

		Spark.notFound((request, response) -> SerializationUtil.GSON.toJson(ImmutableMap.of("error", "404 Not Found")));
		Spark.internalServerError((request, response) -> SerializationUtil.GSON.toJson(ImmutableMap.of("error", "500 Internal Server Error")));

		Spark.awaitInitialization();
	}
}
