/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.youtube;

import com.gigadrivegroup.yoshino.core.module.Module;

public class YouTube extends Module {
	public YouTube() {
		super("youtube");
	}
}
