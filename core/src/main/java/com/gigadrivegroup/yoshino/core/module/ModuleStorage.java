/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.module;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Arrays;

public class ModuleStorage {
	@Getter
	@Setter
	private static ModuleStorage instance;

	/**
	 * @return An {@link java.util.ArrayList ArrayList} of loaded modules
	 */
	@Getter
	private ArrayList<Module> loadedModules;

	public ModuleStorage() {
		this.loadedModules = new ArrayList<Module>();
	}

	/**
	 * Loads a module into the storage
	 *
	 * @param module The module to be loaded
	 * @throws IllegalArgumentException Thrown if there is already a module loaded with the given {@link Module#getName() namespace}.
	 * @throws Exception                Thrown if the module fails to start
	 */
	public void loadModule(Module module) throws IllegalArgumentException, Exception {
		Validate.isTrue(this.getModule(module.getName()) == null, "There is already a module loaded with the name " + module.getName());

		if (Arrays.asList(Config.getInstance().getDisabledModules()).contains(module.getName())) {
			Logger.log(LoggerLevel.DEBUG, String.format("Skipping module '%s' due to being disabled in the configuration.", module.getName()));
			return;
		}

		Logger.log(LoggerLevel.DEBUG, String.format("Starting module '%s'...", module.getName()));

		module.start();

		Logger.log(LoggerLevel.DEBUG, String.format("Loading module '%s'...", module.getName()));

		this.loadedModules.add(module);

		Logger.log(LoggerLevel.INFO, String.format("Module '%s' is ready!", module.getName()));
	}

	/**
	 * Unloads a module from the storage
	 *
	 * @param name The name of the module to be unloaded
	 * @throws IllegalArgumentException Thrown if there is no module loaded with the given {@link Module#getName() namespace}.
	 * @throws Exception                Thrown if the module fails to stop
	 */
	public void unloadModule(String name) throws IllegalArgumentException, Exception {
		Module module = this.getModule(name);
		Validate.isTrue(module != null, "There is no module loaded with the name " + name);

		Logger.log(LoggerLevel.DEBUG, String.format("Stopping module '%s'...", module.getName()));

		module.stop();

		Logger.log(LoggerLevel.DEBUG, String.format("Unloading module '%s'...", module.getName()));

		this.loadedModules.remove(module);

		Logger.log(LoggerLevel.INFO, String.format("Module '%s' has been removed!", module.getName()));
	}

	/**
	 * Gets a module by it's {@link Module#getName() namespace}
	 *
	 * @param name The name of the module to be found
	 * @return The module, null if there is no module with this name
	 */
	public Module getModule(String name) {
		Validate.notNull(name, "The given module name may not be null");
		Validate.notEmpty(name, "The given module name may not be empty");
		Validate.notNull(this.loadedModules, "The loaded modules list is null, has the storage been initiated properly?");

		for (Module module : this.loadedModules) {
			if (module.getName() != null && module.getName().equalsIgnoreCase(name)) {
				return module;
			}
		}

		return null;
	}
}
