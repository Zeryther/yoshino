/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.youtube;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;

public class GoogleAPIUtil {
	public static final HttpTransport HTTP_TRANSPORT;
	public static final JsonFactory JSON_FACTORY;

	static {
		HTTP_TRANSPORT = new NetHttpTransport();
		JSON_FACTORY = new GsonFactory();
	}
}
