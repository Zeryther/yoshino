/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice;

import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.discord.listener.jda.JDAListenerManager;
import com.gigadrivegroup.yoshino.discord.model.dataservice.anime.KitsuAnimeDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.anime.MyAnimeListAnimeDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.anime.SimklAnimeDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.manga.KitsuMangaDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.manga.MyAnimeListMangaDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.movie.SimklMovieDataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.tv.SimklTVDataService;
import com.jagrosh.jdautilities.menu.OrderedMenu;
import lombok.Getter;
import lombok.Setter;

public class DataService {
	/**
	 * @return The name used to identify this data service.
	 */
	@Getter
	@Setter
	private String name;

	public DataService(String name) {
		this.name = name;
	}

	/**
	 * @return The menu builder used to display the selection of the search results.
	 */
	public static final OrderedMenu.Builder getMenuBuilder() {
		return new OrderedMenu.Builder()
				.allowTextInput(true)
				.useNumbers()
				.setColor(Constants.YOSHINO_EMBED_COLOR)
				.setEventWaiter(JDAListenerManager.getEventWaiter())
				.useCancelButton(true);
	}

	/**
	 * Finds a data service by it's name.
	 *
	 * @param name The name to look for.
	 * @return The data service object or null if it could not be found.
	 */
	public static DataService getDataService(String name) {
		for (Class<? extends DataService> clazz : getDataServiceClasses()) {
			try {
				DataService dataService = clazz.newInstance();

				if (dataService.getName().equalsIgnoreCase(name)) {
					return dataService;
				}
			} catch (Exception e) {
			}
		}

		return null;
	}

	/**
	 * @return An array of available data service classes.
	 */
	public static Class<? extends DataService>[] getDataServiceClasses() {
		return new Class[]{
				KitsuAnimeDataService.class,
				MyAnimeListAnimeDataService.class,
				SimklAnimeDataService.class,
				KitsuMangaDataService.class,
				MyAnimeListMangaDataService.class,
				SimklMovieDataService.class,
				SimklTVDataService.class
		};
	}

	/**
	 * Executes a search on this data service.
	 *
	 * @param query The search query.
	 * @return An array of search results, null if the data could not be fetched.
	 */
	public DataServiceSearchResult[] search(String query) {
		return null;
	}

	/**
	 * Updates the data in the search result to be used for further information display.
	 *
	 * @param result The result to update.
	 */
	public void fetchData(DataServiceSearchResult result) {

	}
}
