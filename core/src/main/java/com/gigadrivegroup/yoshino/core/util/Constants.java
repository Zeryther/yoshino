/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util;

import com.gigadrivegroup.yoshino.core.Core;
import com.gigadrivegroup.yoshino.core.config.Config;

import java.awt.*;

public class Constants {
	/**
	 * Main color used for Discord embeds
	 */
	public static final Color YOSHINO_EMBED_COLOR;
	/**
	 * Caching time for all objects in milliseconds.
	 */
	public static final long OBJECT_CACHE_TIME;
	/**
	 * The User-Agent header value to use for HTTP requests.
	 */
	public static final String USER_AGENT;

	static {
		YOSHINO_EMBED_COLOR = new Color(82, 116, 167);
		OBJECT_CACHE_TIME = 60 * 1000;
		USER_AGENT = String.format("Yoshino/%s/%s", Core.getVersion(), Config.getInstance().getEnvironmentType().name());
	}
}
