/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands;

import lombok.Getter;
import lombok.Setter;

public class CommandSettings {
	/**
	 * @return The name of the command these settings are bound to.
	 */
	@Getter
	private String commandName;

	/**
	 * @return Whether the command is enabled or not.
	 */
	@Getter
	@Setter
	private boolean enabled = true;

	/**
	 * @return Whether to mention the executor in the reply.
	 */
	@Getter
	@Setter
	private boolean replyToExecutor = true;

	/**
	 * @return Whether to send the response in a private message or not.
	 */
	@Getter
	@Setter
	private boolean privateResponse = false;

	/**
	 * @return Whether to delete the message that invoked this command.
	 */
	@Getter
	@Setter
	private boolean invocationDeleteMode = false;

	public CommandSettings(String commandName) {
		this.commandName = commandName;
	}

	/**
	 * @return The command these settings are bound to.
	 */
	public Command getCommand() {
		return CommandManager.getInstance().getCommand(this.commandName);
	}
}
