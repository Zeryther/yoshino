/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.guild.settings.components;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.i18n.Locale;
import com.gigadrivegroup.yoshino.core.i18n.LocalizationManager;
import org.apache.commons.lang3.Validate;

public class BotSettingsComponent extends DiscordComponent {
	private String commandPrefix;
	private String locale;

	/**
	 * @return The command prefix.
	 */
	public String getCommandPrefix() {
		if (commandPrefix != null && !commandPrefix.isEmpty()) {
			return commandPrefix;
		}

		return Config.getInstance().getEnvironmentType().getDefaultCommandPrefix();
	}

	/**
	 * Updates the command prefix.
	 *
	 * @param commandPrefix The new command prefix.
	 */
	public void setCommandPrefix(String commandPrefix) {
		if (commandPrefix == null || commandPrefix.isEmpty()) {
			commandPrefix = Config.getInstance().getEnvironmentType().getDefaultCommandPrefix();
		}

		if (commandPrefix.length() > 5) {
			commandPrefix = commandPrefix.substring(0, 5);
		}

		this.commandPrefix = commandPrefix;
	}

	/**
	 * @return The locale, returns English as default.
	 */
	public Locale getLocale() {
		if (this.locale != null) {
			Locale locale = LocalizationManager.getInstance().getLocale(this.locale);

			if (locale != null) {
				return locale;
			}
		}

		return LocalizationManager.getInstance().getLocale("en");
	}

	/**
	 * Updates the locale.
	 *
	 * @param code The code of the locale.
	 */
	public void setLocale(String code) {
		Validate.notNull(code, "The locale code may not be null.");
		Validate.notEmpty(code, "The locale code may not be empty.");

		this.locale = code.equalsIgnoreCase("en") ? null : code;
	}

	/**
	 * Updates the locale.
	 *
	 * @param locale The locale object.
	 */
	public void setLocale(Locale locale) {
		Validate.notNull(locale, "The locale may not be null.");

		this.locale = locale.getCode().equalsIgnoreCase("en") ? null : locale.getCode();
	}

	@Override
	public boolean mayBeDisabled() {
		return false;
	}
}
