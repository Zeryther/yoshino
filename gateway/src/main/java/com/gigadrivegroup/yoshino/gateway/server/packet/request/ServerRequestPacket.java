/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.packet.request;

import com.gigadrivegroup.yoshino.gateway.server.packet.ServerPacket;

/**
 * A packet sent by the client to the server, requesting data and expecting a {@link com.gigadrivegroup.yoshino.gateway.server.packet.response.ServerResponsePacket}.
 */
public class ServerRequestPacket extends ServerPacket {
}
