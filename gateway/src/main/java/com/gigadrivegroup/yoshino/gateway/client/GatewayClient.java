/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.gateway.client.packet.request.auth.ClientAuthorizationRequestPacket;
import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListenerManager;
import com.gigadrivegroup.yoshino.gateway.packet.util.PacketUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class GatewayClient extends Module {
	public Scanner in;
	public PrintWriter out;
	/**
	 * @return The client socket instance.
	 */
	@Getter
	private Socket clientSocket;
	/**
	 * @return The Thread that is being used to read the input stream.
	 */
	@Getter
	private Thread readThread;
	/**
	 * @return The Thread that is being to write to the output stream.
	 */
	@Getter
	private Thread writeThread;
	/**
	 * @param connectionUUID The new UUID of this connection.
	 * @return The UUID of this connection, null if the client has not been authorized yet.
	 */
	@Getter
	@Setter
	private UUID connectionUUID;
	/**
	 * @return The packets currently in queue to be sent.
	 */
	@Getter
	private LinkedBlockingQueue<Packet> packetQueue;

	private ArrayList<Class<? extends PacketListener>> listeners;

	private String lastLine;

	public GatewayClient() {
		super("gateway-client");

		this.packetQueue = new LinkedBlockingQueue<Packet>();

		this.listeners = new ArrayList<Class<? extends PacketListener>>();
		this.addListeners();
	}

	private void addListeners() {
		this.listeners.add(ClientPacketListener.class);
	}

	@Override
	public void start() throws Exception {
		this.listeners.forEach(listener -> {
			try {
				PacketListenerManager.registerListener(listener);
				Logger.log(LoggerLevel.DEBUG, "Registered listener: %s", listener.getSimpleName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		this.packetQueue.add(new ClientAuthorizationRequestPacket(Config.getInstance().getGatewayAuthToken()));

		this.clientSocket = new Socket(Config.getInstance().getGatewayHost(), Config.getInstance().getGatewayPort());
		this.in = new Scanner(this.clientSocket.getInputStream());
		this.out = new PrintWriter(this.clientSocket.getOutputStream(), true);

		// handle input
		this.readThread = new Thread(() -> {
			try {
				while (this.in.hasNextLine() && isConnected()) {
					this.lastLine = this.in.nextLine();

					if (PacketUtil.isValidPacket(this.lastLine)) {
						Packet packet = PacketUtil.getStringAsPacket(this.lastLine);

						if (packet != null) {
							Logger.log(LoggerLevel.DEBUG, "Incoming packet: %s", packet.getType());
							PacketListenerManager.handlePacket(packet);
						}
					}
				}

				this.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, "Yoshino Gateway Client Read Thread");

		// handle output
		this.writeThread = new Thread(() -> {
			try {
				while (isConnected()) {
					if (this.packetQueue.size() > 0) {
						Packet packet = this.packetQueue.take();

						out.println(SerializationUtil.GSON.toJson(packet));
						Logger.log(LoggerLevel.DEBUG, "Outgoing packet: %s", packet.getType());
					}
				}

				this.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, "Yoshino Gateway Client Write Thread");

		this.readThread.start();
		this.writeThread.start();
	}

	@Override
	public void stop() throws Exception {
		this.close();

		this.listeners.forEach(listener -> {
			try {
				PacketListenerManager.unregisterListener(listener);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * @return True if the connection is still running.
	 */
	public boolean isConnected() {
		if (this.clientSocket == null) {
			return false;
		} else if (this.clientSocket.isConnected()) {
			return true;
		} else {
			try {
				this.close();
			} catch (IOException ignored) {
				// does not matter, just ignore
			}

			return false;
		}
	}

	/**
	 * Closes the connection and all readers.
	 *
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (this.readThread != null) {
			this.readThread.interrupt();
			this.readThread = null;
		}

		if (this.writeThread != null) {
			this.writeThread.interrupt();
			this.writeThread = null;
		}

		if (this.clientSocket != null) {
			this.clientSocket.close();
			this.clientSocket = null;
		}

		if (this.in != null) {
			try {
				this.in.close();
			} catch (NullPointerException e) {
				// ignored
			}

			this.in = null;
		}

		if (this.out != null) {
			this.out.close();
			this.out = null;
		}
	}

	/**
	 * Adds a packet to the {@link GatewayClient#getPacketQueue() queue}.
	 *
	 * @param packet
	 */
	public void sendPacket(Packet packet) {
		try {
			if (packet.getAssociatedConnectionUUID() == null || !packet.getAssociatedConnectionUUID().equals(this.getConnectionUUID())) {
				packet.setAssociatedConnectionUUID(this.getConnectionUUID());
			}

			this.packetQueue.put(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
