/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.shard;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.discord.util.ShardUtil;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class GlobalShardHandler {
	/**
	 * @param instance The new GlobalShardHandler instance.
	 * @return The GlobalShardHandler instance.
	 */
	@Getter
	@Setter
	private static GlobalShardHandler instance;

	/**
	 * Returns an array of assignable shard IDs matching a shard amount.
	 *
	 * @param shardAmount The maximum amount of shards to assign.
	 * @return An array of shard IDs.
	 */
	public Integer[] assignShardIDs(int shardAmount) {
		int shardTotal = ShardUtil.getRecommendedShardAmount(Config.getInstance().getDiscordBotToken());

		if (shardTotal == -1) return new Integer[]{}; // return empty array on shard request failure

		ArrayList<Integer> shards = new ArrayList<Integer>();

		for (int i = 0; i < shardTotal; i++) {
			if (shards.size() >= shardAmount) break;
			if (ClientConnectionManager.getInstance().getConnectionByDiscordShardID(i) != null) continue;

			shards.add(i);
		}

		return shards.toArray(new Integer[]{});
	}

	public void shutdown() {
		setInstance(null);
	}
}
