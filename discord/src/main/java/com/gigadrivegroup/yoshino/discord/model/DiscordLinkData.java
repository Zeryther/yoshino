/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model;

import com.gigadrivegroup.yoshino.core.Platform;
import com.gigadrivegroup.yoshino.core.account.AccountLinkData;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import lombok.Getter;
import lombok.Setter;

public class DiscordLinkData extends AccountLinkData {
	public DiscordLinkData() {
		super(Platform.DISCORD);
	}

	/**
	 * @param clientId The new client ID.
	 * @return The client ID, may be null.
	 */
	@Getter
	@Setter
	private long clientId;

	/**
	 * @param clientSecret The new client secret.
	 * @return The client secret, may be null.
	 */
	@Getter
	@Setter
	private String clientSecret;

	/**
	 * @param accessToken The new access token.
	 * @return The access token, may be null.
	 */
	@Getter
	@Setter
	private String accessToken;

	/**
	 * @param refreshToken The new refresh token.
	 * @return The refresh token, may be null.
	 */
	@Getter
	@Setter
	private String refreshToken;

	@Override
	public String getUsername() {
		DiscordUser discordUser = DiscordUser.getUser(Long.valueOf(this.getId()));

		if (discordUser != null) {
			return String.format("%s#%s", discordUser.getName().get(), discordUser.getDiscriminator().get());
		}

		return super.getUsername();
	}

	@Override
	public String getAvatarURL() {
		DiscordUser discordUser = DiscordUser.getUser(Long.valueOf(this.getId()));

		if (discordUser != null) {
			return discordUser.getAvatarUrl().get();
		}

		return super.getAvatarURL();
	}
}
