/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.twitch;

import com.gigadrivegroup.yoshino.core.module.Module;

public class Twitch extends Module {
	public Twitch() {
		super("twitch");
	}
}
