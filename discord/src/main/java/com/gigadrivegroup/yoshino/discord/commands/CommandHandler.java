/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands;

import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.DiscordComponent;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.time.Instant;
import java.util.Arrays;

public class CommandHandler {
	/**
	 * Handles a message event that was previously verified as a command.
	 *
	 * @param event The message event.
	 */
	public void handleCommand(MessageReceivedEvent event) {
		DiscordGuild guild = DiscordGuild.getGuild(event.getGuild());
		DiscordUser user = DiscordUser.getUser(event.getAuthor());

		if (guild != null && user != null) {
			String commandPrefix = guild.getCommandPrefix();

			String message = event.getMessage() != null ? event.getMessage().getContentRaw() : null;
			if (message != null) {
				if (message.startsWith(commandPrefix) && message.length() > commandPrefix.length()) {
					String beheaded = message.substring(commandPrefix.length());

					String[] split = beheaded.split(" ");

					String label = split[0];
					String[] args = split.length > 1 ? Arrays.copyOfRange(split, 1, split.length) : new String[]{};

					Command command = CommandManager.getInstance().getCommand(label);

					if (command != null) {
						DiscordComponent component = guild.getSettings().getComponent(command.getComponent());

						if (component != null) {
							if (component.isEnabled()) {
								CommandSettings commandSettings = component.getCommandSettings(command);

								if (commandSettings != null) {
									if (commandSettings.isEnabled()) {
										CommandExecution execution = new CommandExecution();

										execution.setLabel(label);
										execution.setArguments(args);
										execution.setUser(user);
										execution.setGuild(guild);
										execution.setChannel(event.getTextChannel());
										execution.setCommandSettings(commandSettings);
										execution.setTime(Instant.now());
										execution.setEvent(event);

										command.execute(execution);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
