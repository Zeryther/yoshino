/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.connection;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class ClientConnectionManager {
	/**
	 * @param instance The new ClientConnectionManager instance.
	 * @return The ClientConnectionManager instance.
	 */
	@Getter
	@Setter
	private static ClientConnectionManager instance;

	/**
	 * @return A list of open client connections
	 */
	@Getter
	private ArrayList<ClientConnection> connections;

	/**
	 * Constructor
	 */
	public ClientConnectionManager() {
		this.connections = new ArrayList<ClientConnection>();
	}

	/**
	 * Ends all connections held by the manager
	 *
	 * @throws IOException
	 */
	public static void shutdown() throws IOException {
		Iterator<ClientConnection> iterator = getInstance().getConnections().iterator();
		while (iterator.hasNext()) {
			ClientConnection clientConnection = iterator.next();

			clientConnection.close();
		}

		getInstance().getConnections().clear();
	}

	/**
	 * @return An unused connection UUID.
	 */
	public UUID generateConnectionUUID() {
		UUID id = null;

		while (id == null || this.getConnectionByUUID(id) != null) {
			id = UUID.randomUUID();
		}

		return id;
	}

	/**
	 * Gets a connection by it's UUID.
	 *
	 * @param id The connection UUID.
	 * @return The connection instance.
	 */
	public ClientConnection getConnectionByUUID(UUID id) {
		if (id == null) return null;

		Iterator<ClientConnection> iterator = this.connections.iterator();
		while (iterator.hasNext()) {
			ClientConnection connection = iterator.next();

			if (id.equals(connection.getUuid())) {
				return connection;
			}
		}

		return null;
	}

	/**
	 * Gets a connection by a Discord shard ID.
	 *
	 * @param shardID The shard ID.
	 * @return The client connection holding this shard, null if no client is holding this shard.
	 */
	public ClientConnection getConnectionByDiscordShardID(int shardID) {
		Validate.isTrue(shardID >= 0, "The shard ID may not be less than 0.");

		Iterator<ClientConnection> iterator = getConnections().iterator();
		while (iterator.hasNext()) {
			ClientConnection connection = iterator.next();

			ClientConnectionDiscordData discordData = connection.getDiscordData();
			if (discordData != null) {
				ArrayList<Integer> shards = discordData.getShardIDs();

				if (shards != null) {
					if (shards.contains(shardID)) {
						return connection;
					}
				}
			}
		}

		return null;
	}
}
