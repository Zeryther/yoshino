/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.i18n;

import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;

public class Locale {
	/**
	 * @return The language code of this locale.
	 */
	@Getter
	private String code;

	/**
	 * @return The name of this locale in english.
	 */
	@Getter
	private String name;

	/**
	 * @return The localized name of this locale.
	 */
	@Getter
	private String localizedName;

	/**
	 * @return The native java locale object.
	 */
	@Getter
	private java.util.Locale javaLocale;

	/**
	 * @return The loaded phrases of this locale.
	 */
	@Getter
	private HashMap<String, String> phrases;

	/**
	 * Constructor
	 *
	 * @param code The language code.
	 * @param file The file that contains the translation phrases.
	 */
	public Locale(String code, File file) throws IOException {
		this.code = code;
		this.javaLocale = new java.util.Locale(this.code);
		this.localizedName = this.javaLocale.getDisplayName(this.javaLocale);
		this.phrases = new HashMap<String, String>();

		switch (code.toLowerCase()) {
			case "zh_hans":
				this.name = "Chinese (Simplified)";
				break;
			case "zh_hant":
				this.name = "Chinese (Traditional)";
				break;
			case "pt_br":
				this.name = "Portuguese (Brazil)";
				break;
			default:
				this.name = this.javaLocale.getDisplayName();
				break;
		}

		JsonParser parser = new JsonParser();

		JsonElement root = parser.parse(new String(Files.readAllBytes(file.toPath()), Charset.forName("UTF-8")));

		if (root.isJsonObject()) {
			JsonObject object = root.getAsJsonObject();
			for (String id : object.keySet()) {
				if (id == null || id.isEmpty()) continue;

				if (object.get(id) != null && !object.get(id).isJsonNull() && !object.get(id).getAsString().isEmpty()) {
					phrases.put(id.toLowerCase(), object.get(id).getAsString());
				}
			}
		} else {
			Logger.log(LoggerLevel.WARN, "Failed to load %s (%s), file root is not a json object!", this.name, this.code);
		}
	}

	/**
	 * Gets a translated message of a language by it's phrase identifier.
	 *
	 * @param phrase    The phrase id of the message.
	 * @param variables The variables of the message.
	 * @return The translated message, returns the english translation as a fallback or the phrase identifier if no message could be found.
	 */
	public String getTranslatedMessage(String phrase, Object... variables) {
		return LocalizationManager.getInstance().getTranslatedMessage(this.getCode(), phrase, variables);
	}

	/**
	 * Checks if a phrase with a specific phrase identifier exists.
	 *
	 * @param phrase The phrase identifier.
	 * @return True if the phrase exists.
	 */
	public boolean doesPhraseExist(String phrase) {
		return this.phrases.containsKey(phrase.toLowerCase());
	}
}
