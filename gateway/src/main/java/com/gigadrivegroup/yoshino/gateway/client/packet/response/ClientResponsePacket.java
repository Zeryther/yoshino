/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client.packet.response;

import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.gateway.client.GatewayClient;
import com.gigadrivegroup.yoshino.gateway.client.packet.ClientPacket;
import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import lombok.Getter;
import lombok.Setter;

/**
 * A packet sent by the server to the client, after receiving a {@link com.gigadrivegroup.yoshino.gateway.client.packet.request.ClientRequestPacket}.
 */
public class ClientResponsePacket extends ClientPacket {
	/**
	 * @param status The status of this packet.
	 * @return The status of this packet.
	 */
	@Getter
	@Setter
	private PacketResponseStatus status;

	public ClientResponsePacket(PacketResponseStatus status) {
		this.status = status;
	}

	/**
	 * Calls {@link com.gigadrivegroup.yoshino.gateway.client.GatewayClient#sendPacket(Packet)}, use this method for easier use in {@link com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener PacketListeners}.
	 *
	 * @param packet
	 */
	public void sendPacket(Packet packet) {
		((GatewayClient) ModuleStorage.getInstance().getModule("gateway-client")).sendPacket(packet);
	}
}
