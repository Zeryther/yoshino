/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.guild.settings.components;

public class UtilityComponent extends DiscordComponent {
	@Override
	public boolean mayBeDisabled() {
		return false;
	}
}
