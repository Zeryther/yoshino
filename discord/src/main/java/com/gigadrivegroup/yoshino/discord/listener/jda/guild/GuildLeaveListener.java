/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.jda.guild;

import com.gigadrivegroup.yoshino.core.thread.AsyncManager;
import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class GuildLeaveListener extends ListenerAdapter {
	@Override
	public void onGuildLeave(GuildLeaveEvent event) {
		DiscordGuild guild = DiscordGuild.getGuild(event.getGuild());

		if (guild != null) {
			guild.getInside().set(false);
			AsyncManager.getInstance().async(guild::saveData);
		}
	}
}
