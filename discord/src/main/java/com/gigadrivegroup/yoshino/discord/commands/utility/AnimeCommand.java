/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.utility;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandExecution;
import com.gigadrivegroup.yoshino.discord.commands.CommandSettings;
import com.gigadrivegroup.yoshino.discord.model.dataservice.DataService;
import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import com.gigadrivegroup.yoshino.discord.model.dataservice.anime.AnimeSearchResultData;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.UtilityComponent;
import com.jagrosh.jdautilities.menu.OrderedMenu;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;

import java.util.Arrays;

public class AnimeCommand extends Command {
	public AnimeCommand() {
		super("anime", UtilityComponent.class);

		setAliases(Arrays.asList("animesearch", "searchanime"));
		setRequiredDiscordPermissions(Arrays.asList(Permission.MESSAGE_MANAGE, Permission.MESSAGE_ADD_REACTION));
		setCommandSettingsClass(AnimeCommandSettings.class);
	}

	@Override
	public void execute(CommandExecution execution) {
		DataService dataService = DataService.getDataService(((AnimeCommandSettings) execution.getCommandSettings()).getAnimeDataService());
		if (dataService == null) {
			execution.reply(execution.t("errorOccurred"));
			return;
		}

		if (execution.getArguments().length > 0) {
			execution.reply(execution.t("discord.dataService.search.fetching"), m -> {
				DataServiceSearchResult[] results = dataService.search(Util.implode(execution.getArguments(), " "));

				if (results != null) {
					if (results.length >= 2) {
						OrderedMenu.Builder builder = DataService.getMenuBuilder();

						String[] displayStrings = new String[results.length];
						for (int i = 0; i < results.length; i++) {
							DataServiceSearchResult result = results[i];

							displayStrings[i] = String.format("**[%s](%s)**", result.getName(), result.getLink());
						}

						builder.clearChoices()
								.addChoices(displayStrings)
								.setSelection((message, i) -> {
									DataServiceSearchResult result = results[i - 1];
									DataServiceSearchResult.Data data = result.getDataObject();

									if (data != null) {
										this.showData((AnimeSearchResultData) data, execution);
									} else {
										execution.getChannel().sendMessage(execution.reply(execution.t("errorOccurred"))).queue();
									}
								})
								.setText("asd")
								.build()
								.display(m);
					} else if (results.length == 1) {
						DataServiceSearchResult result = results[0];
						DataServiceSearchResult.Data data = result.getDataObject();

						if (data != null) {
							this.showData((AnimeSearchResultData) data, execution, m);
						} else {
							m.editMessage(execution.reply(execution.t("errorOccurred"))).queue();
						}
					} else {
						m.editMessage(execution.reply(execution.t("discord.dataService.search.noResults"))).queue();
					}
				} else {
					m.editMessage(execution.reply(execution.t("errorOccurred"))).queue();
				}
			});
		} else {
			execution.replyWithUsage();
		}
	}

	private void showData(AnimeSearchResultData data, CommandExecution execution) {
		this.showData(data, execution, null);
	}

	private void showData(AnimeSearchResultData data, CommandExecution execution, Message message) {
		EmbedBuilder builder = new EmbedBuilder()
				.setThumbnail(data.getImage())
				.setTitle(data.getName(), data.getUrl())
				.setColor(Constants.YOSHINO_EMBED_COLOR);

		if (data.getEpisodes() > 0) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.episodes"), String.valueOf(data.getEpisodes()), true);
		}

		if (data.getRatingString() != null) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.rating"), data.getRatingString(), true);
		}

		if (data.getType() != null) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.type"), data.getType(), true);
		}

		if (data.getStatus() != null) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.status"), execution.t(String.format("discord.dataSearch.infoWidget.status.%s", data.getStatus().name().toLowerCase())), true);
		}

		if (data.getStartDate() != null) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.startDate"), data.getStartDate(), true);
		}

		if (data.getEndDate() != null) {
			builder.addField(execution.t("discord.dataSearch.infoWidget.endDate"), data.getEndDate(), true);
		}

		String synposis = null;
		if (data.getDescription() != null) {
			synposis = String.format("%s%s", Util.limitString(data.getDescription(), Config.getInstance().getDataServiceDescriptionLimit()), data.getDescription().length() > Config.getInstance().getDataServiceDescriptionLimit() ? "..." : "");
		}

		if (synposis != null) builder.addField(execution.t("discord.dataSearch.infoWidget.synposis"), synposis, false);

		if (message != null) {
			message.editMessage(execution.reply(builder)).queue();
		} else {
			execution.getChannel().sendMessage(execution.reply(builder)).queue();
		}
	}

	public static class AnimeCommandSettings extends CommandSettings {
		/**
		 * @return The service used to fetch anime data.
		 */
		@Getter
		@Setter
		private String animeDataService = "KITSU-ANIME";

		public AnimeCommandSettings() {
			super("anime");
		}
	}
}
