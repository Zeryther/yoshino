/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.guild.settings;

import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.util.ComponentUtil;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.DiscordComponent;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Iterator;

public class DiscordGuildSettings {
	/**
	 * @return The ID of the guild that holds these settings.
	 */
	@Getter
	private transient long guildId;
	/**
	 * @return A list of all registered components.
	 */
	@Getter
	private ArrayList<DiscordComponent> components;

	public DiscordGuildSettings(long guildId) {
		this.guildId = guildId;
		this.components = new ArrayList<>();
	}

	/**
	 * Re-creates a settings object from a MongoDB object.
	 *
	 * @param object The MongoDB object.
	 * @return The settings object.
	 */
	public static DiscordGuildSettings fromDBObject(DBObject object) {
		DatabaseManager manager = DatabaseManager.getInstance();

		DiscordGuildSettings settings = new DiscordGuildSettings(manager.retrieveLong(object, "_id"));

		settings.components = manager.retrieveArrayListOfType(object, "components");

		return settings;
	}

	/**
	 * Gets a component object by it's type.
	 *
	 * @param type The type of the component object.
	 * @return The component object, null if the component could not be found.
	 */
	public DiscordComponent getComponent(String type) {
		Iterator<DiscordComponent> iterator = this.components.iterator();
		while (iterator.hasNext()) {
			DiscordComponent component = iterator.next();

			if (component.getType().equalsIgnoreCase(type)) {
				return component;
			}
		}

		try {
			Class clazz = ComponentUtil.getComponentClass(type);

			if (clazz != null) {
				DiscordComponent component = (DiscordComponent) clazz.newInstance();

				if (component != null) {
					components.add(component);
					return component;
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			// ignored
		}

		return null;
	}

	/**
	 * Gets a component object by it's class.
	 *
	 * @param clazz The class of the component object.
	 * @return The component object, null if the component could not be found.
	 */
	public DiscordComponent getComponent(Class<? extends DiscordComponent> clazz) {
		return this.getComponent(clazz.getSimpleName());
	}

	/**
	 * Gets this settings object as a DBObject to be used in a database.
	 *
	 * @return The converted DBObject.
	 */
	public DBObject toDBObject() {
		DatabaseManager manager = DatabaseManager.getInstance();

		return new BasicDBObject("_id", this.guildId)
				.append("components", manager.convertObjectToDBObject(this.components));
	}
}
