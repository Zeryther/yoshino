/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.reaction;

import java.util.Arrays;

public class BakaCommand extends WeebReactionCommand {
	public BakaCommand() {
		super("baka", Arrays.asList("idiot", "moron"));
	}
}
