/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands;

import com.gigadrivegroup.yoshino.core.i18n.Locale;
import com.gigadrivegroup.yoshino.core.i18n.LocalizationManager;
import com.gigadrivegroup.yoshino.core.util.ComponentUtil;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.DiscordComponent;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.core.Permission;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;

public class Command {
	/**
	 * @return The command name.
	 */
	@Getter
	private String name;

	/**
	 * @return The command aliases.
	 */
	@Getter
	@Setter
	private List<String> aliases;

	/**
	 * @return The required Discord permissions for this command.
	 */
	@Getter
	@Setter
	private List<Permission> requiredDiscordPermissions;

	/**
	 * @return The component this command is a part of.
	 */
	@Getter
	private Class<? extends DiscordComponent> component;

	/**
	 * @return The class that holds the command settings.
	 */
	@Getter
	@Setter
	private Class<? extends CommandSettings> commandSettingsClass;

	public Command(String name, String componentType) {
		Validate.notNull(name, "The command name may not be null.");
		Validate.notEmpty(name, "The command name may not be empty.");

		Validate.notNull(componentType, "The component type may not be null.");
		Validate.notEmpty(componentType, "The component type may not be empty.");

		this.name = name;
		this.component = (Class<? extends DiscordComponent>) ComponentUtil.getComponentClass(componentType);
		this.commandSettingsClass = CommandSettings.class;

		Validate.notNull(this.component, String.format("The component with the type %s could not be found", componentType));
	}

	public Command(String name, Class<? extends DiscordComponent> component) {
		Validate.notNull(name, "The command name may not be null.");
		Validate.notEmpty(name, "The command name may not be empty.");

		Validate.notNull(component, "The component may not be null.");

		this.name = name;
		this.component = component;
		this.commandSettingsClass = CommandSettings.class;
	}

	/**
	 * Executes the command.
	 *
	 * @param execution Data about where, when and by who the command was executed.
	 */
	public void execute(CommandExecution execution) {

	}

	/**
	 * Gets the command description in English.
	 *
	 * @return The command description string.
	 */
	public String getDescription() {
		return getDescription(LocalizationManager.getInstance().getLocale("en"));
	}

	/**
	 * Gets the command description in a specific locale.
	 *
	 * @param locale The locale to use.
	 * @return The command description string.
	 */
	public String getDescription(Locale locale) {
		return locale.getTranslatedMessage(String.format("discord.cmd.%s.description", this.name));
	}

	/**
	 * Gets the command usage in English.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @return The command usage string.
	 */
	public String getUsage(String commandPrefix) {
		return this.getUsage(commandPrefix, this.name);
	}

	/**
	 * Gets the command usage in a specific locale.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @param label         The label to use.
	 * @return The command usage string.
	 */
	public String getUsage(String commandPrefix, String label) {
		return this.getUsage(commandPrefix, label, LocalizationManager.getInstance().getLocale("en"));
	}

	/**
	 * Gets the command usage in a specific locale.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @param label         The command label to use.
	 * @param locale        The locale to use.
	 * @return The command usage string.
	 */
	public String getUsage(String commandPrefix, String label, Locale locale) {
		final String phrase = String.format("discord.cmd.%s.usage", this.name);

		return locale.doesPhraseExist(phrase) ? locale.getTranslatedMessage(phrase, commandPrefix, label) : String.format("%s%s", commandPrefix, label);
	}

	/**
	 * Gets the command examples in English.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @return The examples as a string array.
	 */
	public String[] getExamples(String commandPrefix) {
		return this.getExamples(commandPrefix, this.name);
	}

	/**
	 * Gets the command examples in English.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @param label         The command label to use.
	 * @return The examples as a string array.
	 */
	public String[] getExamples(String commandPrefix, String label) {
		return this.getExamples(commandPrefix, label, LocalizationManager.getInstance().getLocale("en"));
	}

	/**
	 * Gets the command examples in a specific locale.
	 *
	 * @param commandPrefix The command prefix to use.
	 * @param label         The command label to use.
	 * @param locale        The locale to use.
	 * @return The examples as a string array.
	 */
	public String[] getExamples(String commandPrefix, String label, Locale locale) {
		ArrayList<String> examples = new ArrayList<String>();

		int i = 1;
		String format = "discord.cmd.%s.example.%s";

		while (locale.doesPhraseExist(String.format(format, this.name, i))) {
			examples.add(locale.getTranslatedMessage(String.format(format, this.name, i), commandPrefix, label));

			i++;
		}

		return examples.toArray(new String[]{});
	}
}
