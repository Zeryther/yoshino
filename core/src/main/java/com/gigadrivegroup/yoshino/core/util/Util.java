/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util;

import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class Util {
	public static final String ALPHANUMERIC_CHARACTERS_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String ALPHANUMERIC_CHARACTERS_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
	public static final String ALPHANUMERIC_CHARACTERS_NUMBERS = "0123456789";
	public static final String ALPHANUMERIC_CHARACTERS = new StringBuilder(ALPHANUMERIC_CHARACTERS_UPPERCASE).append(ALPHANUMERIC_CHARACTERS_LOWERCASE).append(ALPHANUMERIC_CHARACTERS_NUMBERS).toString();

	/**
	 * Formats a number with decimal and thousand separators.
	 *
	 * @param number The number to be formatted.
	 * @return The formatted number string.
	 */
	public static String formatNumber(int number) {
		return NumberFormat.getNumberInstance(Locale.US).format(number);
	}

	/**
	 * Formats a number with decimal and thousand separators.
	 *
	 * @param number The number to be formatted.
	 * @return The formatted number string.
	 */
	public static String formatNumber(long number) {
		return NumberFormat.getNumberInstance(Locale.US).format(number);
	}

	/**
	 * Formats a number with decimal and thousand separators.
	 *
	 * @param number The number to be formatted.
	 * @return The formatted number string.
	 */
	public static String formatNumber(double number) {
		return NumberFormat.getNumberInstance(Locale.US).format(number);
	}

	/**
	 * Turns a string list into a string by sticking it's elements together with a "glue" separator.
	 *
	 * @param pieces The list of strings that will be used.
	 * @param glue   The "glue" that will separate the pieces in the final string.
	 * @return The final string.
	 */
	public static String implode(List<String> pieces, String glue) {
		return implode(pieces.toArray(new String[]{}), glue);
	}

	/**
	 * Turns a string array into a string by sticking it's elements together with a "glue" separator.
	 *
	 * @param pieces The array of strings that will be used.
	 * @param glue   The "glue" that will separate the pieces in the final string.
	 * @return The final string.
	 */
	public static String implode(String[] pieces, String glue) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < pieces.length; i++) {
			sb.append(pieces[i]);

			if (!(i == pieces.length - 1)) {
				sb.append(glue);
			}
		}

		return sb.toString();
	}

	/**
	 * Returns whether a string is alpha numeric (contains of letters and numbers).
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isAlphaNumeric(String s) {
		for (char c : s.toCharArray()) if (!ALPHANUMERIC_CHARACTERS.contains(String.valueOf(c))) return false;

		return true;
	}

	/**
	 * Returns whether an integer is between two other integers (bigger than min, smaller than max).
	 *
	 * @param i   The integer to check.
	 * @param min The minimum value.
	 * @param max The maximum value.
	 * @return True or false.
	 */
	public static boolean isBetween(double i, double min, double max) {
		return i > min && i < max;
	}

	/**
	 * Returns whether an integer is between two other integers (bigger than min or equal to min, smaller than max or equal to max).
	 *
	 * @param i   The integer to check.
	 * @param min The minimum value.
	 * @param max The maximum value.
	 * @return True or false.
	 */
	public static boolean isInBetween(double i, double min, double max) {
		return i >= min && i <= max;
	}

	/**
	 * Returns whether a string can also be a double.
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isValidDouble(String s) {
		try {
			Double d = Double.parseDouble(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns whether a string can also be a float.
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isValidFloat(String s) {
		try {
			Float f = Float.parseFloat(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns whether a string can also be an integer.
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isValidInteger(String s) {
		try {
			Integer i = Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns whether a string is a valid JSON Object.
	 *
	 * @param s The JSON string.
	 * @return True or false.
	 */
	public static boolean isValidJsonObject(String s) {
		try {
			JsonElement element = new JsonParser().parse(s);
			if (element != null && element.isJsonObject()) {
				return true;
			}
		} catch (Exception e) {
			// ignore
		}

		return false;
	}

	/**
	 * Returns whether a string is a valid JSON Object of a class.
	 *
	 * @param s     The JSON string.
	 * @param clazz The class to parse the object with.
	 * @return True or false.
	 */
	public static boolean isValidJsonObject(String s, Class clazz) {
		try {
			Object o = SerializationUtil.GSON.fromJson(s, clazz);

			return o != null;
		} catch (Exception e) {
			// ignore
		}

		return false;
	}

	/**
	 * Returns whether a string can also be a long.
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isValidLong(String s) {
		try {
			Long l = Long.parseLong(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns whether a string can also be an URL.
	 *
	 * @param s The string to check.
	 * @return True or false.
	 */
	public static boolean isValidURL(String s) {
		try {
			new URL(s);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	/**
	 * Limits a string at a certain character amount.
	 *
	 * @param s     The string to be limited.
	 * @param limit The character limit.
	 * @return The final string.
	 */
	public static String limitString(String s, int limit) {
		if (s.length() > limit) {
			return s.substring(0, limit - 1);
		} else {
			return s;
		}
	}
}
