/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.manga;

import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import lombok.Getter;
import lombok.Setter;

public class MangaSearchResultData extends DataServiceSearchResult.Data {
	@Getter
	@Setter
	private String startDate;

	@Getter
	@Setter
	private String endDate;

	@Getter
	@Setter
	private int volumes;

	@Getter
	@Setter
	private int chapters;
}
