/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.config;

import com.gigadrivegroup.yoshino.core.EnvironmentType;
import com.gigadrivegroup.yoshino.core.shutdown.Shutdown;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.gson.stream.JsonReader;

import java.io.*;

public class ConfigLoader {
	public static void loadConfiguration() throws IOException {
		Logger.log(LoggerLevel.INFO, "Loading configuration...");

		final File configFile = new File("config.json");
		if (!configFile.exists()) {
			Logger.log(LoggerLevel.ERROR, "Configuration file does not exist!");
			Logger.log(LoggerLevel.INFO, "Creating configuration file...");

			try (Writer writer = new FileWriter(configFile)) {
				SerializationUtil.GSON_PRETTY_PRINT.toJson(new Config(), writer);
				Logger.log(LoggerLevel.INFO, "Configuration file created.");
			}

			Logger.log(LoggerLevel.INFO, "Please fill the configuration file before starting the bot.");
			Shutdown.shutdown();
		} else {
			Config config = SerializationUtil.GSON.fromJson(new JsonReader(new FileReader(configFile)), Config.class);
			Config.setInstance(config);

			Logger.getInstance().getOptions().setShowingDebugMessages(config.getEnvironmentType() != EnvironmentType.PRODUCTION);

			Logger.log(LoggerLevel.INFO, "Configuration loaded.");
		}
	}
}
