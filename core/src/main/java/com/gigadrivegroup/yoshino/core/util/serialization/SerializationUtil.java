/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.DBObject;

public class SerializationUtil {
	public static final Gson GSON;
	public static final Gson GSON_PRETTY_PRINT;

	static {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(DBObject.class, new DBObjectDeserializer());

		GSON = builder.create();
		GSON_PRETTY_PRINT = builder.setPrettyPrinting().create();
	}
}
