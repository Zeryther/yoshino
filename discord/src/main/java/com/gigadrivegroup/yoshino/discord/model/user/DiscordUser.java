/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.user;

import com.gigadrivegroup.yoshino.core.Platform;
import com.gigadrivegroup.yoshino.core.account.Account;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.CachedObjectField;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.gigadrivegroup.yoshino.discord.util.UserUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import lombok.Getter;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.User;

import java.util.Iterator;

public class DiscordUser extends CachedObject {
	/**
	 * @return The ID of this user.
	 */
	@Getter
	private long id;

	/**
	 * @return The name of this user.
	 */
	@Getter
	private CachedObjectField<String> name = new CachedObjectField<>();

	/**
	 * @return The discriminator of this user.
	 */
	@Getter
	private CachedObjectField<String> discriminator = new CachedObjectField<>();

	/**
	 * @return The avatar URL of this user.
	 */
	@Getter
	private CachedObjectField<String> avatarUrl = new CachedObjectField<>();

	/**
	 * @return The email address of this user.
	 */
	@Getter
	private CachedObjectField<String> email = new CachedObjectField<>();

	public DiscordUser(long id) {
		this.id = id;

		DatabaseManager manager = DatabaseManager.getInstance();

		DBCursor cursor = manager.discordUsers().find(new BasicDBObject("_id", this.id));
		if (cursor.count() > 0) {
			DBObject result = cursor.one();

			this.id = manager.retrieveLong(result, "_id");
			this.name = new CachedObjectField<>(manager.retrieveString(result, "name"));
			this.discriminator = new CachedObjectField<>(manager.retrieveString(result, "discriminator"));
			this.avatarUrl = new CachedObjectField<>(manager.retrieveString(result, "avatarUrl"));
			this.email = new CachedObjectField<>(manager.retrieveString(result, "email"));

			this.loadDataFromJDA();

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for user %s (%s#%s)", this.id, this.name.get(), this.discriminator.get());
		} else {
			if (this.isOnThisInstance()) {
				try {
					this.loadDataFromJDA();

					manager.discordUsers().insert(this.toDBObject());

					getStorage().add(this);

					Logger.log(LoggerLevel.DEBUG, "Loaded data for user %s (%s#%s)", this.id, this.name.get(), this.discriminator.get());
				} catch (DuplicateKeyException e) {
					getUser(this.id); // retry
				}
			}
		}

		cursor.close();
	}

	/**
	 * Gets a DiscordUser object by it's ID.
	 *
	 * @param id The ID of the user.
	 * @return The DiscordUser object, null if it does not exist.
	 */
	public static DiscordUser getUser(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();
				if (object instanceof DiscordUser) {
					DiscordUser o = (DiscordUser) object;

					if (o.id == id) {
						return o;
					}
				}
			}
		}

		DiscordUser g = new DiscordUser(id);
		return getStorage().contains(g) ? g : null;
	}

	/**
	 * Gets a DiscordUser object by it's JDA object.
	 *
	 * @param user The JDA object of the user.
	 * @return The DiscordUser object, null if it does not exist.
	 */
	public static DiscordUser getUser(User user) {
		return user != null && UserUtil.isHuman(user) ? getUser(user.getIdLong()) : null;
	}

	/**
	 * Gets the Account that this Discord account is linked with.
	 *
	 * @return The Account object, null if this Account has not been linked.
	 */
	public Account getAccount() {
		return Account.getAccountByLinkedAccountId(String.valueOf(this.id), Platform.DISCORD);
	}

	/**
	 * Returns whether the shard of this user is on the current instance.
	 *
	 * @return True or false.
	 */
	public boolean isOnThisInstance() {
		return this.toJDAUser() != null;
	}

	/**
	 * Updates the data by getting it from JDA, does not work if the user is on another instance.
	 */
	public void loadDataFromJDA() {
		loadDataFromJDA(this.toJDAUser());
	}

	/**
	 * Updates the data by getting it from JDA, does not work if the user is on another instance.
	 *
	 * @param user The {@link User} object to load the data from.
	 */
	public void loadDataFromJDA(User user) {
		if (user == null) return;
		if (user.getIdLong() != this.id) return;

		this.name.set(user.getName());
		this.discriminator.set(user.getDiscriminator());
		this.avatarUrl.set(user.getEffectiveAvatarUrl());

		this.saveData();
	}

	/**
	 * Saves the object data to the database, skipped if there is no data to save.
	 */
	public void saveData() {
		DatabaseManager manager = DatabaseManager.getInstance();
		super.saveData(manager.discordUsers());
	}

	/**
	 * Returns a string that may be used to mention this user in Discord's chat.
	 *
	 * @return The mention string.
	 */
	public String toMention() {
		return String.format("<@%s>", this.id);
	}

	/**
	 * Returns the associated {@link User} object.
	 *
	 * @return The user instance, null if the User could not be found (e.g. on another instance).
	 */
	public User toJDAUser() {
		ShardHandler handler = ShardHandler.getInstance();

		Iterator<JDA> iterator = handler.getShards().iterator();
		while (iterator.hasNext()) {
			JDA shard = iterator.next();

			User user = shard.getUserById(this.id);
			if (user != null) {
				return user;
			}
		}

		return null;
	}
}
