/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.module;

import lombok.Getter;
import lombok.Setter;

public class Module {
	/**
	 * @param name The name of the module
	 * @return name The name of the module
	 */
	@Getter
	@Setter
	private String name;

	/**
	 * @param metricsCollector The new metrics collector class
	 * @return The metrics collector of this module
	 */
	@Getter
	@Setter
	private ModuleMetricsCollector metricsCollector;

	public Module(String name) {
		this.name = name;
	}

	/**
	 * Starts the module.
	 *
	 * @throws Exception
	 */
	public void start() throws Exception {

	}

	/**
	 * Stops the module.
	 *
	 * @throws Exception
	 */
	public void stop() throws Exception {

	}

	/**
	 * Restarts the module.
	 *
	 * @throws Exception
	 */
	public void restart() throws Exception {
		stop();
		start();
	}
}
