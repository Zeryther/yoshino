/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.i18n;

import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class LocalizationManager {
	/**
	 * @return The LocalizationManager instance.
	 */
	@Setter
	@Getter
	private static LocalizationManager instance;

	/**
	 * @return The list of loaded locales.
	 */
	@Getter
	private ArrayList<Locale> locales;

	public LocalizationManager() {
		this.locales = new ArrayList<Locale>();

		try {
			ClassLoader classLoader = getClass().getClassLoader();

			for (File localeFolder : new File(classLoader.getResource("Locales").getPath()).listFiles()) {
				if (!localeFolder.isDirectory()) continue;

				String code = localeFolder.getName();

				for (File file : localeFolder.listFiles()) {
					if (file.getName().equals("translation.json")) {
						Locale locale = new Locale(code, file);
						this.locales.add(locale);

						Logger.log(LoggerLevel.DEBUG, "Loading %s (%s)", locale.getName(), locale.getCode());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets a locale object by it's language code.
	 *
	 * @param code The language code.
	 * @return The locale object, null if none could be found.
	 */
	public Locale getLocale(String code) {
		Iterator<Locale> iterator = this.locales.iterator();
		while (iterator.hasNext()) {
			Locale locale = iterator.next();

			if (locale.getCode().equalsIgnoreCase(code)) {
				return locale;
			}
		}

		return null;
	}

	/**
	 * Gets a translated message of a language by it's phrase identifier.
	 *
	 * @param code      The preferred language code.
	 * @param phrase    The phrase id of the message.
	 * @param variables The variables of the message.
	 * @return The translated message, returns the english translation as a fallback or the phrase identifier if no message could be found.
	 */
	public String getTranslatedMessage(String code, String phrase, Object... variables) {
		Validate.notNull(code, "The language code may not be null.");
		Validate.notEmpty(code, "The language code may not be empty.");

		Validate.notNull(code, "The phrase identifier may not be null.");
		Validate.notEmpty(code, "The phrase identifier may not be empty.");

		phrase = phrase.toLowerCase();

		String message = null;

		Locale locale = this.getLocale(code);
		if (locale != null) {
			if (locale.getPhrases().containsKey(phrase)) {
				message = locale.getPhrases().get(phrase);
			}
		}

		if (message == null && !code.equalsIgnoreCase("en")) {
			Locale english = this.getLocale("en");

			if (english != null) {
				if (locale.getPhrases().containsKey(phrase)) {
					message = locale.getPhrases().get(phrase);
				}
			}
		}

		if (message != null) {
			for (int i = 0; i < variables.length; i++) {
				Object variable = variables[i];

				message = message.replace(String.format("{%s}", i), String.valueOf(variable));
			}

			return message;
		} else {
			return phrase;
		}
	}
}
