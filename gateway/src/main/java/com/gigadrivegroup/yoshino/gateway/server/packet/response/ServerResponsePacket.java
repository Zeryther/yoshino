/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.packet.response;

import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import com.gigadrivegroup.yoshino.gateway.server.packet.ServerPacket;
import lombok.Getter;
import lombok.Setter;

/**
 * A packet sent by the client to the server, after receiving a {@link com.gigadrivegroup.yoshino.gateway.server.packet.request.ServerRequestPacket}.
 */
public class ServerResponsePacket extends ServerPacket {
	/**
	 * @param status The status of this packet.
	 * @return The status of this packet.
	 */
	@Getter
	@Setter
	private PacketResponseStatus status;

	public ServerResponsePacket(PacketResponseStatus status) {
		this.status = status;
	}
}
