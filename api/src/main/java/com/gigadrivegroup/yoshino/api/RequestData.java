/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api;

import com.gigadrivegroup.yoshino.core.account.Account;
import com.gigadrivegroup.yoshino.core.account.AccountToken;
import lombok.Getter;
import org.bson.types.ObjectId;
import spark.Request;

import java.util.Date;
import java.util.HashMap;

public class RequestData {
	@Getter
	private Account account;

	@Getter
	private AccountToken token;

	@Getter
	private HashMap<String, String> data;

	public RequestData(Request request) {
		this.data = API.getRequestDataParameters(request);

		if (request.headers("Authorization") != null) {
			String authorizationHeader = request.headers("Authorization");
			final String tokenPrefix = "Token ";

			if (authorizationHeader.startsWith(tokenPrefix)) {
				String tokenString = authorizationHeader.substring(tokenPrefix.length());
				this.token = AccountToken.getToken(new ObjectId(tokenString));

				if (this.token != null) {
					this.token.getUserAgent().set(request.userAgent());
					this.token.getIp().set(request.ip());
					this.token.getLastAccess().set(new Date(System.currentTimeMillis()));

					this.account = this.token.getAccount();
				}
			}
		}
	}
}
