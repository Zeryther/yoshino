/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.core.util.ComponentUtil;
import com.gigadrivegroup.yoshino.discord.commands.CommandManager;
import com.gigadrivegroup.yoshino.discord.listener.gateway.DiscordGuildSettingsReloadListener;
import com.gigadrivegroup.yoshino.discord.listener.gateway.DiscordShardDataListener;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.*;
import com.gigadrivegroup.yoshino.discord.packet.request.DiscordShardDataRequestPacket;
import com.gigadrivegroup.yoshino.discord.shard.GlobalShardHandler;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.gigadrivegroup.yoshino.discord.util.ApiUtil;
import com.gigadrivegroup.yoshino.gateway.client.GatewayClient;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListenerManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

import java.util.ArrayList;
import java.util.Arrays;

public class Discord extends Module {
	private ArrayList<Class<? extends PacketListener>> gatewayListeners;

	public Discord() {
		super("discord");

		this.gatewayListeners = new ArrayList<Class<? extends PacketListener>>();

		this.addGatewayListeners();
	}

	private void addGatewayListeners() {
		this.gatewayListeners.add(DiscordGuildSettingsReloadListener.class);
		this.gatewayListeners.add(DiscordShardDataListener.class);
	}

	@Override
	public void start() throws Exception {
		super.start();
		super.setMetricsCollector(new DiscordMetricsCollector());

		Module module = ModuleStorage.getInstance().getModule("gateway-client");
		if (module != null) {
			this.gatewayListeners.forEach(listener -> {
				try {
					PacketListenerManager.registerListener(listener);

					DiscordShardDataRequestPacket packet = new DiscordShardDataRequestPacket(Config.getInstance().getDiscordShardAmount());
					((GatewayClient) ModuleStorage.getInstance().getModule("gateway-client")).sendPacket(packet);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			if (ModuleStorage.getInstance().getModule("gateway-server") != null) {
				GlobalShardHandler.setInstance(new GlobalShardHandler());
			}

			ApiUtil.setInstance(new ApiUtil());

			// register components
			ComponentUtil.COMPONENTS.addAll(Arrays.asList(
					AnimeReactionComponent.class,
					BotSettingsComponent.class,
					FunComponent.class,
					MusicComponent.class,
					UtilityComponent.class
			));

			CommandManager.setInstance(new CommandManager());
		} else {
			Logger.log(LoggerLevel.WARN, "Failed to request shard data: Gateway client is not running!");
			this.restart();
			return;
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();

		Module module = ModuleStorage.getInstance().getModule("gateway-client");
		if (module != null) {
			this.gatewayListeners.forEach(listener -> {
				try {
					PacketListenerManager.unregisterListener(listener);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			CommandManager.setInstance(null);

			// unregister components
			ComponentUtil.COMPONENTS.removeAll(Arrays.asList(
					AnimeReactionComponent.class,
					BotSettingsComponent.class,
					FunComponent.class,
					MusicComponent.class,
					UtilityComponent.class
			));

			ApiUtil.setInstance(null);
		} else {
			Logger.log(LoggerLevel.WARN, "Failed to unregister packet listeners: Gateway client is not running!");
		}

		ShardHandler.getInstance().shutdown();

		GlobalShardHandler globalShardHandler = GlobalShardHandler.getInstance();
		if (globalShardHandler != null) {
			globalShardHandler.shutdown();
		}
	}
}
