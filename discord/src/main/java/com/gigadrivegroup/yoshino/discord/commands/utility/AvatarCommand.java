/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.utility;

import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandExecution;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.UtilityComponent;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.Arrays;

public class AvatarCommand extends Command {
	public AvatarCommand() {
		super("avatar", UtilityComponent.class);

		setAliases(Arrays.asList("profilepicture", "picture", "avi", "pfp"));
	}

	@Override
	public void execute(CommandExecution execution) {
		DiscordUser target = execution.getTarget();

		if (target != null) {
			String url = target.getAvatarUrl().get().concat("?size=1024");

			execution.reply(
					new EmbedBuilder()
							.setColor(Constants.YOSHINO_EMBED_COLOR)
							.setDescription("[".concat(execution.t("discord.cmd.avatar.clickForSource")).concat("](").concat(url).concat(")"))
							.setImage(url)
			);
		} else {
			execution.reply(execution.t("discord.targetNotFound"));
		}
	}
}
