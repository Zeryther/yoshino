/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.account;

import com.gigadrivegroup.yoshino.core.Platform;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.CachedObjectField;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.gson.reflect.TypeToken;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class Account extends CachedObject {
	/**
	 * @return The id of this account.
	 */
	@Getter
	@Setter
	private ObjectId id;

	/**
	 * @return The link data for this account's Discord account, null if no Discord account is linked.
	 */
	@Getter
	private CachedObjectField<HashMap<Platform, DBObject>> linkedAccounts = new CachedObjectField<>(new HashMap<>());

	/**
	 * @return The timestamp of when this account was created.
	 */
	@Getter
	private CachedObjectField<Date> timeCreated;

	public Account(ObjectId id) {
		this.id = id;

		DatabaseManager manager = DatabaseManager.getInstance();

		DBCursor cursor = manager.accounts().find(new BasicDBObject("_id", id));
		if (cursor.count() > 0) {
			DBObject result = cursor.one();

			this.linkedAccounts = new CachedObjectField<>(manager.retrieveHashMapOfType(Platform.class, DBObject.class, result, "linkedAccounts", new HashMap<>()));
			this.timeCreated = new CachedObjectField<>((Date) manager.retrieveObject(result, "timeCreated", new Date(System.currentTimeMillis())));

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for account %s", this.id);
		} else {
			this.timeCreated = new CachedObjectField<>(new Date(System.currentTimeMillis()));
			this.timeCreated.setModified(true);

			manager.accounts().insert(this.toDBObject());

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for account %s", this.id);
		}

		cursor.close();
	}

	/**
	 * Gets an Account object by it's ID.
	 *
	 * @param id The ID of the account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Account getAccount(ObjectId id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Account) {
					Account o = ((Account) object);

					if (o.id.equals(id)) {
						return o;
					}
				}
			}
		}

		Account a = new Account(id);
		return getStorage().contains(a) ? a : null;
	}

	/**
	 * Gets an Account object by the ID of the account it's linked with
	 *
	 * @param id       The ID of the Discord account.
	 * @param platform The the passed user ID matches to.
	 * @return The Account object, null if it does not exist.
	 */
	public static Account getAccountByLinkedAccountId(String id, Platform platform) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Account) {
					Account o = ((Account) object);

					AccountLinkData accountLinkData = o.getLinkedAccount(platform, AccountLinkData.class);
					if (accountLinkData != null) {
						if (accountLinkData.getId() != null && accountLinkData.getId().equals(id)) {
							return o;
						}
					}
				}
			}
		}

		Account account = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		//DBCursor cursor = manager.accounts().find(new BasicDBObject("linkedAccounts", new BasicDBObject("$exists", true).append("$elemMatch", new BasicDBObject("platform", platform.toString()).append("userId", id))));
		DBCursor cursor = manager.accounts().find(new BasicDBObject(String.format("linkedAccounts.%s.id", platform.toString()), id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			account = Account.getAccount((ObjectId) result.get("_id"));
		}

		cursor.close();

		return account;
	}

	/**
	 * Creates a new account and saves it to the database.
	 *
	 * @return The Account object.
	 */
	public static Account createAccount() {
		return new Account(ObjectId.get());
	}

	/**
	 * Adds a linked account to this account and saves it to the database. If there already is a linked account with the platform of the passed account, it will be replaced.
	 *
	 * @param accountLinkData The linked account to save.
	 */
	public void addLinkedAccount(AccountLinkData accountLinkData) {
		Platform platform = accountLinkData.getPlatform();

		if (!this.linkedAccounts.is()) {
			this.linkedAccounts.set(new HashMap<>());
		}

		this.linkedAccounts.get().remove(platform);

		if (this.getPrimaryLinkedAccount() == null) {
			accountLinkData.setPrimary(true);
		}

		this.linkedAccounts.get().put(platform, BasicDBObject.parse(SerializationUtil.GSON.toJson(accountLinkData)));
		this.linkedAccounts.setModified(true);
	}

	/**
	 * Gets all linked accounts in an array.
	 *
	 * @return An array holding all linked accounts.
	 */
	public AccountLinkData[] getLinkedAccounts() {
		ArrayList<AccountLinkData> accounts = new ArrayList<>();

		if (this.linkedAccounts.is()) {
			this.linkedAccounts.get().forEach((platform, object) -> {
				try {
					accounts.add(SerializationUtil.GSON.fromJson(JSON.serialize(object), TypeToken.get(Class.forName(platform.getLinkDataClassName())).getType()));
				} catch (Exception e) {
					e.printStackTrace(); // TODO
				}
			});
		}

		return accounts.toArray(new AccountLinkData[]{});
	}

	/**
	 * Gets the linked account of the passed platform.
	 *
	 * @param platform The platform to look for.
	 * @return The {@link AccountLinkData} object of the linked account, null if not found.
	 */
	public <T extends AccountLinkData> T getLinkedAccount(Platform platform, Class<T> clazz) {
		DBObject object = this.linkedAccounts.get().getOrDefault(platform, null);

		if (object != null) {
			return SerializationUtil.GSON.fromJson(JSON.serialize(object), clazz);
		}

		return null;
	}

	/**
	 * Gets the primary linked account.
	 *
	 * @return The {@link AccountLinkData} object of the primary linked account, <em>should</em> not be null.
	 */
	public AccountLinkData getPrimaryLinkedAccount() {
		for (AccountLinkData accountLinkData : this.getLinkedAccounts()) {
			if (accountLinkData.isPrimary()) return accountLinkData;
		}

		return null;
	}

	/**
	 * Saves the object data to the database, skipped if there is no data to save.
	 */
	public void saveData() {
		DatabaseManager manager = DatabaseManager.getInstance();
		super.saveData(manager.accounts());
	}
}
