/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.account;

import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.CachedObjectField;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.Iterator;

public class AccountToken extends CachedObject {
	/**
	 * @return The token id (the token string itself).
	 */
	@Getter
	@Setter
	private ObjectId id;

	/**
	 * @return The ID of the account that is associated with this token.
	 */
	@Getter
	private CachedObjectField<ObjectId> accountId;

	/**
	 * @return The last user agent that was used with this token.
	 */
	@Getter
	private CachedObjectField<String> userAgent;

	/**
	 * @return The last IP that was used with this token.
	 */
	@Getter
	private CachedObjectField<String> ip;

	/**
	 * @return The date of when this token was created.
	 */
	@Getter
	private CachedObjectField<Date> timeCreated;

	/**
	 * @return The date of when this token was last used.
	 */
	@Getter
	private CachedObjectField<Date> lastAccess;

	public AccountToken(ObjectId id) {
		this.id = id;

		DatabaseManager manager = DatabaseManager.getInstance();

		DBCursor cursor = manager.tokens().find(new BasicDBObject("_id", id));
		if (cursor.count() > 0) {
			DBObject result = cursor.one();

			this.accountId = new CachedObjectField<>(manager.retrieveObjectId(result, "accountId"));
			this.userAgent = new CachedObjectField<>(manager.retrieveString(result, "userAgent"));
			this.ip = new CachedObjectField<>(manager.retrieveString(result, "ip"));
			this.timeCreated = new CachedObjectField<>((Date) manager.retrieveObject(result, "timeCreated", new Date(System.currentTimeMillis())));
			this.lastAccess = new CachedObjectField<>((Date) manager.retrieveObject(result, "lastAccess", new Date(System.currentTimeMillis())));

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for token %s", this.id);
		}

		cursor.close();
	}

	/**
	 * Creates a new token and saves it to the database.
	 *
	 * @param account The account to be bound with the new token.
	 * @return The AccountToken object.
	 */
	public static AccountToken createToken(Account account) {
		ObjectId id = ObjectId.get();

		DatabaseManager manager = DatabaseManager.getInstance();
		manager.tokens().insert(new BasicDBObject("_id", id).append("accountId", account.getId()).append("timeCreated", new Date(System.currentTimeMillis())));

		return getToken(id);
	}

	/**
	 * Gets an AccountToken object by it's ID.
	 *
	 * @param id The ID of the token.
	 * @return The AccountToken object, null if it does not exist.
	 */
	public static AccountToken getToken(ObjectId id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof AccountToken) {
					AccountToken o = ((AccountToken) object);

					if (o.id.equals(id)) {
						return o;
					}
				}
			}
		}

		AccountToken a = new AccountToken(id);
		return getStorage().contains(a) ? a : null;
	}

	/**
	 * @return The account that owns this token.
	 */
	public Account getAccount() {
		return Account.getAccount(this.accountId.get());
	}

	/**
	 * Saves the object data to the database, skipped if there is no data to save.
	 */
	public void saveData() {
		DatabaseManager manager = DatabaseManager.getInstance();
		super.saveData(manager.tokens());
	}

	@Override
	public String toString() {
		return this.id.toString();
	}
}
