/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.packet.handler;

import lombok.Getter;

import java.lang.reflect.Method;

public class PacketListenerData {
	/**
	 * @return The PacketListener instance.
	 */
	@Getter
	private PacketListener listener;

	/**
	 * @return The PacketHandler method.
	 */
	@Getter
	private Method method;

	/**
	 * Constructor
	 *
	 * @param listener The PacketListener instance.
	 * @param method   The PacketHandler method.
	 */
	public PacketListenerData(PacketListener listener, Method method) {
		this.listener = listener;
		this.method = method;
	}

	/**
	 * @return The name of the packet type associated with this listener.
	 */
	public String getPacketType() {
		return new StringBuilder(method.getParameterTypes()[0].getPackageName()).append(".").append(method.getParameterTypes()[0].getSimpleName()).toString();
	}
}
