/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands.reader;

import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommand;
import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommandManager;
import lombok.Getter;

import java.util.Arrays;

public class ConsoleCommandParser {
	/**
	 * Parses console input to trigger the proper command.
	 *
	 * @param input The string input from the console.
	 * @return The {@link ConsoleCommandParser.ParsedData} instance that holds all the parse data, null if no input was passed.
	 */
	public ParsedData parseData(String input) {
		if (input != null) {
			input = input.trim();

			if (input.length() > 0) {
				String[] split = input.split(" ");

				if (split.length > 0) {
					String label = split[0];
					String[] args = split.length > 1 ? Arrays.copyOfRange(split, 1, split.length - 1) : new String[]{}; // create sub array, cutting label off to get arguments (use empty array if no arguments were passed)

					return new ParsedData(label, args);
				}
			}
		}

		return null;
	}

	public static class ParsedData {
		/**
		 * @return The name/alias used to invoke this command.
		 */
		@Getter
		private String label;

		/**
		 * @return The arguments used after the command label.
		 */
		@Getter
		private String[] args;

		/**
		 * Constructor
		 *
		 * @param label The name/alias used to invoke this command.
		 * @param args  The arguments used after the command label.
		 */
		public ParsedData(String label, String[] args) {
			this.label = label;
			this.args = args;
		}

		/**
		 * Gets the command that was invoked by this input.
		 *
		 * @return The command instance, null if no command was found.
		 */
		public ConsoleCommand getCommand() {
			return ConsoleCommandManager.getInstance().getCommand(this.label, true);
		}
	}
}
