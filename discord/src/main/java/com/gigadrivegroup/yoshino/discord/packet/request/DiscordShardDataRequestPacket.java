/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.packet.request;

import com.gigadrivegroup.yoshino.gateway.client.packet.request.ClientRequestPacket;
import lombok.Getter;

public class DiscordShardDataRequestPacket extends ClientRequestPacket {
	/**
	 * @return The amount of shards to request.
	 */
	@Getter
	private int shardAmount;

	public DiscordShardDataRequestPacket(int shardAmount) {
		super();

		this.shardAmount = shardAmount;
	}
}
