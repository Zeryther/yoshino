/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util.serialization;

import com.google.gson.*;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import java.lang.reflect.Type;

public class DBObjectDeserializer implements JsonDeserializer<DBObject> {
	@Override
	public DBObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		return (DBObject) JSON.parse(new Gson().toJson(json));
	}
}
