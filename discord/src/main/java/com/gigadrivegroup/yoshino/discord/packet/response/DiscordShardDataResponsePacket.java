/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.packet.response;

import com.gigadrivegroup.yoshino.gateway.client.packet.response.ClientResponsePacket;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import lombok.Getter;

public class DiscordShardDataResponsePacket extends ClientResponsePacket {
	/**
	 * @return The shard total value.
	 */
	@Getter
	private int shardTotal;

	/**
	 * @return The shard IDs.
	 */
	@Getter
	private Integer[] shardIDs;

	public DiscordShardDataResponsePacket(PacketResponseStatus status, Integer[] shardIDs, int shardTotal) {
		super(status);

		this.shardIDs = shardIDs;
		this.shardTotal = shardTotal;
	}
}
