/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.account.platforms;

import com.gigadrivegroup.yoshino.core.Platform;
import com.gigadrivegroup.yoshino.core.account.AccountLinkData;
import lombok.Getter;
import lombok.Setter;

public class PatreonLinkData extends AccountLinkData {
	public PatreonLinkData() {
		super(Platform.PATREON);
	}

	/**
	 * @param clientId The new client ID.
	 * @return The client ID, may be null.
	 */
	@Getter
	@Setter
	private String clientId;

	/**
	 * @param clientSecret The new client secret.
	 * @return The client secret, may be null.
	 */
	@Getter
	@Setter
	private String clientSecret;

	/**
	 * @param accessToken The new access token.
	 * @return The access token, may be null.
	 */
	@Getter
	@Setter
	private String accessToken;

	/**
	 * @param refreshToken The new refresh token.
	 * @return The refresh token, may be null.
	 */
	@Getter
	@Setter
	private String refreshToken;

	/**
	 * @param userId The new user id.
	 * @return The user id, may be 0.
	 */
	@Getter
	@Setter
	private long userId;
}
