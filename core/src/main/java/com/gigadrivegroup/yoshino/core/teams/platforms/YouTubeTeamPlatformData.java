/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.teams.platforms;

import com.gigadrivegroup.yoshino.core.teams.TeamPlatformData;
import lombok.Getter;
import lombok.Setter;

public class YouTubeTeamPlatformData extends TeamPlatformData {
	/**
	 * @param userId The new user id.
	 * @return The user id, may be null.
	 */
	@Getter
	@Setter
	private String userId;
}
