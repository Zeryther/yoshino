/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.teams;

import com.gigadrivegroup.yoshino.core.account.Account;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.CachedObjectField;
import com.gigadrivegroup.yoshino.core.teams.platforms.*;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.bson.types.ObjectId;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;

public class Team extends CachedObject {
	/**
	 * @return The id of this platform link.
	 */
	@Getter
	@Setter
	private ObjectId id;

	/**
	 * @return The name of this team.
	 */
	@Getter
	@Setter
	private CachedObjectField<String> name;

	/**
	 * @return A list of the members of this team.
	 */
	@Getter
	private CachedObjectField<ArrayList<TeamMember>> members = new CachedObjectField<>();

	/**
	 * @return The link data for this team's Discord account, null if no Discord account is linked.
	 */
	@Getter
	private CachedObjectField<DiscordTeamPlatformData> discordLinkData = new CachedObjectField<>();

	/**
	 * @return The link data for this team's Patreon account, null if no Patreon account is linked.
	 */
	@Getter
	private CachedObjectField<PatreonTeamPlatformData> patreonLinkData = new CachedObjectField<>();

	/**
	 * @return The link data for this team's Streamlabs account, null if no Streamlabs account is linked.
	 */
	@Getter
	private CachedObjectField<StreamlabsTeamPlatformData> streamlabsLinkData = new CachedObjectField<>();

	/**
	 * @return The link data for this team's Tipeeestream account, null if no Tipeeestream account is linked.
	 */
	@Getter
	private CachedObjectField<TipeeeStreamTeamPlatformData> tipeeeStreamLinkData = new CachedObjectField<>();

	/**
	 * @return The link data for this team's Twitch account, null if no Twitch account is linked.
	 */
	@Getter
	private CachedObjectField<TwitchTeamPlatformData> twitchLinkData = new CachedObjectField<>();

	/**
	 * @return The link data for this team's YouTube account, null if no YouTube account is linked.
	 */
	@Getter
	private CachedObjectField<YouTubeTeamPlatformData> youTubeLinkData = new CachedObjectField<>();

	/**
	 * @return The timestamp of when this team was created.
	 */
	@Getter
	private CachedObjectField<Date> timeCreated;

	private TeamSettings settings;
	private long settingsCacheTime;

	private Team(ObjectId id) {
		this.id = id;

		DatabaseManager manager = DatabaseManager.getInstance();

		DBCursor cursor = manager.teams().find(new BasicDBObject("_id", id));
		if (cursor.count() > 0) {
			DBObject result = cursor.one();

			this.members = new CachedObjectField<>(manager.retrieveArrayListOfType(result, "members"));
			this.discordLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(DiscordTeamPlatformData.class, result, "discordLinkData"));
			this.patreonLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(PatreonTeamPlatformData.class, result, "patreonLinkData"));
			this.streamlabsLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(StreamlabsTeamPlatformData.class, result, "streamlabsLinkData"));
			this.tipeeeStreamLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(TipeeeStreamTeamPlatformData.class, result, "tipeeeStreamLinkData"));
			this.twitchLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(TwitchTeamPlatformData.class, result, "twitchLinkData"));
			this.youTubeLinkData = new CachedObjectField<>(manager.retrieveObjectOfType(YouTubeTeamPlatformData.class, result, "youTubeLinkData"));
			this.timeCreated = new CachedObjectField<>((Date) manager.retrieveObject(result, "timeCreated", new Date(System.currentTimeMillis())));

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for team %s", this.id);
		} else {
			this.name = new CachedObjectField<>();
			this.members = new CachedObjectField<>(new ArrayList<>());
			this.timeCreated = new CachedObjectField<>(new Date(System.currentTimeMillis()));

			manager.teams().insert(this.toDBObject());

			getStorage().add(this);

			Logger.log(LoggerLevel.DEBUG, "Loaded data for team %s", this.id);
		}

		cursor.close();
	}

	/**
	 * Creates a new team and saves it to the database.
	 *
	 * @param name  The name of the new team.
	 * @param owner The account to be saved as the new team's owner.
	 * @return The newly created Team, null if the team could not be created.
	 */
	public static Team createTeam(String name, Account owner) {
		Validate.notNull(name, "The name may not be null.");
		Validate.notEmpty(name, "The name may not be empty.");
		Validate.notNull(owner, "The owner may not be null.");

		ObjectId id = ObjectId.get();

		Team team = new Team(id);
		if (getStorage().contains(team)) {
			team.getName().set(name);

			team.getMembers().get().add(new TeamMember(owner.getId(), TeamMemberRank.OWNER));
			team.getMembers().setModified(true);

			return team;
		}

		return null;
	}

	/**
	 * Gets a Team object by it's ID.
	 *
	 * @param id The ID of the platform link.
	 * @return The Team object, null if it does not exist.
	 */
	public static Team getTeam(ObjectId id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.id.equals(id)) {
						return o;
					}
				}
			}
		}

		Team p = new Team(id);
		return getStorage().contains(p) ? p : null;
	}

	/**
	 * Gets an Account object by the ID of the Discord account it's linked with
	 *
	 * @param id The ID of the Discord account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByDiscordId(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.discordLinkData.is() && o.discordLinkData.get().getUserId() == id) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("discordLinkData", new BasicDBObject("$exists", true)).append("discordLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Gets an Account object by the ID of the Patreon account it's linked with
	 *
	 * @param id The ID of the Patreon account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByPatreonId(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.patreonLinkData.is() && o.patreonLinkData.get().getUserId() == id) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("patreonLinkData", new BasicDBObject("$exists", true)).append("patreonLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Gets an Account object by the ID of the Streamlabs account it's linked with
	 *
	 * @param id The ID of the Streamlabs account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByStreamlabsId(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.streamlabsLinkData.is() && o.streamlabsLinkData.get().getUserId() == id) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("streamlabsLinkData", new BasicDBObject("$exists", true)).append("streamlabsLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Gets an Account object by the ID of the TipeeeStream account it's linked with
	 *
	 * @param id The ID of the TipeeeStream account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByTipeeeStreamId(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.tipeeeStreamLinkData.is() && o.tipeeeStreamLinkData.get().getUserId() == id) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("tipeeeStreamLinkData", new BasicDBObject("$exists", true)).append("tipeeeStreamLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Gets an Account object by the ID of the Twitch account it's linked with
	 *
	 * @param id The ID of the Twitch account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByTwitchId(long id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.twitchLinkData.is() && o.twitchLinkData.get().getUserId() == id) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("twitchLinkData", new BasicDBObject("$exists", true)).append("twitchLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Gets an Account object by the ID of the YouTube account it's linked with
	 *
	 * @param id The ID of the YouTube account.
	 * @return The Account object, null if it does not exist.
	 */
	public static Team getTeamByYouTubeId(String id) {
		if (getStorage().size() > 0) {
			Iterator<? super CachedObject> iterator = getStorage().iterator();
			while (iterator.hasNext()) {
				Object object = iterator.next();

				if (object instanceof Team) {
					Team o = ((Team) object);

					if (o.youTubeLinkData.is() && o.youTubeLinkData.get().getUserId() != null && o.youTubeLinkData.get().getUserId().equals(id)) {
						return o;
					}
				}
			}
		}

		Team team = null;

		DatabaseManager manager = DatabaseManager.getInstance();
		DBCursor cursor = manager.teams().find(new BasicDBObject("youTubeLinkData", new BasicDBObject("$exists", true)).append("youTubeLinkData.userId", id));
		if (cursor.size() > 0) {
			DBObject result = cursor.one();

			team = Team.getTeam((ObjectId) result.get("_id"));
		}

		cursor.close();

		return team;
	}

	/**
	 * Returns the settings object from this platform link. The settings are fetched from the database and cached for a maximum of {@link com.gigadrivegroup.yoshino.core.util.Constants#OBJECT_CACHE_TIME}. If the settings are not stored in the database yet, a new object will be created and saved to the database.
	 *
	 * @return The settings object.
	 */
	public TeamSettings getSettings() {
		final long cacheTime = Constants.OBJECT_CACHE_TIME;

		if (this.settings == null || (System.currentTimeMillis() - this.settingsCacheTime) >= cacheTime) {
			DatabaseManager manager = DatabaseManager.getInstance();

			DBCursor cursor = manager.teamSettings().find(new BasicDBObject("_id", this.id));
			if (cursor.count() > 0) {
				DBObject result = cursor.one();

				this.settings = TeamSettings.fromDBObject(result);
			} else {
				this.settings = new TeamSettings(this.id);

				try {
					manager.teamSettings().insert(this.settings.toDBObject());
				} catch (DuplicateKeyException e) {
					// ignored
				}
			}

			this.settingsCacheTime = System.currentTimeMillis();

			cursor.close();
		}

		return this.settings;
	}
}
