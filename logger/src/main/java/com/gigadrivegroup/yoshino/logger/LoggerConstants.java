/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.logger;

public class LoggerConstants {
	/**
	 * Color string used to reset console color after using a color string of {@link LoggerLevel#getColorString() getColorString()}.
	 */
	public static String RESET_COLOR = "\u001B[0m";

	/**
	 * Color string used to color the name of the module in log messages
	 */
	public static String MODULE_COLOR = "\u001B[32m";

	/**
	 * Color string used to color the name of the calling class in log messages
	 */
	public static String CLASS_COLOR = "\u001B[36m";
}
