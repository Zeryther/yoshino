/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.util;

import com.gigadrivegroup.yoshino.core.components.Component;

import java.util.ArrayList;

public class ComponentUtil {
	public static ArrayList<Class<? extends Component>> COMPONENTS;

	static {
		COMPONENTS = new ArrayList<>();
	}

	/**
	 * Gets a component class from its type.
	 *
	 * @param componentType The component type.
	 * @return The component class, null if it could not be found.
	 */
	public static Class<? extends Component> getComponentClass(String componentType) {
		final String[] formats = new String[]{
				"com.gigadrivegroup.yoshino.discord.model.guild.settings.components.%s"
		};

		for (String format : formats) {
			try {
				Class clazz = Class.forName(String.format(format, componentType));

				if (clazz != null)
					return clazz;
			} catch (ClassNotFoundException ignored) {
				// ignored
			}
		}

		return null;
	}
}
