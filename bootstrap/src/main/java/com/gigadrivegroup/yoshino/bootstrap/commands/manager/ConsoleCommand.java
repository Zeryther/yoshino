/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands.manager;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

public class ConsoleCommand {
	/**
	 * @param name The new name of the command.
	 * @return The name of the command.
	 */
	@Getter
	@Setter
	private String name;
	/**
	 * @param description The new description of the command.
	 * @return The description of the command.
	 */
	@Getter
	@Setter
	private String description;
	/**
	 * @param aliases The new aliases of the command.
	 * @return The aliases of the command.
	 */
	@Getter
	@Setter
	private List<String> aliases;

	/**
	 * The command contructor.
	 *
	 * @param name The name for this command.
	 */
	public ConsoleCommand(String name) {
		this.name = name;

		this.aliases = Arrays.asList();
	}

	/**
	 * Executes the command.
	 *
	 * @param label The name/alias used to invoke this command.
	 * @param args  The arguments used after the command label.
	 */
	public void execute(String label, String[] args) {

	}
}
