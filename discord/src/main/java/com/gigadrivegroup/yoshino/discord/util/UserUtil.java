/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.util;

import net.dv8tion.jda.core.entities.User;

public class UserUtil {
	/**
	 * Checks whether the passed user is human (not a bot or webhook).
	 *
	 * @param user The user to check.
	 * @return True or false.
	 */
	public static boolean isHuman(User user) {
		return !user.isBot() && !user.isFake() && !user.getDiscriminator().equals("0000");
	}
}
