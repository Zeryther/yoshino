/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.config;

import com.gigadrivegroup.yoshino.core.EnvironmentType;
import lombok.Getter;
import lombok.Setter;

public class Config {
	/**
	 * @return The config instance holding configuration values for Yoshino
	 */
	@Getter
	@Setter
	private static Config instance;

	//////////////////////////////////////////////////////////////////
	// API
	//////////////////////////////////////////////////////////////////

	@Getter
	private int apiServerPort = 3079;

	/**
	 * @return The client ID associated with the Streamlabs OAuth2 application
	 */
	@Getter
	private String streamlabsClientId = "";

	/**
	 * @return The client secret associated with the Streamlabs OAuth2 application
	 */
	@Getter
	private String streamlabsClientSecret = "";

	/**
	 * @return The redirect URL associated with the Streamlabs OAuth2 application
	 */
	@Getter
	private String streamlabsRedirectUrl = "";

	/**
	 * @return The client ID associated with the TipeeeStream OAuth2 application
	 */
	@Getter
	private String tipeeeStreamClientId = "";

	/**
	 * @return The client secret associated with the TipeeeStream OAuth2 application
	 */
	@Getter
	private String tipeeeStreamClientSecret = "";

	/**
	 * @return The redirect URL associated with the TipeeeStream OAuth2 application
	 */
	@Getter
	private String tipeeeStreamRedirectUrl = "";

	/**
	 * @return The client ID associated with the Patreon OAuth2 application
	 */
	@Getter
	private String patreonClientId = "";

	/**
	 * @return The client secret associated with the Patreon OAuth2 application
	 */
	@Getter
	private String patreonClientSecret = "";

	/**
	 * @return The redirect URL associated with the Patreon OAuth2 application
	 */
	@Getter
	private String patreonRedirectURL = "";

	//////////////////////////////////////////////////////////////////
	// CORE
	//////////////////////////////////////////////////////////////////

	/**
	 * @return The environment type used to identify what type of Yoshino is being used
	 */
	@Getter
	private EnvironmentType environmentType = EnvironmentType.DEVELOPMENT;

	/**
	 * @return An array of module names, all modules in this array will not be loaded on startup
	 */
	@Getter
	private String[] disabledModules = new String[]{};

	/**
	 * @return The URI that is used to connect to the MongoDB server
	 */
	@Getter
	private String mongoURI = "mongodb://127.0.0.1:27017";

	/**
	 * @return The name of the MongoDB database that is being used.
	 */
	@Getter
	private String mongoDatabaseName = "yoshino";

	/**
	 * @return The prefix used for finding the collections in the {@link Config#mongoDatabaseName MongoDB database}.
	 */
	@Getter
	private String mongoCollectionPrefix = "";

	//////////////////////////////////////////////////////////////////
	// DISCORD
	//////////////////////////////////////////////////////////////////

	/**
	 * @return The token used to authenticate the Yoshino discord bot
	 */
	@Getter
	private String discordBotToken = "";

	/**
	 * @return The client ID associated with the Discord bot and OAuth2 application
	 */
	@Getter
	private String discordClientId = "";

	/**
	 * @return The client secret associated with the Discord bot and OAuth2 application
	 */
	@Getter
	private String discordClientSecret = "";

	/**
	 * @return The redirect URL associated with the Discord bot and OAuth2 application
	 */
	@Getter
	private String discordRedirectURL = "";

	/**
	 * @return The amount of discord shards this JVM should hold
	 */
	@Getter
	private int discordShardAmount = 1;

	/**
	 * @return The API token for
	 */
	@Getter
	private String weebToken = "";

	/**
	 * @return Whether discord components should be enabled by default.
	 */
	@Getter
	private boolean componentEnabledByDefault = false;

	/**
	 * @return The maximum results for search results (!anime, !manga, !play etc.)
	 */
	@Getter
	private int maximumDataServiceSearchResults = 5;

	/**
	 * @return The maximum amount of characters for a search result description.
	 */
	@Getter
	private int dataServiceDescriptionLimit = 900;

	/**
	 * @return The Simkl.com API client ID.
	 */
	@Getter
	private String simklClientId = "";

	/**
	 * @return The Simkl.com API client secret.
	 */
	@Getter
	private String simklClientSecret = "";

	//////////////////////////////////////////////////////////////////
	// GATEWAY
	//////////////////////////////////////////////////////////////////

	/**
	 * @return The host used by the socket gateway
	 */
	@Getter
	private String gatewayHost = "127.0.0.1";

	/**
	 * @return The port number used by the socket gateway
	 */
	@Getter
	private int gatewayPort = 53257;

	/**
	 * @return This token is being used to authorize Yoshino instances on the gateway.
	 */
	@Getter
	private String gatewayAuthToken = "";

	//////////////////////////////////////////////////////////////////
	// TWITCH
	//////////////////////////////////////////////////////////////////

	/**
	 * @return The client ID associated with the Twitch bot and OAuth2 application
	 */
	@Getter
	private String twitchClientId = "";

	/**
	 * @return The client secret associated with the Twitch bot and OAuth2 application
	 */
	@Getter
	private String twitchClientSecret = "";

	/**
	 * @return The redirect URL associated with the Twitch bot and OAuth2 application
	 */
	@Getter
	private String twitchRedirectURL = "";

	//////////////////////////////////////////////////////////////////
	// YOUTUBE
	//////////////////////////////////////////////////////////////////

	/**
	 * @return The client ID associated with the YouTube bot and OAuth2 application
	 */
	@Getter
	private String youtubeClientId = "";

	/**
	 * @return The client secret associated with the YouTube bot and OAuth2 application
	 */
	@Getter
	private String youtubeClientSecret = "";

	/**
	 * @return The redirect URL associated with the YouTube bot and OAuth2 application
	 */
	@Getter
	private String youtubeRedirectURL = "";
}
