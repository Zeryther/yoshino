/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client.packet.response.auth;

import com.gigadrivegroup.yoshino.gateway.client.packet.response.ClientResponsePacket;
import com.gigadrivegroup.yoshino.gateway.packet.PacketResponseStatus;
import lombok.Getter;

import java.util.UUID;

/**
 * A packet sent by the server to the client after receiving a {@link com.gigadrivegroup.yoshino.gateway.client.packet.request.auth.ClientAuthorizationRequestPacket}.
 */
public class ClientAuthorizationResponsePacket extends ClientResponsePacket {
	/**
	 * @param connectionUUID The UUID of this connection.
	 * @return The UUID of this connection.
	 */
	@Getter
	private UUID connectionUUID;

	public ClientAuthorizationResponsePacket(PacketResponseStatus status, UUID connectionUUID) {
		super(status);

		this.connectionUUID = connectionUUID;
	}
}
