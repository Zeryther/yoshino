/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListener;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListenerManager;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;

public class GatewayServer extends Module {
	/**
	 * @return The server socket instance
	 */
	@Getter
	private ServerSocket serverSocket;

	/**
	 * @return The gateway listener instance
	 */
	@Getter
	private GatewayListener gatewayListener;

	private ArrayList<Class<? extends PacketListener>> listeners;

	public GatewayServer() {
		super("gateway-server");

		this.listeners = new ArrayList<Class<? extends PacketListener>>();
		this.addListeners();
	}

	private void addListeners() {
		this.listeners.add(ServerPacketListener.class);
	}

	@Override
	public void start() throws Exception {
		final String host = Config.getInstance().getGatewayHost();
		final int port = Config.getInstance().getGatewayPort();

		Validate.notNull(host, "The socket host may not be null.");
		Validate.notEmpty(host, "The socket host may not be empty.");

		Validate.isTrue(port <= 65535, "The port number may not be higher than 65535.");
		Validate.isTrue(port >= 0, "The port number may not be lower than 0.");

		ClientConnectionManager.setInstance(new ClientConnectionManager());

		this.listeners.forEach(listener -> {
			try {
				PacketListenerManager.registerListener(listener);
				Logger.log(LoggerLevel.DEBUG, "Registered listener: %s", listener.getSimpleName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		this.serverSocket = new ServerSocket();
		this.serverSocket.bind(new InetSocketAddress(host, port));

		this.gatewayListener = new GatewayListener(this);
		this.gatewayListener.listen();
	}

	@Override
	public void stop() throws Exception {
		ClientConnectionManager.shutdown();

		if (this.gatewayListener != null) {
			this.gatewayListener.shutdown();
			this.gatewayListener = null;
		}

		this.listeners.forEach(listener -> {
			try {
				PacketListenerManager.unregisterListener(listener);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
}
