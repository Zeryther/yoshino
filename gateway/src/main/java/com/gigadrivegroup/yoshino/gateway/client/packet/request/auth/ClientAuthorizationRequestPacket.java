/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.client.packet.request.auth;

import com.gigadrivegroup.yoshino.gateway.client.packet.request.ClientRequestPacket;
import lombok.Getter;
import lombok.Setter;

/**
 * A packet sent by the client to the server, requesting to be authorized and expecting a {@link com.gigadrivegroup.yoshino.gateway.client.packet.response.auth.ClientAuthorizationResponsePacket} next.
 */
public class ClientAuthorizationRequestPacket extends ClientRequestPacket {
	/**
	 * @param authToken The authorization token sent in this packet.
	 * @return The authorization token sent in this packet.
	 */
	@Getter
	@Setter
	private String authToken;

	public ClientAuthorizationRequestPacket(String authToken) {
		super();

		this.authToken = authToken;
	}
}
