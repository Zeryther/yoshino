/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.packet;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

public class Packet {
	/**
	 * @return The type of packet.
	 */
	@Getter
	private String type;

	/**
	 * @return The UUID of the connection associated with this packet
	 */
	@Getter
	@Setter
	private transient UUID associatedConnectionUUID;

	public Packet() {
		this.type = new StringBuilder(this.getClass().getPackageName()).append(".").append(this.getClass().getSimpleName()).toString();
	}
}
