/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.account;

import com.gigadrivegroup.yoshino.core.Platform;
import lombok.Getter;
import lombok.Setter;

public class AccountLinkData {
	/**
	 * @param userId The new user id.
	 * @return The user ID of this linked account, if available, null if not.
	 */
	@Getter
	@Setter
	private String id;

	/**
	 * @return The platform this linked account is from.
	 */
	@Getter
	private Platform platform;

	/**
	 * @return True if this linked account is the primary choice for this account.
	 */
	@Getter
	@Setter
	private boolean primary;

	protected AccountLinkData(Platform platform) {
		this.platform = platform;
	}

	/**
	 * @return The username of this linked account, if available, null if not.
	 */
	public String getUsername() {
		return null;
	}

	/**
	 * @return The URL of this linked account's avatar, if available, null if not.
	 */
	public String getAvatarURL() {
		return null;
	}
}
