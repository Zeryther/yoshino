/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server;

import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnection;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;
import lombok.Getter;
import lombok.Setter;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class GatewayListener {
	/**
	 * Cancels the listener, use {@link GatewayServer#stop()} for stopping the server instead!!
	 *
	 * @param running
	 * @return Whether the socket listener is currently running
	 */
	@Getter
	@Setter
	private boolean running;

	/**
	 * @return The main thread looping over open connections
	 */
	@Getter
	private Thread mainThread;

	/**
	 * @return The gateway server instance
	 */
	@Getter
	private GatewayServer server;

	public GatewayListener(GatewayServer server) {
		this.server = server;
	}

	/**
	 * Starts listening for client connections.
	 */
	public void listen() {
		this.setRunning(true);

		this.mainThread = new Thread(() -> {
			while (this.running) {
				Socket socket = null;

				try {
					socket = server.getServerSocket().accept();

					DataInputStream inputStream = new DataInputStream(socket.getInputStream());
					DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

					ClientConnection connection = new ClientConnection(ClientConnectionManager.getInstance().generateConnectionUUID(), socket, inputStream, outputStream);
					ClientConnectionManager.getInstance().getConnections().add(connection);
					connection.start();
				} catch (Exception e) {
					e.printStackTrace();

					try {
						if (socket != null) {
							socket.close();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		this.mainThread.setName("Yoshino Gateway Server");
		this.mainThread.start();
	}

	/**
	 * Shuts down the gateway listener
	 */
	public void shutdown() {
		this.setRunning(false);
		this.mainThread = null;
	}
}
