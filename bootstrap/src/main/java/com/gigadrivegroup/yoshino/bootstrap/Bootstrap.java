/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap;

public class Bootstrap {
	public static void main(String[] args) throws Exception {
		final String minVersion = "10.0.2";

		if (!(minVersion.compareTo(System.getProperty("java.version")) <= 0)) {
			System.err.println("Yoshino requires Java 10 or above to function! Please download and install it.");
			System.out.println("You can check your Java version by running: java -version");
			return;
		}

		YoshinoLauncher.main(args);
	}
}
