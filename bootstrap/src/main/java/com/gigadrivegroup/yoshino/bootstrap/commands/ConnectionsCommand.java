/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands;

import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommand;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnection;
import com.gigadrivegroup.yoshino.gateway.server.connection.ClientConnectionManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;

import java.util.Arrays;
import java.util.Iterator;

public class ConnectionsCommand extends ConsoleCommand {
	public ConnectionsCommand() {
		super("connections");

		setAliases(Arrays.asList("openconnections", "gatewayconnections", "clients", "clientlist"));
		setDescription("Lists all currently connected gateway clients.");
	}

	@Override
	public void execute(String label, String[] args) {
		Module module = ModuleStorage.getInstance().getModule("gateway-server");

		if (module != null) {
			ClientConnectionManager manager = ClientConnectionManager.getInstance();

			if (manager != null) {
				if (manager.getConnections().size() > 0) {
					StringBuilder sb = new StringBuilder();

					Iterator<ClientConnection> iterator = manager.getConnections().iterator();
					while (iterator.hasNext()) {
						ClientConnection connection = iterator.next();

						sb.append("\n").append("    ").append("[").append(connection.getStatus().name()).append("] ").append(connection.getUuid().toString()).append(" | ").append(connection.getSocket());
					}

					Logger.log(LoggerLevel.INFO, sb.toString());
				} else {
					Logger.log(LoggerLevel.INFO, "There are currently no clients connected.");
				}
			} else {
				Logger.log(LoggerLevel.ERROR, "The client connection manager is currently not running.");
			}
		} else {
			Logger.log(LoggerLevel.ERROR, "The gateway server is currently not running.");
		}
	}
}
