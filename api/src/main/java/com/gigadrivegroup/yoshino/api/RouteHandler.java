/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api;

import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.util.ArrayList;

public class RouteHandler {
	/**
	 * @return The name of the route.
	 */
	@Getter
	private String routeName;
	private boolean registered = false;

	public RouteHandler(String routeName) {
		this.routeName = routeName;

		this.register();
	}

	/**
	 * Creates an error response to be used in an API route.
	 *
	 * @param response The response.
	 * @param code     The HTTP code to be used.
	 * @return The object to return in the route.
	 */
	protected ImmutableMap<String, String> error(Response response, int code) {
		return this.error(response, code, "An error occurred.");
	}

	/**
	 * Creates an error response to be used in an API route.
	 *
	 * @param response The response.
	 * @param code     The HTTP code to be used.
	 * @param message  The error message.
	 * @return The object to return in the route.
	 */
	protected ImmutableMap<String, String> error(Response response, int code, String message) {
		return API.error(response, code, message);
	}

	/**
	 * @return The route to be executed on HTTP GET requests.
	 */
	public Route get() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP HEAD requests.
	 */
	public Route head() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP POST requests.
	 */
	public Route post() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP PUT requests.
	 */
	public Route put() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP DELETE requests.
	 */
	public Route delete() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP CONNECT requests.
	 */
	public Route connect() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP OPTIONS requests.
	 */
	public Route options() throws Exception {
		return (request, response) -> {
			response.status(204);
			response.header("Allow", Util.implode(this.getAllowedMethods(), ","));
			response.header("Access-Control-Allow-Methods", Util.implode(this.getAllowedMethods(), ","));
			return "";
		};
	}

	/**
	 * @return The route to be executed on HTTP TRACE requests.
	 */
	public Route trace() throws Exception {
		return null;
	}

	/**
	 * @return The route to be executed on HTTP PATCH requests.
	 */
	public Route patch() throws Exception {
		return null;
	}

	/**
	 * @return An array of all allowed methods for this route.
	 */
	public String[] getAllowedMethods() {
		ArrayList<String> methods = new ArrayList<String>();

		try {
			if (this.get() != null) methods.add("GET");
			if (this.head() != null) methods.add("HEAD");
			if (this.post() != null) methods.add("POST");
			if (this.put() != null) methods.add("PUT");
			if (this.delete() != null) methods.add("DELETE");
			if (this.connect() != null) methods.add("CONNECT");
			if (this.options() != null) methods.add("OPTIONS");
			if (this.trace() != null) methods.add("TRACE");
			if (this.patch() != null) methods.add("PATCH");
		} catch (Exception e) {
			e.printStackTrace(); // TODO
		}

		return methods.toArray(new String[]{});
	}

	/**
	 * Registers this route with Spark, may only be executed once.
	 */
	public void register() {
		if (!registered) {
			try {
				if (this.get() != null) Spark.get(this.routeName, this.get(), SerializationUtil.GSON::toJson);
				if (this.head() != null) Spark.head(this.routeName, this.head(), SerializationUtil.GSON::toJson);
				if (this.post() != null) Spark.post(this.routeName, this.post(), SerializationUtil.GSON::toJson);
				if (this.put() != null) Spark.put(this.routeName, this.put(), SerializationUtil.GSON::toJson);
				if (this.delete() != null) Spark.delete(this.routeName, this.delete(), SerializationUtil.GSON::toJson);
				if (this.connect() != null)
					Spark.connect(this.routeName, this.connect(), SerializationUtil.GSON::toJson);
				if (this.options() != null)
					Spark.options(this.routeName, this.options(), SerializationUtil.GSON::toJson);
				if (this.trace() != null) Spark.trace(this.routeName, this.trace(), SerializationUtil.GSON::toJson);
				if (this.patch() != null) Spark.patch(this.routeName, this.patch(), SerializationUtil.GSON::toJson);

				this.registered = true;
			} catch (Exception e) {
				e.printStackTrace(); // TODO
			}
		}
	}
}
