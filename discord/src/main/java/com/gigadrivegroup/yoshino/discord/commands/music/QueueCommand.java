/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands.music;

import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandExecution;
import com.gigadrivegroup.yoshino.discord.model.guild.settings.components.MusicComponent;
import net.dv8tion.jda.core.Permission;

import java.util.Arrays;

public class QueueCommand extends Command {
	public QueueCommand() {
		super("queue", MusicComponent.class);

		setAliases(Arrays.asList("q", "musicqueue", "tracks", "musictracks", "songs"));
		setRequiredDiscordPermissions(Arrays.asList(Permission.MESSAGE_MANAGE));
	}

	@Override
	public void execute(CommandExecution execution) {

	}
}
