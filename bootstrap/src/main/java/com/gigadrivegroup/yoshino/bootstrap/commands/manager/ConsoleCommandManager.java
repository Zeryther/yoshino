/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands.manager;

import com.gigadrivegroup.yoshino.bootstrap.commands.ConnectionsCommand;
import com.gigadrivegroup.yoshino.bootstrap.commands.HelpCommand;
import com.gigadrivegroup.yoshino.bootstrap.commands.ShutdownCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;

public class ConsoleCommandManager {
	/**
	 * @param instance The new ConsoleCommandManager instance.
	 * @return The ConsoleCommandManager instance.
	 */
	@Getter
	@Setter
	private static ConsoleCommandManager instance;
	/**
	 * @return The commands that are currently registered.
	 */
	@Getter
	private ArrayList<ConsoleCommand> commands;

	/**
	 * The constructor.
	 */
	public ConsoleCommandManager() {
		this.commands = new ArrayList<ConsoleCommand>();

		registerCommands();
	}

	private void registerCommands() {
		registerCommand(new ConnectionsCommand());
		registerCommand(new HelpCommand());
		registerCommand(new ShutdownCommand());
	}

	/**
	 * Gets a command by it's name.
	 *
	 * @param name            The name of the command to look for.
	 * @param considerAliases If true, command aliases will be considered as well.
	 * @return The command, null if no command could be found.
	 */
	public ConsoleCommand getCommand(String name, boolean considerAliases) {
		for (ConsoleCommand command : getCommands()) {
			if (command.getName().equalsIgnoreCase(name)) {
				return command;
			}

			if (considerAliases) {
				for (String alias : command.getAliases()) {
					if (alias.equalsIgnoreCase(name)) {
						return command;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Registers a command to the manager.
	 *
	 * @param command The command to be registered.
	 */
	public void registerCommand(ConsoleCommand command) {
		Validate.notNull(command.getName(), "The command name may not be null");
		Validate.notEmpty(command.getName(), "The command name may not be empty");

		Validate.isTrue(getCommand(command.getName(), true) == null, String.format("There is already a command with the name %s.", command.getName()));

		for (String alias : command.getAliases()) {
			Validate.isTrue(getCommand(alias, true) == null, String.format("There is already a command with the name %s.", command.getName()));
		}

		this.commands.add(command);
	}
}
