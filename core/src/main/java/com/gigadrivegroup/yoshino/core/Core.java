/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core;

import com.gigadrivegroup.yoshino.core.cache.CacheHandler;
import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.i18n.LocalizationManager;
import com.gigadrivegroup.yoshino.core.model.CachedObject;
import com.gigadrivegroup.yoshino.core.model.RepeatableTask;
import com.gigadrivegroup.yoshino.core.module.Module;
import com.gigadrivegroup.yoshino.core.thread.AsyncManager;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;

public class Core extends Module {
	private RepeatableTask cachedObjectTask;
	private RepeatableTask cacheHandlerTask;

	/**
	 * @return The current Yoshino version.
	 */
	@Getter
	@Setter
	private static String version;

	public Core() {
		super("core");
	}

	@Override
	public void start() {
		AsyncManager.setInstance(new AsyncManager());
		CacheHandler.setInstance(new CacheHandler());

		// load i18n
		Logger.log(LoggerLevel.INFO, "Loading locales...");

		LocalizationManager.setInstance(new LocalizationManager());

		// load database
		Logger.log(LoggerLevel.INFO, "Connecting to the database...");

		DatabaseManager.setInstance(new DatabaseManager(Config.getInstance().getMongoURI()));

		Logger.log(LoggerLevel.INFO, "Connected to the database.");

		// clear CachedObject cache
		this.cachedObjectTask = new RepeatableTask(30 * 1000, CachedObject::clearCache);
		this.cachedObjectTask.start();

		// clear CacheHandler storage
		this.cacheHandlerTask = new RepeatableTask(30 * 1000, CacheHandler::clearCache);
		this.cacheHandlerTask.start();
	}

	@Override
	public void stop() {
		AsyncManager.getInstance().shutdown();
		CachedObject.clearCache(true);

		Logger.log(LoggerLevel.INFO, "Shutting down database...");

		DatabaseManager.getInstance().shutdown();

		Logger.log(LoggerLevel.INFO, "Shut down database.");

		this.cachedObjectTask.stop();

		LocalizationManager.setInstance(null);
	}
}
