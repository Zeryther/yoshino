/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.guild.settings.components;

import com.gigadrivegroup.yoshino.core.components.Component;
import com.gigadrivegroup.yoshino.discord.commands.Command;
import com.gigadrivegroup.yoshino.discord.commands.CommandManager;
import com.gigadrivegroup.yoshino.discord.commands.CommandSettings;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Iterator;

public class DiscordComponent extends Component {
	/**
	 * Checks if a component class is a Discord component.
	 *
	 * @param componentClass The class to check.
	 * @return True if the passed class is a Discord component.
	 */
	public static boolean isDiscordComponent(Class<? extends Component> componentClass) {
		return DiscordComponent.class.isAssignableFrom(componentClass);
	}

	/**
	 * @return All registered command settings.
	 */
	@Getter
	private ArrayList<CommandSettings> registeredCommandSettings = new ArrayList<CommandSettings>();

	/**
	 * Gets the command settings object by the command's name.
	 *
	 * @param commandName The command name.
	 * @return The command settings object, null if it could not be found.
	 */
	public CommandSettings getCommandSettings(String commandName) {
		return this.getCommandSettings(CommandManager.getInstance().getCommand(commandName));
	}

	/**
	 * Gets the command settings object by the command.
	 *
	 * @param command The command.
	 * @return The command settings object, null if it could not be found.
	 */
	public CommandSettings getCommandSettings(Command command) {
		if (this.registeredCommandSettings.size() > 0) {
			Iterator<CommandSettings> iterator = this.registeredCommandSettings.iterator();
			while (iterator.hasNext()) {
				CommandSettings settings = iterator.next();

				if (settings.getCommandName().equalsIgnoreCase(command.getName())) {
					return settings;
				}
			}
		}

		if (command.getComponent().getSimpleName().equals(this.getType())) {
			try {
				if (command.getCommandSettingsClass() != CommandSettings.class) {
					CommandSettings settings = command.getCommandSettingsClass().newInstance();
					this.registeredCommandSettings.add(settings);

					return settings;
				} else {
					CommandSettings settings = new CommandSettings(command.getName());

					this.registeredCommandSettings.add(settings);
					return settings;
				}
			} catch (Exception e) {
				e.printStackTrace();

				Logger.log(LoggerLevel.WARN, "Failed to create instance of %s, defaulting to normal command settings!", command.getCommandSettingsClass().getSimpleName());

				CommandSettings settings = new CommandSettings(command.getName());

				this.registeredCommandSettings.add(settings);
				return settings;
			}
		}

		return null;
	}
}
