/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.util;

import com.gigadrivegroup.yoshino.core.Core;
import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.github.natanbc.weeb4j.TokenType;
import com.github.natanbc.weeb4j.Weeb4J;
import lombok.Getter;
import lombok.Setter;

public class ApiUtil {
	/**
	 * @return The ApiUtil instance
	 */
	@Getter
	@Setter
	private static ApiUtil instance;

	/**
	 * @return The Weeb4J instance.
	 */
	@Getter
	private Weeb4J weebApi;

	/**
	 * Constructor
	 */
	public ApiUtil() {
		this.weebApi = new Weeb4J.Builder()
				.setBotId(Long.valueOf(Config.getInstance().getDiscordClientId()))
				.setBotInfo("Yoshino", Core.getVersion(), Config.getInstance().getEnvironmentType().name()) // TODO: Use actual version string
				.setToken(TokenType.WOLKE, Config.getInstance().getWeebToken())
				.setHttpClient(HttpUtil.getHttpClient())
				.build();
	}
}
