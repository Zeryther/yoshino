/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.util;

import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.Request;
import okhttp3.Response;

public class ShardUtil {
	private static int shardTotal = -1;

	/**
	 * Returns the recommended amount of shards for the bot of the passed token.
	 *
	 * @param botToken The token of the bot.
	 * @return The number of shards.
	 */
	public static int getRecommendedShardAmount(String botToken) {
		if (shardTotal != -1) return shardTotal;

		try {
			Request request = new Request.Builder()
					.url("https://discordapp.com/api/gateway/bot")
					.header("Authorization", String.format("Bot %s", botToken))
					.header("Content-Type", "application/json")
					.build();

			Response response = HttpUtil.getHttpClient().newCall(request).execute();
			JsonObject object = new JsonParser().parse(response.body().string()).getAsJsonObject();

			response.close();

			shardTotal = object.get("shards").getAsInt();
		} catch (Exception e) {
			Logger.log(LoggerLevel.ERROR, "Failed to request shard data due to %s", e.getClass().getSimpleName());
		}

		return shardTotal;
	}
}
