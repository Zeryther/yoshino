/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.logger;

import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	@Getter
	private static Logger instance;

	static {
		instance = new Logger();
	}

	/**
	 * @return The Java 9 Stack Walker used to get the calling class when logging a message
	 */
	@Getter
	private StackWalker walker;

	/**
	 * @return The options used for this logger.
	 */
	@Getter
	private LoggerOptions options;

	public Logger() {
		walker = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
		options = new LoggerOptions();
	}

	/**
	 * Log a message to the console.
	 *
	 * @param level     The logging level that is supposed to be used for this message.
	 * @param message   The message that will be logged.
	 * @param variables The variables that will be used to format the message (check {@link String#format(String, Object...)} for more info).
	 */
	public static void log(LoggerLevel level, String message, Object... variables) {
		if (!getInstance().options.isShowingDebugMessages() && level.equals(LoggerLevel.DEBUG)) return;

		String formattedDate = new SimpleDateFormat("y'-'MM'-'dd' 'HH':'mm':'ss'.'SSS").format(new Date(System.currentTimeMillis()));

		// get the calling class from the stack walker
		Class callingClass = getInstance().getWalker().getCallerClass();

		// get the module name from the package
		String qualifiedPrefix = "com.gigadrivegroup.yoshino.";

		String moduleName = "???";

		String packageName = callingClass.getPackageName();
		if (packageName.startsWith(qualifiedPrefix) && packageName.length() > qualifiedPrefix.length()) {
			String[] split = packageName.split("\\.");

			int index = qualifiedPrefix.substring(0, qualifiedPrefix.length() - 1).split("\\.").length;

			if (split.length > index) {
				moduleName = split[index];
			}
		}

		StringBuilder sb = new StringBuilder()
				.append(formattedDate).append(" ")
				.append(level.getColorString()).append(level.getMaximizedName()).append(LoggerConstants.RESET_COLOR).append(" ")
				.append("[")
				.append(LoggerConstants.MODULE_COLOR).append(moduleName)
				.append(LoggerConstants.RESET_COLOR).append(":")
				.append(LoggerConstants.CLASS_COLOR).append(callingClass.getSimpleName())
				.append(LoggerConstants.RESET_COLOR).append("]:").append(" ")
				.append(String.format(message, variables));

		System.out.println(sb.toString());
	}
}
