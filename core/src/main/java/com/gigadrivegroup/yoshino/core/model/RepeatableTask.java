/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a task that will be repeated after a certain interval.
 */
public class RepeatableTask {
	/**
	 * Returns the millisecond interval of this repeatable task.
	 */
	@Setter
	@Getter
	private long interval;

	private Thread thread;

	private Runnable runnable;

	/**
	 * Constructor
	 *
	 * @param interval The interval of this task (in milliseconds).
	 */
	public RepeatableTask(long interval, Runnable runnable) {
		this.interval = interval;
		this.runnable = runnable;
	}

	/**
	 * Starts the repeatable task.
	 */
	public void start() {
		this.thread = new Thread(() -> {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					runnable.run();
					Thread.sleep(this.interval);
				}
			} catch (InterruptedException e) {
				// ignored
			}
		});

		this.thread.start();
	}

	/**
	 * Stops the repeatable task.
	 */
	public void stop() {
		this.thread.interrupt();
	}
}
