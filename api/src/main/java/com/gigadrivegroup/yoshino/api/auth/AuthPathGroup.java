/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api.auth;

import com.gigadrivegroup.yoshino.api.API;
import com.gigadrivegroup.yoshino.api.RequestData;
import com.gigadrivegroup.yoshino.core.EnvironmentType;
import com.gigadrivegroup.yoshino.core.Platform;
import com.gigadrivegroup.yoshino.core.account.Account;
import com.gigadrivegroup.yoshino.core.account.AccountLinkData;
import com.gigadrivegroup.yoshino.core.account.AccountToken;
import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.discord.model.DiscordLinkData;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import com.gigadrivegroup.yoshino.youtube.GoogleAPIUtil;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import spark.Route;
import spark.Spark;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

public class AuthPathGroup {
	static Route url = (request, response) -> {
		RequestData requestData = API.getRequestData(request);
		HashMap<String, String> data = requestData.getData();
		String url = null;

		if (data.containsKey("service")) {
			final String service = data.get("service");

			switch (service) {
				case "DISCORD":
					url = String.format("https://discordapp.com/api/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s", URLEncoder.encode(Config.getInstance().getDiscordClientId(), StandardCharsets.UTF_8), Config.getInstance().getDiscordRedirectURL(), URLEncoder.encode("identify email guilds", StandardCharsets.UTF_8));
					break;
				case "PATREON":
					url = String.format("https://patreon.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code", Config.getInstance().getPatreonClientId(), URLEncoder.encode(Config.getInstance().getPatreonRedirectURL(), StandardCharsets.UTF_8));
					break;
				case "STREAMLABS":
					url = String.format("https://streamlabs.com/api/v1.0/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=donations.read%20socket.token", URLEncoder.encode(Config.getInstance().getStreamlabsClientId(), StandardCharsets.UTF_8), URLEncoder.encode(Config.getInstance().getStreamlabsRedirectUrl(), StandardCharsets.UTF_8));
					break;
				case "TIPEEE":
					url = String.format("https://api.tipeeestream.com/oauth/v2/auth?client_id=%s&response_type=code&redirect_uri=%s", URLEncoder.encode(Config.getInstance().getTipeeeStreamClientId(), StandardCharsets.UTF_8), URLEncoder.encode(Config.getInstance().getStreamlabsRedirectUrl(), StandardCharsets.UTF_8));
					break;
				case "TWITCH":
					url = String.format("https://id.twitch.tv/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s", URLEncoder.encode(Config.getInstance().getTwitchClientId(), StandardCharsets.UTF_8), URLEncoder.encode(Config.getInstance().getTwitchRedirectURL(), StandardCharsets.UTF_8), URLEncoder.encode("channel_check_subscription channel_commercial channel_editor channel_subscriptions communities_moderate user_blocks_edit user_blocks_read chat_login bits:read openid user_read", StandardCharsets.UTF_8));
					break;
				case "YOUTUBE":
					url = new GoogleAuthorizationCodeFlow.Builder(
							GoogleAPIUtil.HTTP_TRANSPORT,
							GoogleAPIUtil.JSON_FACTORY,
							Config.getInstance().getYoutubeClientId(),
							Config.getInstance().getYoutubeClientSecret(),
							Arrays.asList("https://www.googleapis.com/auth/youtube.force-ssl"))
							.setApprovalPrompt("force")
							.setAccessType("offline").build()
							.newAuthorizationUrl().setRedirectUri(Config.getInstance().getYoutubeRedirectURL()).build();
					break;
			}
		}

		if (url != null) {
			response.status(200);
			return ImmutableMap.of("url", url);
		} else {
			return API.error(response, 400);
		}
	};

	static Route callback = (request, response) -> {
		RequestData requestData = API.getRequestData(request);
		HashMap<String, String> data = requestData.getData();

		if (data.containsKey("service")) {
			String service = data.get("service");

			switch (service) {
				case "DISCORD":
					if (data.containsKey("code")) {
						String code = data.get("code");

						try {
							OkHttpClient client = HttpUtil.getHttpClient();
							Response exchangeResponse = client.newCall(new Request.Builder()
									.url("https://discordapp.com/api/oauth2/token")
									.header("User-Agent", Constants.USER_AGENT)
									.method("POST", new FormBody.Builder()
											.add("client_id", Config.getInstance().getDiscordClientId())
											.add("client_secret", Config.getInstance().getDiscordClientSecret())
											.add("grant_type", "authorization_code")
											.add("code", code)
											.add("redirect_uri", Config.getInstance().getDiscordRedirectURL())
											.add("scope", "identify email guilds")
											.build())
									.build()).execute();

							if (exchangeResponse.isSuccessful()) {
								String body;
								if (exchangeResponse.body() != null && !(body = exchangeResponse.body().string()).isEmpty()) {
									exchangeResponse.close();

									JsonElement element = new JsonParser().parse(body);

									if (element.isJsonObject()) {
										JsonObject object = element.getAsJsonObject();

										if (object.has("access_token") && object.has("refresh_token")) {
											String accessToken = object.get("access_token").getAsString();
											String refreshToken = object.get("refresh_token").getAsString();

											Response dataResponse = client.newCall(new Request.Builder()
													.url("https://discordapp.com/api/users/@me")
													.header("User-Agent", Constants.USER_AGENT)
													.header("Authorization", String.format("Bearer %s", accessToken))
													.build()).execute();

											if (dataResponse.isSuccessful()) {
												if (dataResponse.body() != null && !(body = dataResponse.body().string()).isEmpty()) {
													dataResponse.close();

													element = new JsonParser().parse(body);

													if (element.isJsonObject()) {
														object = element.getAsJsonObject();

														if (object.has("id") && object.has("username") && object.has("discriminator") && object.has("avatar") && object.has("email")) {
															final long id = object.get("id").getAsLong();
															final String username = object.get("username").getAsString();
															final String discriminator = object.get("discriminator").getAsString();
															final String avatarHash = object.get("avatar").isJsonNull() ? null : object.get("avatar").getAsString();
															final String email = object.get("email").isJsonNull() ? null : object.get("email").getAsString();

															final String avatar = avatarHash != null ? String.format("https://cdn.discordapp.com/avatars/%s/%s.%s", id, avatarHash, avatarHash.startsWith("a_") ? "gif" : "png") : String.format("https://cdn.discordapp.com/embed/avatars/%s.png", discriminator);

															DiscordUser discordUser = DiscordUser.getUser(id);
															if (discordUser != null) {
																discordUser.getName().set(username);
																discordUser.getDiscriminator().set(discriminator);
																discordUser.getAvatarUrl().set(avatar);
																discordUser.getEmail().set(email);
																discordUser.saveData();
															} else {
																DatabaseManager manager = DatabaseManager.getInstance();
																manager.discordUsers().insert(new BasicDBObject("_id", id)
																		.append("name", username)
																		.append("discriminator", discriminator)
																		.append("avatarUrl", avatar)
																		.append("email", email));

																discordUser = DiscordUser.getUser(id);
															}

															if (discordUser != null) {
																Account account = requestData.getAccount() != null ? requestData.getAccount() : discordUser.getAccount() != null ? discordUser.getAccount() : Account.createAccount();
																AccountToken token = requestData.getToken() != null ? requestData.getToken() : AccountToken.createToken(account);

																AccountLinkData accountLinkData = account.getLinkedAccount(Platform.DISCORD, DiscordLinkData.class);
																DiscordLinkData discordLinkData = accountLinkData == null ? new DiscordLinkData() : (DiscordLinkData) accountLinkData;

																discordLinkData.setAccessToken(accessToken);
																discordLinkData.setRefreshToken(refreshToken);
																discordLinkData.setClientId(Long.parseLong(Config.getInstance().getDiscordClientId()));
																discordLinkData.setClientSecret(Config.getInstance().getDiscordClientSecret());
																discordLinkData.setId(String.valueOf(id));

																account.addLinkedAccount(discordLinkData);
																account.saveData();

																response.status(200);
																return ImmutableMap.of("token", token.getId().toString());
															} else {
																return API.error(response, 500);
															}
														} else {
															return API.error(response, 500);
														}
													} else {
														return API.error(response, 500);
													}
												} else {
													dataResponse.close();
													return API.error(response, 500);
												}
											} else {
												dataResponse.close();
												return API.error(response, 500);
											}
										} else {
											return API.error(response, 500);
										}
									} else {
										if (Config.getInstance().getEnvironmentType() == EnvironmentType.DEVELOPMENT) {
											return API.error(response, 500, body);
										}

										return API.error(response, 500);
									}
								} else {
									if (Config.getInstance().getEnvironmentType() == EnvironmentType.DEVELOPMENT) {
										final String s = exchangeResponse.body() != null ? exchangeResponse.body().string() : "An error occurred.";
										exchangeResponse.close();

										return API.error(response, 500, s);
									}

									exchangeResponse.close();
									return API.error(response, 500);
								}
							} else {
								exchangeResponse.close();
								return API.error(response, 500);
							}
						} catch (Exception e) {
							e.printStackTrace(); // TODO
							return API.error(response, 500);
						}
					}

					break;
				case "PATREON":
					break;
				case "STREAMLABS":
					break;
				case "TIPEEE":
					break;
				case "TWITCH":
					break;
				case "YOUTUBE":
					break;
			}
		}

		return API.error(response, 400);
	};

	public static void init() {
		Spark.get("/auth/url", url, SerializationUtil.GSON::toJson);
		Spark.options("/auth/url", API.OPTIONS_RESPONSE, SerializationUtil.GSON::toJson);

		Spark.post("/auth/callback", callback, SerializationUtil.GSON::toJson);
		Spark.options("/auth/callback", API.OPTIONS_RESPONSE, SerializationUtil.GSON::toJson);
	}
}
