/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice;

import lombok.Getter;
import lombok.Setter;

public class DataServiceSearchResult {
	/**
	 * @return The identifier of this result, used to later fetch the full data.
	 */
	@Getter
	private String identifier;

	/**
	 * @return The name that should be displayed in the search results.
	 */
	@Getter
	private String name;

	/**
	 * @return The link that should be used in the search results.
	 */
	@Getter
	private String link;

	/**
	 * @return The DataService used to fetch this result.
	 */
	@Getter
	private DataService dataService;

	@Setter
	private Data dataObject;

	/**
	 * Constructor
	 *
	 * @param identifier  The identifier of this result, used to later fetch the full data.
	 * @param name        The name that should be displayed in the search results.
	 * @param link        The link that should be used in the search results.
	 * @param dataService The DataService used to fetch this result.
	 */
	public DataServiceSearchResult(String identifier, String name, String link, DataService dataService) {
		this.identifier = identifier;
		this.name = name;
		this.link = link;
		this.dataService = dataService;
	}

	/**
	 * @return The object that holds the final data of the result, null = requires another request to fetch the data.
	 */
	public Data getDataObject() {
		if (dataObject == null) {
			this.dataService.fetchData(this);
		}

		return dataObject;
	}

	public static class Data {
		@Getter
		@Setter
		private String identifier;

		@Getter
		@Setter
		private String name;

		@Getter
		@Setter
		private String url;

		@Getter
		@Setter
		private String image;

		@Getter
		@Setter
		private String description;

		@Getter
		@Setter
		private String ratingString;

		@Getter
		@Setter
		private String type;

		@Getter
		@Setter
		private Status status;

		public enum Status {
			CURRENT,
			FINISHED,
			TBA,
			UNRELEASED,
			UPCOMING
		}
	}
}
