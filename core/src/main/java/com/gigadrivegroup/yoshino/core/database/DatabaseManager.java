/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.database;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.google.gson.reflect.TypeToken;
import com.mongodb.*;
import com.mongodb.util.JSON;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseManager {
	/**
	 * @param instance The new instance
	 * @return The DatabaseManager instance
	 */
	@Getter
	@Setter
	private static DatabaseManager instance;

	/**
	 * @param mongoClient The new MongoClient instance
	 * @return The MongoClient instance
	 * @throws Exception Thrown if the client fails to connect to the server
	 */
	@Getter
	@Setter
	private MongoClient mongoClient;

	/**
	 * @return The main MongoDB database used by Yoshino
	 */
	@Getter
	private DB mainDatabase;

	public DatabaseManager(String uri) {
		Validate.notNull(uri, "The MongoDB URI may not be null.");
		Validate.notEmpty(uri, "The MongoDB URI may not be empty.");

		this.mongoClient = new MongoClient(new MongoClientURI(Config.getInstance().getMongoURI()));
		this.mainDatabase = this.mongoClient.getDB(Config.getInstance().getMongoDatabaseName());
	}

	// COLLECTIONS START

	/**
	 * Returns the collection that holds the data of all accounts.
	 *
	 * @return The collection instance.
	 */
	public DBCollection accounts() {
		return this.mainDatabase.getCollection(String.format("%saccounts", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that holds the data of all discord guilds.
	 *
	 * @return The collection instance.
	 */
	public DBCollection discordGuilds() {
		return this.mainDatabase.getCollection(String.format("%sdiscord_guilds", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that holds the data of all settings of all discord guilds.
	 *
	 * @return The collection instance.
	 */
	public DBCollection discordGuildSettings() {
		return this.mainDatabase.getCollection(String.format("%sdiscord_guildsettings", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that holds the data of all discord users.
	 *
	 * @return The collection instance.
	 */
	public DBCollection discordUsers() {
		return this.mainDatabase.getCollection(String.format("%sdiscord_users", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that holds the data of all teams.
	 *
	 * @return The collection instance.
	 */
	public DBCollection teams() {
		return this.mainDatabase.getCollection(String.format("%steams", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that holds the data of all settings of all teams.
	 *
	 * @return The collection instance.
	 */
	public DBCollection teamSettings() {
		return this.mainDatabase.getCollection(String.format("%steams_settings", Config.getInstance().getMongoCollectionPrefix()));
	}

	/**
	 * Returns the collection that olds the data of all tokens used to authorize users with the API.
	 *
	 * @return The collection instance.
	 */
	public DBCollection tokens() {
		return this.mainDatabase.getCollection(String.format("%stokens", Config.getInstance().getMongoCollectionPrefix()));
	}

	// COLLECTIONS END

	/**
	 * Returns the value by key of a DBObject result as an ArrayList of a certain type.
	 *
	 * @param result    The result to look through.
	 * @param key       The key of the value in question.
	 * @param <T>       The type to return the value as.
	 * @return The value as an ArrayList of a certain type, null if not found.
	 */
	public <T> ArrayList<T> retrieveArrayListOfType(DBObject result, String key) {
		return retrieveArrayListOfType(result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as an ArrayList of a certain type.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @param <T>          The type to return the value as.
	 * @return The value as an ArrayList of a certain type.
	 */
	public <T> ArrayList<T> retrieveArrayListOfType(DBObject result, String key, ArrayList<T> defaultValue) {
		Object o = result.get(key);

		if (o != null) {
			String json = JSON.serialize(o);

			return SerializationUtil.GSON.fromJson(json, new TypeToken<ArrayList<T>>() {
			}.getType());
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as a Boolean.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as a Boolean, false if not found.
	 */
	public boolean retrieveBoolean(DBObject result, String key) {
		return retrieveBoolean(result, key, false);
	}

	/**
	 * Returns the value by key of a DBObject result as a Boolean.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a Boolean.
	 */
	public boolean retrieveBoolean(DBObject result, String key, boolean defaultValue) {
		Object o = result.get(key);

		if (o instanceof Boolean) {
			return (Boolean) o;
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as a Double.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as a Double, 0 if not found.
	 */
	public double retrieveDouble(DBObject result, String key) {
		return retrieveDouble(result, key, 0);
	}

	/**
	 * Returns the value by key of a DBObject result as a Double.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a Double.
	 */
	public double retrieveDouble(DBObject result, String key, double defaultValue) {
		Object o = result.get(key);

		if (o != null) {
			if (o instanceof Long) {
				return (Long) o;
			} else if (o instanceof Integer) {
				return (Integer) o;
			} else if (o instanceof Double) {
				return (Double) o;
			}
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as a HashMap of certain types.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @param <K>    The type of the key of the final HashMap.
	 * @param <V>    The type of the value of the final HashMap.
	 * @return The value as a HashMap with certain types, null if not found.
	 */
	public <K, V> HashMap<K, V> retrieveHashMapOfType(Class<K> keyClass, Class<V> valueClass, DBObject result, String key) {
		return retrieveHashMapOfType(keyClass, valueClass, result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as a HashMap of certain types.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @param <K>          The type of the key of the final HashMap.
	 * @param <V>          The type of the value of the final HashMap.
	 * @return The value as a HashMap with certain types.
	 */
	public <K, V> HashMap<K, V> retrieveHashMapOfType(Class<K> keyClass, Class<V> valueClass, DBObject result, String key, HashMap<K, V> defaultValue) {
		try {
			Object o = result.get(key);

			if (o != null) {
				return SerializationUtil.GSON.fromJson(JSON.serialize(o), TypeToken.getParameterized(HashMap.class, keyClass, valueClass).getType());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as an Integer.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as an Integer, 0 if not found.
	 */
	public int retrieveInteger(DBObject result, String key) {
		return retrieveInteger(result, key, 0);
	}

	/**
	 * Returns the value by key of a DBObject result as an Integer.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as an Integer.
	 */
	public int retrieveInteger(DBObject result, String key, int defaultValue) {
		Object o = result.get(key);

		if (o != null) {
			if (o instanceof Long) {
				return ((Long) o).intValue();
			} else if (o instanceof Integer) {
				return (Integer) o;
			} else if (o instanceof Double) {
				return ((Double) o).intValue();
			}
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as a Long.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as a Long, 0 if not found.
	 */
	public long retrieveLong(DBObject result, String key) {
		return retrieveLong(result, key, 0);
	}

	/**
	 * Returns the value by key of a DBObject result as a Long.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a Long.
	 */
	public long retrieveLong(DBObject result, String key, long defaultValue) {
		Object o = result.get(key);

		if (o != null) {
			if (o instanceof Long) {
				return (Long) o;
			} else if (o instanceof Integer) {
				return (Integer) o;
			} else if (o instanceof Double) {
				return ((Double) o).intValue();
			}
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as an Object.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as an Object, null if not found.
	 */
	public Object retrieveObject(DBObject result, String key) {
		return retrieveObject(result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as an Object.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a Object.
	 */
	public Object retrieveObject(DBObject result, String key, Object defaultValue) {
		Object o = result.get(key);

		return o != null ? o : defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as an Object.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as an Object, null if not found.
	 */
	public ObjectId retrieveObjectId(DBObject result, String key) {
		return retrieveObjectId(result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as an ObjectId.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a ObjectId.
	 */
	public ObjectId retrieveObjectId(DBObject result, String key, ObjectId defaultValue) {
		Object o = result.get(key);

		return o instanceof ObjectId ? (ObjectId) o : defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as an Object of a certain type.
	 *
	 * @param clazzType The class of the type to return the value as.
	 * @param result    The result to look through.
	 * @param key       The key of the value in question.
	 * @param <T>       The type to return the value as.
	 * @return The value as an Object of a certain type, null if not found.
	 */
	public <T> T retrieveObjectOfType(Class<T> clazzType, DBObject result, String key) {
		return retrieveObjectOfType(clazzType, result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as an Object of a certain type.
	 *
	 * @param clazzType    The class of the type to return the value as.
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @param <T>          The type to return the value as.
	 * @return The value as an Object of a certain type, null if not found.
	 */
	public <T> T retrieveObjectOfType(Class<T> clazzType, DBObject result, String key, T defaultValue) {
		Object o = result.get(key);

		if (o != null) {
			String json = JSON.serialize(o);

			return SerializationUtil.GSON.fromJson(json, clazzType);
		}

		return defaultValue;
	}

	/**
	 * Returns the value by key of a DBObject result as a String.
	 *
	 * @param result The result to look through.
	 * @param key    The key of the value in question.
	 * @return The value as a String, null if not found.
	 */
	public String retrieveString(DBObject result, String key) {
		return retrieveString(result, key, null);
	}

	/**
	 * Returns the value by key of a DBObject result as a String.
	 *
	 * @param result       The result to look through.
	 * @param key          The key of the value in question.
	 * @param defaultValue The value to be returned if the key could not be found.
	 * @return The value as a String.
	 */
	public String retrieveString(DBObject result, String key, String defaultValue) {
		Object o = retrieveObject(result, key, defaultValue);

		return o != null ? (String) o : defaultValue;
	}

	/**
	 * Converts an object to an object that can be used in a database.
	 *
	 * @return The converted object.
	 */
	public Object convertObjectToDBObject(Object object) {
		return JSON.parse(SerializationUtil.GSON.toJson(object));
	}

	/**
	 * Shuts down the MongoClient.
	 */
	public void shutdown() {
		this.mongoClient.close();
	}
}
