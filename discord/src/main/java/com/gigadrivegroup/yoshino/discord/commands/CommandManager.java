/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands;

import com.gigadrivegroup.yoshino.discord.commands.fun.LennyCommand;
import com.gigadrivegroup.yoshino.discord.commands.music.*;
import com.gigadrivegroup.yoshino.discord.commands.reaction.*;
import com.gigadrivegroup.yoshino.discord.commands.utility.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Iterator;

public class CommandManager {
	/**
	 * @param instance The new command manager instance.
	 * @return The command manager instance.
	 */
	@Getter
	@Setter
	private static CommandManager instance;

	/**
	 * @return The command handler instance.
	 */
	@Getter
	private CommandHandler handler;

	/**
	 * @return All registered commands.
	 */
	@Getter
	private ArrayList<Command> commands;

	public CommandManager() {
		this.handler = new CommandHandler();
		this.commands = new ArrayList<Command>();

		// register commands

		// fun
		this.commands.add(new LennyCommand());

		// music
		this.commands.add(new ClearQueueCommand());
		this.commands.add(new LoopCommand());
		this.commands.add(new NowPlayingCommand());
		this.commands.add(new PlayCommand());
		this.commands.add(new PlaylistCommand());
		this.commands.add(new QueueCommand());
		this.commands.add(new ReconnectCommand());
		this.commands.add(new RemoveCommand());
		this.commands.add(new RestartCommand());
		this.commands.add(new SeekCommand());
		this.commands.add(new ShuffleCommand());
		this.commands.add(new SkipCommand());
		this.commands.add(new StopCommand());
		this.commands.add(new VolumeCommand());

		// reaction
		this.commands.add(new BakaCommand());
		this.commands.add(new BlushCommand());
		this.commands.add(new CryCommand());
		this.commands.add(new CuddleCommand());
		this.commands.add(new DanceCommand());
		this.commands.add(new HugCommand());
		this.commands.add(new InsultCommand());
		this.commands.add(new LewdCommand());
		this.commands.add(new LickCommand());
		this.commands.add(new NekoCommand());
		this.commands.add(new NomCommand());
		this.commands.add(new OwoCommand());
		this.commands.add(new PatCommand());
		this.commands.add(new PokeCommand());
		this.commands.add(new PoutCommand());
		this.commands.add(new ShrugCommand());
		this.commands.add(new SlapCommand());
		this.commands.add(new SleepyCommand());
		this.commands.add(new SmugCommand());
		this.commands.add(new StareCommand());
		this.commands.add(new ThumbsUpCommand());
		this.commands.add(new TriggeredCommand());
		this.commands.add(new WastedCommand());

		// utility
		this.commands.add(new AnimeCommand());
		this.commands.add(new AvatarCommand());
		this.commands.add(new HelpCommand());
		this.commands.add(new MangaCommand());
		this.commands.add(new MovieCommand());
		this.commands.add(new TVCommand());
	}

	/**
	 * Gets a command from the matching label.
	 *
	 * @param label The command label (either the command name or an alias).
	 * @return The command object, null if the command could not be found.
	 */
	public Command getCommand(String label) {
		Iterator<Command> iterator = this.commands.iterator();
		while (iterator.hasNext()) {
			Command command = iterator.next();

			if (command.getName().equalsIgnoreCase(label)) {
				return command;
			}

			if (command.getAliases() != null) {
				Iterator<String> aliasIterator = command.getAliases().iterator();
				while (aliasIterator.hasNext()) {
					String alias = aliasIterator.next();

					if (alias.equalsIgnoreCase(label)) {
						return command;
					}
				}
			}
		}

		return null;
	}
}
