/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.logger;

import lombok.Getter;
import lombok.Setter;

public class LoggerOptions {
	/**
	 * @param showingDebugMessages If false, debug messages will not render
	 * @return If false, debug messages will not render
	 */
	@Getter
	@Setter
	private boolean showingDebugMessages;

	/**
	 * Constructor
	 */
	public LoggerOptions() {
		setShowingDebugMessages(true);
	}
}
