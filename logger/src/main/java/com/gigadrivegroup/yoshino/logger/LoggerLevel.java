/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.logger;

import lombok.Getter;

public enum LoggerLevel {
	INFO("\u001B[32m"),
	WARN("\u001B[33m"),
	ERROR("\u001B[31m"),
	DEBUG("\u001B[34m");

	private static int maxLength = -1;
	/**
	 * String used to color console output.
	 *
	 * @return Color string
	 */
	@Getter
	private String colorString;

	LoggerLevel(String colorString) {
		this.colorString = colorString;
	}

	/**
	 * @return Returns the {@link LoggerLevel#name() name()} of the LoggerLevel with enough trailing spaces to make the output look better.
	 */
	public String getMaximizedName() {
		if (maxLength == -1) {
			for (LoggerLevel level : values()) {
				if (level.name().length() > maxLength)
					maxLength = level.name().length();
			}
		}

		StringBuilder sb = new StringBuilder(this.name());

		while (sb.length() < maxLength) {
			sb.append(" ");
		}

		return sb.toString();
	}
}
