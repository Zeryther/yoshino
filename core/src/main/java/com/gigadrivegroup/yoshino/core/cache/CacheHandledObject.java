/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.cache;

import lombok.Getter;

public class CacheHandledObject {
	/**
	 * @return The key used to identify this object.
	 */
	@Getter
	private String key;

	/**
	 * @return The value of this object.
	 */
	@Getter
	private Object value;

	/**
	 * @return The lifespan of this object.
	 */
	@Getter
	private long expiry;

	/**
	 * @return The unix timestamp of when this object was created.
	 */
	@Getter
	private long created;

	/**
	 * Constructor
	 *
	 * @param key    The key used to identify this object.
	 * @param value  The value of this object.
	 * @param expiry The lifespan of this object.
	 */
	public CacheHandledObject(String key, Object value, long expiry) {
		this.key = key;
		this.value = value;
		this.expiry = expiry;
		this.created = System.currentTimeMillis();
	}

	/**
	 * @return True if the object has expired.
	 */
	public boolean hasExpired() {
		return System.currentTimeMillis() >= this.expiry;
	}
}
