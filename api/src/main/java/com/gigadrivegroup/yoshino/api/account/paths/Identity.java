/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api.account.paths;

import com.gigadrivegroup.yoshino.api.API;
import com.gigadrivegroup.yoshino.api.RequestData;
import com.gigadrivegroup.yoshino.api.RouteHandler;
import com.gigadrivegroup.yoshino.core.account.Account;
import com.gigadrivegroup.yoshino.core.account.AccountLinkData;
import com.google.common.collect.ImmutableMap;
import spark.Route;

import java.util.HashMap;

public class Identity extends RouteHandler {
	public Identity() {
		super("/account/identity");
	}

	@Override
	public Route get() {
		return ((request, response) -> {
			RequestData data = API.getRequestData(request);

			if (data.getAccount() != null) {
				Account account = data.getAccount();
				AccountLinkData primary = account.getPrimaryLinkedAccount();

				HashMap<String, Object> primaryAccount = new HashMap<String, Object>();
				primaryAccount.put("platform", primary.getPlatform().toString());
				primaryAccount.put("id", primary.getId());
				primaryAccount.put("username", primary.getUsername());
				primaryAccount.put("avatar", primary.getAvatarURL());

				return ImmutableMap.of("id", account.getId().toString(), "primaryAccount", primaryAccount);
			}

			return error(response, 400);
		});
	}
}
