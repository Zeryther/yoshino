/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap;

import com.gigadrivegroup.yoshino.api.API;
import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommandManager;
import com.gigadrivegroup.yoshino.bootstrap.commands.reader.ConsoleCommandReader;
import com.gigadrivegroup.yoshino.core.Core;
import com.gigadrivegroup.yoshino.core.config.ConfigLoader;
import com.gigadrivegroup.yoshino.core.module.ModuleStorage;
import com.gigadrivegroup.yoshino.core.shutdown.Shutdown;
import com.gigadrivegroup.yoshino.discord.Discord;
import com.gigadrivegroup.yoshino.gateway.client.GatewayClient;
import com.gigadrivegroup.yoshino.gateway.server.GatewayServer;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import com.gigadrivegroup.yoshino.logger.LoggerUtil;
import com.gigadrivegroup.yoshino.twitch.Twitch;
import com.gigadrivegroup.yoshino.youtube.YouTube;
import org.awaitility.Awaitility;

import java.util.Properties;

public class YoshinoLauncher {
	public static void main(String[] args) throws Exception {
		// disable dependency loggers
		LoggerUtil.disableDefaultLoggers();

		// loading maven properties
		Properties p = new Properties();
		p.load(YoshinoLauncher.class.getClassLoader().getResourceAsStream("project.properties"));

		String version = p.getProperty("version");
		Core.setVersion(version);

		Logger.log(LoggerLevel.INFO, "Starting up Yoshino (version: %s)", Core.getVersion());

		// load configuration
		ConfigLoader.loadConfiguration();

		// load modules
		ModuleStorage storage = new ModuleStorage();
		ModuleStorage.setInstance(storage);

		storage.loadModule(new API());
		storage.loadModule(new Core());
		storage.loadModule(new GatewayServer());
		storage.loadModule(new GatewayClient());

		// wait for the gateway client to authenticate
		Awaitility.await().until((() -> ((GatewayClient) storage.getModule("gateway-client")).getConnectionUUID() != null));

		storage.loadModule(new Discord());
		storage.loadModule(new Twitch());
		storage.loadModule(new YouTube());

		// register console command manager
		ConsoleCommandManager.setInstance(new ConsoleCommandManager());

		// register console command reader
		ConsoleCommandReader reader = new ConsoleCommandReader();
		reader.start();

		ConsoleCommandReader.setInstance(reader);

		// register shutdown hooks
		Shutdown.registerShutdownHook();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> ConsoleCommandReader.getInstance().stop(), "Yoshino Console Command Reader Shutdown Hook"));
	}
}
