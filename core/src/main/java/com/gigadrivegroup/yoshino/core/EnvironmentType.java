/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core;

import lombok.Getter;

public enum EnvironmentType {
	PRODUCTION,
	BETA("+"),
	DEVELOPMENT("=");

	/**
	 * @return The default command prefix for this environment type.
	 */
	@Getter
	private String defaultCommandPrefix;

	EnvironmentType() {
		this.defaultCommandPrefix = "!";
	}

	EnvironmentType(String defaultCommandPrefix) {
		this.defaultCommandPrefix = defaultCommandPrefix;
	}
}
