/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.commands;

import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import com.gigadrivegroup.yoshino.discord.model.user.DiscordUser;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.apache.commons.lang3.Validate;

import java.time.Instant;
import java.util.function.Consumer;

public class CommandExecution {
	/**
	 * @return The command label that was used to execute this command.
	 */
	@Getter
	@Setter
	private String label;

	/**
	 * @return The arguments that were passed when executing this command.
	 */
	@Getter
	@Setter
	private String[] arguments;

	/**
	 * @return The user that executed this command.
	 */
	@Getter
	@Setter
	private DiscordUser user;

	/**
	 * @return The guild that this command was executed in.
	 */
	@Getter
	@Setter
	private DiscordGuild guild;

	/**
	 * @return The channel that this command was executed in.
	 */
	@Getter
	@Setter
	private TextChannel channel;

	/**
	 * @return The command settings used on this command execution.
	 */
	@Getter
	@Setter
	private CommandSettings commandSettings;

	/**
	 * @return When this comamnd was executed.
	 */
	@Getter
	@Setter
	private Instant time;

	/**
	 * @return The event that executed this command.
	 */
	@Getter
	@Setter
	private MessageReceivedEvent event;

	/**
	 * @return Whether a reply was sent to this execution, if this value is true, the reply methods will not send a message.
	 */
	@Getter
	@Setter
	private boolean replied = false;

	/**
	 * @return The object of the command that was executed.
	 */
	public Command getCommand() {
		return CommandManager.getInstance().getCommand(this.label);
	}

	/**
	 * @return The mention string used to mention the command invoker, empty string if mentioning the invoker is disabled in the {@link CommandExecution#getCommandSettings() command settings}.
	 */
	public String getMention() {
		return getCommandSettings() != null && getCommandSettings().isReplyToExecutor() ? new StringBuilder(getUser().toMention()).append(" ").toString() : "";
	}

	/**
	 * @return The targeted user, if available. Returns {@link CommandExecution#getUser() the executing user} if no target was found.
	 */
	public DiscordUser getTarget() {
		return getTarget(getUser());
	}

	/**
	 * @param fallback The user to use as fallback if no target was found.
	 * @return The targeted user, if available.
	 */
	public DiscordUser getTarget(DiscordUser fallback) {
		if (getArguments().length > 0) {
			String compiledArgs = Util.implode(getArguments(), " ");

			return getGuild().getTarget(compiledArgs, fallback);
		}

		return fallback;
	}

	/**
	 * Replies to the user that executed this command with the passed message.
	 *
	 * @param message The message to reply with.
	 */
	public Message reply(CharSequence message) {
		return this.reply(new MessageBuilder().setContent(message.toString()));
	}

	/**
	 * Replies to the user that executed this command with the passed message and executes the passed callback afterwards.
	 *
	 * @param message  The message to reply with.
	 * @param callback The callback to execute.
	 */
	public Message reply(CharSequence message, Consumer<Message> callback) {
		return this.reply(new MessageBuilder().setContent(message.toString()), callback);
	}

	/**
	 * Replies to the user that executed this command with the passed message.
	 *
	 * @param message The message to reply with.
	 */
	public Message reply(MessageBuilder message) {
		return this.reply(message, null);
	}

	/**
	 * Replies to the user that executed this command with the passed message and executed the passed callback afterwards.
	 *
	 * @param message  The message to reply with.
	 * @param callback The callback to execute.
	 */
	public Message reply(MessageBuilder message, Consumer<Message> callback){
		Validate.notNull(message, "The message may not be null");

		Message built = message.build();

		String content = built.getContentRaw() != null && !built.getContentRaw().isEmpty() ? new StringBuilder(getMention()).append(built.getContentRaw()).toString() : getMention();

		Message m = message.setContent(content).build();

		if (!replied) {
			replied = true;
			channel.sendMessage(m).queue(callback);
		}

		return m;
	}

	/**
	 * Replies to the user that executed this command with the passed embed.
	 *
	 * @param embed The embed to reply with.
	 */
	public Message reply(EmbedBuilder embed) {
		return this.reply(new MessageBuilder().setEmbed(embed.build()));
	}

	/**
	 * Replies to the user that executed this command with the passed embed and executed the passed callback afterwards.
	 *
	 * @param embed    The embed to reply with.
	 * @param callback The callback to execute.
	 */
	public Message reply(EmbedBuilder embed, Consumer<Message> callback) {
		return this.reply(new MessageBuilder().setEmbed(embed.build()),callback);
	}

	/**
	 * Replies to the user that executed this command with the passed embed.
	 *
	 * @param embed The embed to reply with.
	 */
	public Message reply(MessageEmbed embed) {
		return this.reply(new MessageBuilder().setEmbed(embed));
	}

	/**
	 * Replies to the user that executed command with the passed embed and executes the passed callback afterwards.
	 *
	 * @param embed    The embed to reply with.
	 * @param callback The callback to execute.
	 */
	public Message reply(MessageEmbed embed, Consumer<Message> callback) {
		return this.reply(new MessageBuilder().setEmbed(embed),callback);
	}

	/**
	 * Replies to the user that executed this command with information about the proper command usage and examples.
	 */
	public Message replyWithUsage() {
		StringBuilder sb = new StringBuilder();

		if (this.getCommandSettings().isReplyToExecutor()) sb.append("\n\n");

		sb.append("**").append(this.t("discord.cmd.help.commandHelp.usage")).append("**: ").append(this.getCommand().getUsage(this.getGuild().getCommandPrefix(), this.label, this.getGuild().getLocale())).append("\n\n");

		sb.append("_").append(this.getCommand().getDescription(this.getGuild().getLocale())).append("_");

		String[] examples = this.getCommand().getExamples(this.getGuild().getCommandPrefix(), this.label, this.getGuild().getLocale());
		if (examples.length > 0) {
			sb.append("\n\n__**").append(this.t("discord.cmd.help.commandHelp.examples")).append(":**__");

			for (String example : examples) {
				sb.append("\n:small_blue_diamond: **`").append(example).append("`**");
			}
		}

		return this.reply(sb.toString());
	}

	/**
	 * Alias of {@link com.gigadrivegroup.yoshino.core.i18n.Locale#getTranslatedMessage(String, Object...)}
	 *
	 * @param phrase    The phrase id of the message.
	 * @param variables The variables of the message.
	 * @return The translated message, returns the english translation as a fallback or the phrase identifier if no message could be found.
	 */
	public String t(String phrase, Object... variables) {
		return getGuild().getLocale().getTranslatedMessage(phrase, variables);
	}
}
