/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.anime;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class KitsuAnimeDataService extends AnimeDataService {
	public KitsuAnimeDataService() {
		super("KITSU-ANIME");
	}

	@Override
	public DataServiceSearchResult[] search(String query) {
		OkHttpClient client = HttpUtil.getHttpClient();

		try {
			Response response = client.newCall(new Request.Builder()
					.url(String.format("https://kitsu.io/api/edge/anime?filter[text]=%s", URLEncoder.encode(query, StandardCharsets.UTF_8)))
					.header("User-Agent", Constants.USER_AGENT)
					.build()).execute();

			String body;
			if (response.isSuccessful() && response.body() != null && !(body = response.body().string()).isEmpty()) {
				JsonElement rootElement = new JsonParser().parse(body);
				response.close();

				if (rootElement.isJsonObject()) {
					JsonObject root = rootElement.getAsJsonObject();

					ArrayList<JsonElement> r = new ArrayList<JsonElement>();

					root.getAsJsonArray("data").forEach(e -> {
						if (r.size() == Config.getInstance().getMaximumDataServiceSearchResults()) return;

						if (e.isJsonObject()) r.add(e);
					});

					ArrayList<DataServiceSearchResult> results = new ArrayList<DataServiceSearchResult>();

					for (int i = 0; i < r.size(); i++) {
						JsonElement e = r.get(i);
						JsonObject attributes = e.getAsJsonObject().get("attributes").getAsJsonObject();

						String slug = attributes.get("slug").getAsString();
						String url = String.format("https://kitsu.io/anime/%s", slug);

						// get proper title
						String title = String.valueOf(e.getAsJsonObject().get("id").getAsInt());
						if (attributes.get("canonicalTitle") != null) {
							title = attributes.get("canonicalTitle").getAsString();
						} else {
							JsonObject titles = attributes.get("titles").getAsJsonObject();
							if (titles.get("en") != null) title = titles.get("en").getAsString();
							if (titles.get("en_jp") != null) title = titles.get("en").getAsString();
							if (titles.get("ja_jp") != null) title = titles.get("en").getAsString();
						}

						// get image
						String image = null;
						if (attributes.has("posterImage") && attributes.get("posterImage").getAsJsonObject().has("large")) {
							image = attributes.get("posterImage").getAsJsonObject().get("large").getAsString();
						}

						// get episodes
						int episodes = -1;
						if (attributes.has("episodeCount") && !attributes.get("episodeCount").isJsonNull()) {
							episodes = attributes.get("episodeCount").getAsInt();
						}

						// get rating
						String rating = null;
						if (attributes.has("averageRating") && !attributes.get("averageRating").isJsonNull()) {
							rating = new StringBuilder(attributes.get("averageRating").getAsString()).append("%").toString();
						}

						// get status
						DataServiceSearchResult.Data.Status status = null;
						if (attributes.has("status") && !attributes.get("status").isJsonNull()) {
							status = DataServiceSearchResult.Data.Status.valueOf(attributes.get("status").getAsString().toUpperCase());
						}

						// get type
						String type = null;
						if (attributes.has("showType") && !attributes.get("showType").isJsonNull()) {
							type = attributes.getAsJsonObject().get("showType").getAsString().toUpperCase();
						}

						// get start date
						String startDate = null;
						if (attributes.has("startDate") && !attributes.get("startDate").isJsonNull()) {
							startDate = attributes.getAsJsonObject().get("startDate").getAsString();
						}

						// get end date
						String endDate = null;
						if (attributes.has("endDate") && !attributes.get("endDate").isJsonNull()) {
							endDate = attributes.getAsJsonObject().get("endDate").getAsString();
						}

						// get synopsis
						String synopsis = null;
						if (attributes.has("synopsis") && !attributes.get("synopsis").isJsonNull()) {
							synopsis = attributes.getAsJsonObject().get("synopsis").getAsString();
						}

						AnimeSearchResultData data = new AnimeSearchResultData();
						data.setIdentifier(slug);
						data.setName(title);
						data.setUrl(url);
						data.setImage(image);
						data.setEpisodes(episodes);
						data.setRatingString(rating);
						data.setType(type);
						data.setStatus(status);
						data.setStartDate(startDate);
						data.setEndDate(endDate);
						data.setDescription(synopsis);

						DataServiceSearchResult result = new DataServiceSearchResult(slug, String.format("%s (%s)", title, type), url, this);
						result.setDataObject(data);

						results.add(result);
					}

					return results.toArray(new DataServiceSearchResult[]{});
				}
			}

			return null;
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
			return null;
		}
	}
}
