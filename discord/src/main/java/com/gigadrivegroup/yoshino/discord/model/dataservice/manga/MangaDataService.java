/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.manga;

import com.gigadrivegroup.yoshino.discord.model.dataservice.DataService;

public class MangaDataService extends DataService {
	public MangaDataService(String name) {
		super(name);
	}
}
