/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core;

import lombok.Getter;

public enum Platform {
	DISCORD("com.gigadrivegroup.yoshino.discord.model.DiscordLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.DiscordTeamPlatformData"),
	PATREON("com.gigadrivegroup.yoshino.core.account.platforms.PatreonLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.PatreonTeamPlatformData"),
	STREAMLABS("com.gigadrivegroup.yoshino.core.account.platforms.StreamlabsLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.StreamlabsTeamPlatformData"),
	TIPEESTREAM("com.gigadrivegroup.yoshino.core.account.platforms.TipeeeStreamLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.TipeeeStreamTeamPlatformData"),
	TWITCH("com.gigadrivegroup.yoshino.core.account.platforms.TwitchLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.TwitchTeamPlatformData"),
	YOUTUBE("com.gigadrivegroup.yoshino.core.account.platforms.YouTubeLinkData", "com.gigadrivegroup.yoshino.core.teams.platforms.YouTubeTeamPlatformData");

	@Getter
	private String linkDataClassName;

	@Getter
	private String teamDataClassName;

	Platform(String linkDataClassName, String teamDataClassName) {
		this.linkDataClassName = linkDataClassName;
		this.teamDataClassName = teamDataClassName;
	}
}
