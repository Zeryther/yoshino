/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.anime;

import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import lombok.Getter;
import lombok.Setter;

public class AnimeSearchResultData extends DataServiceSearchResult.Data {
	@Getter
	@Setter
	private String startDate;

	@Getter
	@Setter
	private String endDate;

	@Getter
	@Setter
	private int episodes;
}
