/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.core.cache;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.HashMap;

public class CacheHandler {
	/**
	 * @param instance The new CacheHandler instance
	 * @return The CacheHandler instance
	 */
	@Getter
	@Setter
	private static CacheHandler instance;

	/**
	 * @return The storage holding all cached objects.
	 */
	@Getter
	private HashMap<String, CacheHandledObject> storage;

	public CacheHandler() {
		this.storage = new HashMap<String, CacheHandledObject>();
	}

	/**
	 * Saves an object to the cache.
	 *
	 * @param key    The key used to identify this object later on.
	 * @param value  THe object to save.
	 * @param expiry The time in milliseconds this object will be saved for.
	 */
	public void setToCache(String key, Object value, long expiry) {
		Validate.notNull(key, "The key may not be null.");
		Validate.notEmpty(key, "The key may not be empty.");
		Validate.notNull(value, "The value may not be null.");
		Validate.isTrue(expiry >= 1, "The expiry has to be at least 1.");

		this.deleteFromCache(key);

		CacheHandledObject object = new CacheHandledObject(key, value, expiry);
		this.storage.put(key, object);
	}

	/**
	 * Gets if an object exists in the cache.
	 *
	 * @param key The object key.
	 * @return True if the object exists and has not expired.
	 */
	public boolean existsInCache(String key) {
		return getFromCache(key) != null;
	}

	/**
	 * Removes the object with the associated key from the storage.
	 *
	 * @param key The key of the object to be deleted.
	 * @return True if the object could be deleted.
	 */
	public boolean deleteFromCache(String key) {
		if (this.existsInCache(key)) {
			this.storage.remove(key);

			return true;
		}

		return false;
	}

	/**
	 * Gets an object associated with the passed key fron the storage.
	 *
	 * @param key The object key.
	 * @return The object, null if it does not exist or has expired.
	 */
	public CacheHandledObject getFromCache(String key) {
		Validate.notNull(key, "The key may not be null!");
		Validate.notEmpty(key, "The key may not be empty!");

		CacheHandledObject object = this.storage.getOrDefault(key, null);

		if (object != null && object.hasExpired()) {
			this.storage.remove(object.getKey());
			object = null;
		}

		return object;
	}

	/**
	 * Gets the value of an object associated with the passed key from the storage.
	 *
	 * @param key The object key.
	 * @return The object value, null if it does not exist or has expired.
	 */
	public Object getValueFromCache(String key) {
		CacheHandledObject object = this.getFromCache(key);

		return object != null ? object.getValue() : null;
	}

	/**
	 * Removes all objects from the {@link CacheHandler#getStorage()}  storage} that are older than {@link CacheHandledObject#getExpiry() their maximum expiry time}.
	 */
	public static void clearCache() {
		getInstance().getStorage().entrySet().removeIf(entry -> entry.getValue().getCreated() + entry.getValue().getExpiry() >= System.currentTimeMillis());
	}
}
