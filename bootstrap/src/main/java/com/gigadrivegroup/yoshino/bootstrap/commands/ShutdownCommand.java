/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands;

import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommand;
import com.gigadrivegroup.yoshino.core.shutdown.Shutdown;

import java.util.Arrays;

public class ShutdownCommand extends ConsoleCommand {
	public ShutdownCommand() {
		super("shutdown");

		setAliases(Arrays.asList("quit", "stop"));
		setDescription("Stops the bot.");
	}

	@Override
	public void execute(String label, String[] args) {
		Shutdown.shutdown();
	}
}
