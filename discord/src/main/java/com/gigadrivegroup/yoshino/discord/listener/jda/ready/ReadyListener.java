/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.jda.ready;

import com.gigadrivegroup.yoshino.discord.model.guild.DiscordGuild;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;

public class ReadyListener extends ListenerAdapter {
	@Override
	public void onReady(ReadyEvent event) {
		JDA shard = event.getJDA();
		ArrayList<JDA> shards = ShardHandler.getInstance().getShards();

		if (!shards.contains(shard)) {
			shards.add(shard);
		}

		shard.getGuilds().forEach(DiscordGuild::getGuild); // load and save data for all guilds

		Logger.log(LoggerLevel.INFO, "Shard %s is ready!", shard.getShardInfo().getShardId());

		if (ShardHandler.getInstance().isReady()) {
			Logger.log(LoggerLevel.INFO, "All shards are ready.");
			ShardHandler.getInstance().broadcastEval(jda -> jda.getPresence().setStatus(OnlineStatus.ONLINE));
		}
	}
}
