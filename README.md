# Yoshino - a fast and reliable chat bot for Twitch, Discord and YouTube
[![Discord](https://discordapp.com/api/guilds/389140727243735053/embed.png)](https://yoshino.gigadrivegroup.com/community)
[![Weblate](http://translate.gigadrivegroup.com/weblate/widgets/yoshino/-/yoshinobot/svg-badge.svg)](https://translate.gigadrivegroup.com/weblate/projects/yoshino/)