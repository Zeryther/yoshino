/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.connection;

import lombok.Getter;

import java.util.ArrayList;

public class ClientConnectionDiscordData {
	/**
	 * @return A list of the shard IDs running on this client.
	 */
	@Getter
	private ArrayList<Integer> shardIDs;

	/**
	 * Constructor
	 */
	public ClientConnectionDiscordData() {
		this.shardIDs = new ArrayList<Integer>();
	}
}
