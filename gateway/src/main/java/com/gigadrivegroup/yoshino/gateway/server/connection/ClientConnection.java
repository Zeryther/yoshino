/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.gateway.server.connection;

import com.gigadrivegroup.yoshino.core.util.serialization.SerializationUtil;
import com.gigadrivegroup.yoshino.gateway.packet.Packet;
import com.gigadrivegroup.yoshino.gateway.packet.handler.PacketListenerManager;
import com.gigadrivegroup.yoshino.gateway.packet.util.PacketUtil;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientConnection {
	public Scanner in;
	public PrintWriter out;
	/**
	 * @return The UUID that is being used to identify this connection internally.
	 */
	@Getter
	private UUID uuid;
	/**
	 * @return The Thread that is being used to read the input stream.
	 */
	@Getter
	private Thread readThread;
	/**
	 * @return The Thread that is being to write to the output stream.
	 */
	@Getter
	private Thread writeThread;
	/**
	 * @return The current status of this connection.
	 */
	@Getter
	@Setter
	private Status status;
	/**
	 * @return The socket instance handled by this thread
	 */
	@Getter
	private Socket socket;
	/**
	 * @return The input stream handled by this thread
	 */
	@Getter
	private DataInputStream inputStream;
	/**
	 * @return The output stream handled by this thread
	 */
	@Getter
	private DataOutputStream outputStream;
	/**
	 * @return The packets currently in queue to be sent.
	 */
	@Getter
	private LinkedBlockingQueue<Packet> packetQueue;
	/**
	 * @return The Discord data of this client.
	 */
	@Getter
	private ClientConnectionDiscordData discordData;
	private String lastLine;

	public ClientConnection(UUID uuid, Socket socket, DataInputStream inputStream, DataOutputStream outputStream) {
		this.uuid = uuid;
		this.socket = socket;
		this.inputStream = inputStream;
		this.outputStream = outputStream;

		this.in = new Scanner(this.inputStream);
		this.out = new PrintWriter(this.outputStream, true);

		this.status = Status.UNAUTHENTICATED;
		this.packetQueue = new LinkedBlockingQueue<Packet>();

		this.discordData = new ClientConnectionDiscordData();
	}

	public void start() {
		Logger.log(LoggerLevel.DEBUG, "New socket client connected (%s): %s", this.uuid, this.socket);

		// handle input
		this.readThread = new Thread(() -> {
			try {
				while (this.in.hasNextLine() && isConnected()) {
					this.lastLine = this.in.nextLine();

					// handle input
					if (PacketUtil.isValidPacket(this.lastLine)) {
						Packet packet = PacketUtil.getStringAsPacket(this.lastLine);

						if (packet != null) {
							packet.setAssociatedConnectionUUID(this.uuid);

							Logger.log(LoggerLevel.DEBUG, "Incoming packet: %s", packet.getType());

							PacketListenerManager.handlePacket(packet);
						}
					}
				}

				this.close();
				ClientConnectionManager.getInstance().getConnections().remove(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, String.format("Gateway-ClientConnection-[%s]-Read", this.getUuid().toString()));

		// handle output
		this.writeThread = new Thread(() -> {
			try {
				while (isConnected()) {
					if (this.packetQueue.size() > 0) {
						Packet packet = this.packetQueue.take();

						out.println(SerializationUtil.GSON.toJson(packet));
						Logger.log(LoggerLevel.DEBUG, "Outgoing packet: %s", packet.getType());
					}
				}

				this.close();
				ClientConnectionManager.getInstance().getConnections().remove(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, String.format("Gateway-ClientConnection-[%s]-Write", this.getUuid().toString()));

		this.readThread.start();
		this.writeThread.start();
	}

	/**
	 * @return True if the connection is still running.
	 */
	public boolean isConnected() {
		if (this.socket == null) {
			return false;
		} else if (this.socket.isConnected()) {
			return true;
		} else {
			try {
				this.close();
				ClientConnectionManager.getInstance().getConnections().remove(this);
			} catch (IOException ignored) {
				// does not matter, just ignore
			}

			return false;
		}
	}

	/**
	 * Closes the connection and all readers.
	 *
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (this.readThread != null) {
			this.readThread.interrupt();
			this.readThread = null;
		}

		if (this.writeThread != null) {
			this.writeThread.interrupt();
			this.writeThread = null;
		}

		if (this.socket != null) {
			this.socket.close();
			this.socket = null;

			Logger.log(LoggerLevel.DEBUG, "Socket client disconnected (%s)", this.uuid);
		}

		if (this.in != null) {
			this.in.close();
			this.in = null;
		}

		if (this.out != null) {
			this.out.close();
			this.out = null;
		}
	}

	/**
	 * Adds a packet to the {@link ClientConnection#getPacketQueue() queue}.
	 *
	 * @param packet
	 */
	public void sendPacket(Packet packet) {
		try {
			packet.setAssociatedConnectionUUID(this.getUuid());
			this.packetQueue.put(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public enum Status {
		AUTHENTICATED,
		UNAUTHENTICATED
	}
}
