/*
 * Copyright (C) 2018 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.bootstrap.commands.reader;

import com.gigadrivegroup.yoshino.bootstrap.commands.manager.ConsoleCommand;
import com.gigadrivegroup.yoshino.logger.Logger;
import com.gigadrivegroup.yoshino.logger.LoggerLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.Scanner;

public class ConsoleCommandReader {
	/**
	 * @param instance The new ConsoleCommandReader instance.
	 * @return The ConsoleCommandReader instance.
	 */
	@Getter
	@Setter
	private static ConsoleCommandReader instance;

	/**
	 * @return The main thread that is reading console input.
	 */
	@Getter
	private Thread thread;

	/**
	 * @return The command parser instance.
	 */
	@Getter
	private ConsoleCommandParser parser;
	private Scanner scanner;

	/**
	 * The constructor.
	 */
	public ConsoleCommandReader() {
		this.parser = new ConsoleCommandParser();
	}

	/**
	 * Starts the command reader thread.
	 */
	public void start() {
		Validate.isTrue(this.thread == null, "The console command reader is already running.");

		this.thread = new Thread(() -> {
			this.scanner = new Scanner(System.in);

			while (this.scanner.hasNextLine()) {
				String input = this.scanner.nextLine();
				ConsoleCommandParser.ParsedData data = this.parser.parseData(input);

				if (data != null) {
					ConsoleCommand command = data.getCommand();

					if (command != null) {
						command.execute(data.getLabel(), data.getArgs());
					} else {
						Logger.log(LoggerLevel.ERROR, "That command could not be found.");
					}
				} else {
					Logger.log(LoggerLevel.ERROR, "Please enter a command.");
				}
			}
		}, "Yoshino Console Command Reader");

		this.thread.start();
	}

	/**
	 * Stops the command reader thread.
	 */
	public void stop() {
		if (this.thread != null) {
			this.thread.interrupt();
			this.thread = null;
		}
	}
}
