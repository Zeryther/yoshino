/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.api.account;

import com.gigadrivegroup.yoshino.api.account.paths.Identity;
import com.gigadrivegroup.yoshino.api.account.paths.Info;

public class AccountPathGroup {
	public static void init() {
		new Identity();
		new Info();
	}
}
