/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.listener.jda;

import com.gigadrivegroup.yoshino.discord.listener.jda.guild.GuildLeaveListener;
import com.gigadrivegroup.yoshino.discord.listener.jda.message.MessageReceivedListener;
import com.gigadrivegroup.yoshino.discord.listener.jda.ready.ReadyListener;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import lombok.Getter;
import net.dv8tion.jda.bot.sharding.ShardManager;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;

public class JDAListenerManager {
	@Getter
	private static EventWaiter eventWaiter;

	static {
		eventWaiter = new EventWaiter();
	}

	public static void registerListeners(ShardManager shardManager) {
		ArrayList<ListenerAdapter> listeners = new ArrayList<ListenerAdapter>();

		// guild
		listeners.add(new GuildLeaveListener());

		// message
		listeners.add(new MessageReceivedListener());

		// ready
		listeners.add(new ReadyListener());

		listeners.forEach(shardManager::addEventListener);
	}
}
