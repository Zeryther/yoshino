/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord;

import com.gigadrivegroup.yoshino.core.cache.CacheHandler;
import com.gigadrivegroup.yoshino.core.database.DatabaseManager;
import com.gigadrivegroup.yoshino.core.module.ModuleMetricsCollector;
import com.gigadrivegroup.yoshino.discord.shard.ShardHandler;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import net.dv8tion.jda.core.JDA;

import java.util.HashMap;
import java.util.Iterator;

public class DiscordMetricsCollector extends ModuleMetricsCollector {
	/**
	 * @return The amount of guilds on the current instance.
	 */
	public int getCurrentGuilds() {
		int guilds = 0;

		Iterator<JDA> shardIterator = ShardHandler.getInstance().getShards().iterator();
		while (shardIterator.hasNext()) {
			JDA shard = shardIterator.next();

			guilds += shard.getGuilds().size();
		}

		return guilds;
	}

	/**
	 * @return The amount of guilds on all instances.
	 */
	public int getTotalGuilds() {
		String key = "discordTotalGuilds";

		if (CacheHandler.getInstance().existsInCache(key)) {
			return (int) CacheHandler.getInstance().getValueFromCache(key);
		} else {
			int count;

			DatabaseManager manager = DatabaseManager.getInstance();

			DBCursor cursor = manager.discordGuilds().find(new BasicDBObject("inside", true));
			count = cursor.count();

			cursor.close();

			CacheHandler.getInstance().setToCache(key, count, 10 * 1000);

			return count;
		}
	}

	@Override
	public HashMap<String, Object> toMap() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		result.put("currentGuilds", this.getCurrentGuilds());
		result.put("totalGuilds", this.getTotalGuilds());

		return result;
	}
}
