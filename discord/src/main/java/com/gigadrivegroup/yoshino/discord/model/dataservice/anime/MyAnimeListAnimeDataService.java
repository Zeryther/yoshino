/*
 * Copyright (C) 2019 Gigadrive Group - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * https://yoshino.gigadrivegroup.com/dev/technologies
 */
package com.gigadrivegroup.yoshino.discord.model.dataservice.anime;

import com.gigadrivegroup.yoshino.core.config.Config;
import com.gigadrivegroup.yoshino.core.util.Constants;
import com.gigadrivegroup.yoshino.core.util.HttpUtil;
import com.gigadrivegroup.yoshino.core.util.Util;
import com.gigadrivegroup.yoshino.discord.model.dataservice.DataServiceSearchResult;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class MyAnimeListAnimeDataService extends AnimeDataService {
	public MyAnimeListAnimeDataService() {
		super("MAL-ANIME");
	}

	@Override
	public DataServiceSearchResult[] search(String query) {
		OkHttpClient client = HttpUtil.getHttpClient();

		try {
			Response response = client.newCall(new Request.Builder()
					.url(String.format("https://api.jikan.moe/v3/search/anime/?q=%s&page=1?genre=12&genre_exclude=0", URLEncoder.encode(query, StandardCharsets.UTF_8)))
					.header("User-Agent", Constants.USER_AGENT)
					.build()).execute();

			String body;
			if (response.isSuccessful() && response.body() != null && !(body = response.body().string()).isEmpty()) {
				JsonElement rootElement = new JsonParser().parse(body);
				response.close();

				if (rootElement.isJsonObject()) {
					JsonObject root = rootElement.getAsJsonObject();

					ArrayList<JsonElement> r = new ArrayList<JsonElement>();

					if (root.has("results")) {
						root.getAsJsonArray("results").forEach(e -> {
							if (r.size() == Config.getInstance().getMaximumDataServiceSearchResults()) return;

							if (e.isJsonObject()) r.add(e);
						});

						ArrayList<DataServiceSearchResult> results = new ArrayList<DataServiceSearchResult>();

						for (int i = 0; i < r.size(); i++) {
							JsonObject attributes = r.get(i).getAsJsonObject();

							String identifier = attributes.get("mal_id").getAsString();
							String url = attributes.get("url").getAsString();
							String image = attributes.has("image_url") ? attributes.get("image_url").getAsString() : null;
							String title = attributes.get("title").getAsString();
							DataServiceSearchResult.Data.Status status = attributes.get("airing").getAsBoolean() ? DataServiceSearchResult.Data.Status.CURRENT : DataServiceSearchResult.Data.Status.FINISHED;
							String synopsis = attributes.has("synopsis") && !attributes.get("synopsis").isJsonNull() ? attributes.get("synopsis").getAsString() : null;
							String type = attributes.get("type").getAsString().toUpperCase();
							int episodes = attributes.has("episodes") && !attributes.get("episodes").isJsonNull() ? attributes.get("episodes").getAsInt() : -1;
							String rating = attributes.has("score") && !attributes.get("score").isJsonNull() ? attributes.get("score").getAsString() : null;
							String startDate = attributes.has("start_date") && !attributes.get("start_date").isJsonNull() ? Util.limitString(attributes.get("start_date").getAsString(), 11) : null;
							String endDate = attributes.has("end_date") && !attributes.get("end_date").isJsonNull() ? Util.limitString(attributes.get("end_date").getAsString(), 11) : null;

							AnimeSearchResultData data = new AnimeSearchResultData();
							data.setIdentifier(identifier);
							data.setName(title);
							data.setUrl(url);
							data.setImage(image);
							data.setEpisodes(episodes);
							data.setRatingString(rating);
							data.setType(type);
							data.setStatus(status);
							data.setStartDate(startDate);
							data.setEndDate(endDate);
							data.setDescription(synopsis);

							DataServiceSearchResult result = new DataServiceSearchResult(identifier, String.format("%s (%s)", title, type), url, this);
							result.setDataObject(data);

							results.add(result);
						}

						return results.toArray(new DataServiceSearchResult[]{});
					}
				}
			}

			return null;
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
			return null;
		}
	}
}
